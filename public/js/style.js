$(document).ready(function () {
    /*menu-mobile*/
    $('.bar-toggle-menu-mobile').click(function () {
        $('.big-menu-sakura').toggleClass('active');
        $('.pc-header').toggleClass('active');
    });

    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {

        /*menu-header*/

        $('.list-menu .dropdown .wrap-menu').click(function () {
            let isActive = $(this).closest('.dropdown.active');
            let isOpen = $(this).closest('.dropdown.open');
            if (isActive.length && isOpen.length) {
                isActive.removeClass('active open');
            } else if (isActive.length) {
                isActive.addClass('open');
            } else {
                $('.list-menu .dropdown').removeClass('active open');
                $(this).closest('li').addClass('active open');
            }
        });
    }
    /*open-select-level-1*/
    $('.select-level-1 .list-select-area li').click(function () {
        if (!$(this).hasClass('active')) {
            $('.select-level-1 .list-select-area li').removeClass('active');
            $(this).addClass('active');
        } else {
            $(this).removeClass('active');
        }
    });


    /*header-scroll*/
    var status = true;
    $(window).scroll(function () {
        var heightScroll = $(window).scrollTop();

        if (heightScroll >= 1) {
            if (status == true) {
                $('.pc-header').addClass('scroll');
                status = false;
            }
        } else {
            if (status == false) {
                $('.pc-header').removeClass('scroll');
                status = true;
            }
        }
    });

    /*tab-services*/
    /*<!-- addclass active để mở panel-body-services -->*/
    $('.item-services-sakura').click(function () {
        $('.item-services-sakura').removeClass('active');
        $(this).addClass('active');
    });

    /*<!-- addclass active để mở panel-body-services -->*/
    $('.item-experience-sakura').click(function () {
        $('.item-experience-sakura').removeClass('active');
        $(this).addClass('active');
    });


    /**addclass active để mở tab-program-tour*/

    $('.tab-program-tour .list .tab-big').click(function () {
        $('.tab-program-tour .list .tab-big').removeClass('active');
        $(this).addClass('active');
    });

    /**addclass active để mở tab-program-tour*/

    $('.item-services-sakura.order-tour').click(function () {
        $('.item-services-sakura.order-tour').removeClass('active');
        $(this).addClass('active');
    });

    $.ajax({
        type: 'GET',
        url: '/give-me-csrf',
        success: function (data) {
            if (data.success) {
                let input = $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "_token").val(data.data);
                $('form').append($(input));
                $('meta[name="csrf-token"]').attr('content', data.data);
            }
        },
    });
    $("#form-contact").submit(function (e) {
        e.preventDefault();
        $('.form-group-sakura').removeClass('has-error'); // remove the error class
        $('.help-block').remove(); // remove the error text
        var formData = {
            '_token': $('input[name=_token]').val(),
            'fullName': $('input[name=fullName]').val(),
            'email': $('input[name=email]').val(),
            'phone': $('input[name=phone]').val(),
            'address': $('input[name=address]').val(),
            'node': $('textarea[name=node]').val()
        };
        $.ajax({
            type: 'POST',
            url: '/lienheSakura',
            data: formData,
            dataType: 'json',
            encode: true
        }).done(function (data) {
            if (!data.success) {
                if (data.errors.fullName) {
                    $('#name-group').addClass('has-error');
                    $('#name-group').append('<div class="help-block">' + data.errors.fullName + '</div>');
                }
                if (data.errors.email) {
                    $('#email-group').addClass('has-error');
                    $('#email-group').append('<div class="help-block">' + data.errors.email + '</div>');
                }
                if (data.errors.phone) {
                    $('#phone-group').addClass('has-error');
                    $('#phone-group').append('<div class="help-block">' + data.errors.phone + '</div>');
                }
                if (data.errors.address) {
                    $('#address-group').addClass('has-error');
                    $('#address-group').append('<div class="help-block">' + data.errors.address + '</div>');
                }
                if (data.errors.node) {
                    $('#node-group').addClass('has-error');
                    $('#node-group').append('<div class="help-block">' + data.errors.node + '</div>');
                }
            } else {
                $('form').find("input, textarea").val('');
                $('form').append('<div class="alert alert-success">' + data.message + '</div>');
            }
        });
    });

    $("#quick-quote").submit(function (e) {
        e.preventDefault();
        $('.form-sakura').find('.alert').remove();
        let provinces = [];
        $('.select-level-2').find('input[type=checkbox]').each((k, v) => {
            if (v.checked) {
                provinces.push(v.value);
            }
        });
        let criterias = {
            'provinces': provinces,
            'places': $('select[name*=places]').val(),
            'requests': $('select[name*=requests]').val()
        };
        let formData = {
            '_token': $('input[name=_token]').val(),
            'expected_departure_date': $('input[name=expected_departure_date]').val(),
            'expected_number_of_days': $('select[name=expected_number_of_days]').val(),
            'departure_from_id': $('select[name=departure_from_id]').val(),
            'number_of_adults': $('select[name=number_of_adults]').val(),
            'number_of_children': $('select[name=number_of_children]').val(),
            'number_of_babies': $('select[name=number_of_babies]').val(),
            'criterias': criterias,
            'special_requirements': $('textarea[name=special_requirements]').val(),
            'fullName': $('input[name=fullName]').val(),
            'email': $('input[name=email]').val(),
            'phone': $('input[name=phone]').val(),
            'address': $('input[name=address]').val(),
            'node': $('textarea[name=node]').val()
        };
        $.ajax({
            type: 'POST',
            url: '/sendQuickQuote',
            data: formData,
            dataType: 'json',
            encode: true
        }).done(function (data) {
            if (!data.success) {
                for (i in data.errors) {
                    $('.form-sakura').append('<div class="alert alert-danger">' + data.errors[i] + '</div>');
                }
            } else {
                $('.form-sakura').find("input, textarea").val('');
                $('.form-sakura').append('<div class="alert alert-success">' + data.message + '</div>');
            }
        });
    });
    // let height_desc = $(".services-sakura .inner #content-description").height();
    // if (height_desc < 60) {
    //     $(".services-sakura .inner .wrap-btn-more").css('display', 'none');
    //     $(".services-sakura .inner").addClass('expanded');
    // } else {
    //     $(".services-sakura .inner #content-description").css('height', '60px');
    // }
    // $('.btn--view-more-fb').on('click', function(e) {
    //     e.preventDefault();
    //     this.expand = !this.expand;
    //     if ($('.services-sakura .inner').hasClass("expanded")) {
    //         $("html, body").animate({
    //             scrollTop: $("#content-description").offset().top - 300
    //         }, 0);
    //     }
    //     $(this).closest('.services-sakura').find('.inner').toggleClass('expanded');
    // });


    if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $("#v7_BD_ZoomImg").elevateZoom({
            gallery: "sys_col_list_thumb",
            galleryActiveClass: "active"
        });
        $(".wrap-thumb-option").on("click", function () {
            var img_zoom = $(this).find("img").attr("data-zoom-image");
            var img_big = $(this).find("img").attr("data-img-big");
            $('.sys_img_big').attr('data-zoom-image', img_zoom);
            $('.sys_img_big').attr('src', img_big);
        });
        var sys_col_list_thumb = $("#sys_col_list_thumb");
        if (sys_col_list_thumb.length > 0) {
            var configThumbPerPage = 7,
                configItemWeight = 85,
                viewportWeightAvaiable = $("#sys_col_list_thumb").width(),
                slideThumbPageCurrent = 1,
                cloneThumbItem = sys_col_list_thumb.children(".sys_show_img_big").last().clone();
            sys_col_list_thumb.css("max-width", viewportWeightAvaiable);
            cloneThumbItem = cloneThumbItem.html("").css("visibility", "hidden");
            configThumbPerPage = Math.floor(viewportWeightAvaiable / configItemWeight);
            var lastSlideItem = sys_col_list_thumb.children(".sys_show_img_big").length % configThumbPerPage,
                slideThumbPageTotal = Math.ceil(sys_col_list_thumb.children(".sys_show_img_big").length / configThumbPerPage);
            if (sys_col_list_thumb.children(".sys_show_img_big").length > configThumbPerPage)
                $("#sys_btn_slide_thumbs").show();
            if (lastSlideItem != 0) {
                for (var p = lastSlideItem; p < configThumbPerPage; p++) {
                    var cloneThumbHtml = cloneThumbItem.prop('outerHTML');
                    $(cloneThumbHtml).insertBefore("#sys_btn_slide_thumbs");
                }
            }
            sys_col_list_thumb.children(".sys_show_img_big").each(function (idx, val) {
                idx++;
                if (idx > configThumbPerPage) {
                    $(this).hide();
                }
                $(this).addClass("sys_thumb_slide_" + (Math.floor((idx - 1) / configThumbPerPage) + 1));
            });
            $("#sys_btn_slide_thumbs").on("click", function () {
                if (slideThumbPageCurrent != slideThumbPageTotal) {
                    slideThumbPageCurrent++
                } else {
                    slideThumbPageCurrent = 1;
                }
                sys_col_list_thumb.children(".sys_show_img_big").hide();
                sys_col_list_thumb.children(".sys_thumb_slide_" + slideThumbPageCurrent).show();
            });
        }
    }

    $(".wrap-thumb").on("mouseover", function () {
        $(this).siblings().removeClass("active").end().addClass("active");
        $(this).children().trigger("click");
        var img_zoom = $(this).find("img").attr("data-zoom-image");
        var img_big = $(this).find("img").attr("data-img-big");
        $('.sys_img_big').attr('data-zoom-image', img_zoom);
        $('.sys_img_big').attr('src', img_big);
    });


    $('#form-language .language-select').on('click', function (e) {
        e.preventDefault();
        $('#form-language input[name=\'code\']').val($(this).attr('name'));
        $('#form-language').submit();
    });

});


/*open-page-detail*/
$('#readmore-detail').click(function () {
    $('.content-news').toggleClass("auto");
    if ($('.content-news').hasClass('auto')) {
        $('#readmore-detail').text('Thu gọn');
    } else {
        $('#readmore-detail').text('Xem thêm');
    }
    return false;
});

/*slide banner*/
var swiper = new Swiper('.wp-slide-banner', {
    centeredSlides: true,
    autoplay: {
        delay: 7400,
        disableOnInteraction: false,
    },
    pagination: {
        el: '.pagination-banner',
        clickable: true,
    },
    height: 250,
    autoHeight: true,
});

/*swiper-function-sakura*/


var swiper = new Swiper('#swiper-function', {
    slidesPerView: 5,
    spaceBetween: 30,
    centeredSlides: true,
    loop: true,
    autoplay: {
        delay: 2000,
        disableOnInteraction: false,
    },
    autoHeight: true,
    breakpoints: {
        320: {
            slidesPerView: 1,
        },
        480: {
            slidesPerView: 1,
        },
        567: {
            slidesPerView: 1,
        },
        767: {
            slidesPerView: 3,
        },
        991: {
            slidesPerView: 3,
        },
    }

});


// top page

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function () {
    scrollFunction()
};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        $('#toppage-btn').fadeIn(200);
    } else {
        $('#toppage-btn').fadeOut(200);
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    $('body,html').animate({
        scrollTop: 0
    }, 500);
}


//active menu
$(document).ready(function () {
    $('.big-menu-sakura .list-menu  > .item-menu').has('a[href="' + $(location)[0].pathname + '"]').addClass('active');
})


// panel-head-select

if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    $('.big-select-area .select-area .panel-head-select').click(function () {
        $(this).closest('.select-area').toggleClass('active');
    });
}   
