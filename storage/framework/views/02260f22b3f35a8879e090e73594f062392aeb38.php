<?php $__env->startSection('content'); ?>
    <?php if(count($breadcrumbs) > 0): ?>
        <div class="wp-breadcumb">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="list-breadcumb">
                            <?php $__currentLoopData = $breadcrumbs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $breadcrumb): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li>
                                    <a href="<?php echo e($breadcrumb['href']); ?>">
                                        <span><?php echo ucfirst(mb_strtolower($breadcrumb['text'],'UTF-8')) ?></span>
                                    </a>
                                </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php
        echo $content_top;
    ?>
    <?php if(!empty($data)): ?>
        <div class="detail-article-travel-guide">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <article class="article-travel-guide row">
                            <div class="col-lg-10 col-md-10 col-sm-12 col-12 mx-auto">
                                <div class="info">
                                    <h3 class="heading-3 title"><?php echo e($data['title']); ?></h3>
                                    <div class="meta-date">
                                        <strong><?php echo e(date("d-m-Y", strtotime($data['created_at']))); ?></strong></div>
                                    <div class="rule"></div>
                                    <!-- <div class="intro heading-3"><?php echo $data['description'] ?></div> -->
                                    <div class="content-post"><?php echo $data['content'] ?></div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php
        echo $content_bottom;
    ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
    <?php
        echo $footer;
    ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('client.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\xampp\htdocs\sakura.amela\resources\views/client/page/detail_reviews.blade.php ENDPATH**/ ?>