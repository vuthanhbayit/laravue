<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <?php echo $__env->make('client.share.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->yieldContent('style'); ?>
</head>
<body>
<?php echo $__env->make('client.share.menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->yieldContent('banner'); ?>
<?php echo $__env->yieldContent('content'); ?>
<?php echo $__env->yieldContent('footer'); ?>
<?php echo $__env->yieldContent('script'); ?>
</body>
</html>
<?php /**PATH E:\xampp\htdocs\Amela\sakura.amela\resources\views/client/main.blade.php ENDPATH**/ ?>