<title><?php if(isset($title)): ?> <?php echo e($title); ?> <?php endif; ?></title>
<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<link href="<?php echo e(asset(getConfig('config_icon'))); ?>" rel="icon"/>
<?php if(isset($description)): ?>
    <meta name="description" content="<?php echo e($description); ?>"/>
<?php endif; ?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/css/client/fontawesome-all.min.css">
<link rel="stylesheet" href="/css/client/swiper.min.css">
<link rel="stylesheet" href="/css/client/bootstrap.min.css">
<link rel="stylesheet" href="/css/client/animate.min.css">
<link rel="stylesheet" href="/css/client/general.css">
<?php /**PATH E:\xampp\htdocs\Amela\sakura.amela\resources\views/client/share/head.blade.php ENDPATH**/ ?>