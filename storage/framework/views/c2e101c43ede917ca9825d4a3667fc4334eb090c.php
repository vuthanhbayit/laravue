<?php $__env->startSection('banner'); ?>
    <?php if(count($data['banner']) > 0): ?>
        <div class="banner-horizontal pt-header">
            <div class="swiper-container wp-slide-banner" id="wp-slide-banner">
                <div class="swiper-wrapper slide-banner">
                    <?php $__currentLoopData = $data['banner']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="swiper-slide item-slide">
                            <div class="img-thumb">
                                <a href="<?php echo e($item->link); ?>" class="item-slide-banner ">
                                    <img src="<?php echo e(asset($item->image)); ?>" alt="" class="image-cover"/>
                                </a>
                            </div>
                            <div class="info-banner container">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="overlay-banner">
                                            <h3 class="title text-white"><?php echo e($item->title); ?></h3>
                                            <div class="text text-white subtitle mb-3">
                                                <?php echo $item->content; ?>
                                            </div>
                                            <a href="<?php echo e($item->link); ?>"
                                               class="readmore text-white"><?php echo e(getLanguage('ViewMore')); ?></a>
                                        </div>
                                    </div>
                                    <div class="col-lg-5"></div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination pagination-banner"></div>
            </div>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <?php
        echo $content_top;
    ?>
    <?php if(count($breadcrumbs) > 0): ?>
        <div class="wp-breadcumb">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="list-breadcumb">
                            <?php $__currentLoopData = $breadcrumbs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $breadcrumb): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li><a href="<?php echo e($breadcrumb['href']); ?>"><span><?php echo e($breadcrumb['text']); ?></span></a></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if(!empty($data['information'])): ?>
        <div class="about-campany-sakura">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-10 col-md-10 col-sm-12 col-12 m-auto">
                                <div class="content-about-campany pd-sakura">
                                    <h1><?php echo $data['information']['title']; ?></h1>
                                </div>
                                <div class="panel-body-open-letter  pd-sakura">
                                    <?php echo $data['information']['description']; ?>

                                    <?php echo $data['information']['content']; ?>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="open-letter-sakura">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-lg-10 col-md-10 col-sm-12 col-12 m-auto">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php
        echo $content_bottom;
    ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
    <?php
        echo $footer;
    ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('client.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\sakura.amela\resources\views/client/page/information.blade.php ENDPATH**/ ?>