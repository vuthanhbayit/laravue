<?php $__env->startSection('style'); ?>
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet"/>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('banner'); ?>
    <?php if(count($banner) > 0): ?>
        <div class="banner-horizontal pt-header">
            <div class="swiper-container wp-slide-banner" id="wp-slide-banner">
                <div class="swiper-wrapper slide-banner">
                    <?php $__currentLoopData = $banner; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="swiper-slide item-slide">
                            <div class="img-thumb">
                                <a href="<?php echo e($item->link); ?>" class="item-slide-banner">
                                    <img src="<?php echo e(asset($item->image)); ?>" alt="<?php echo e($item->title); ?>" class="image-cover"/>
                                </a>
                            </div>
                            <div class="info-banner container">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="overlay-banner">
                                            <h3 class="title text-white"><?php echo e($item->title); ?></h3>
                                            <div class="text text-white subtitle mb-3">
                                                <?php echo $item->content; ?>

                                            </div>
                                            <a href="<?php echo e($item->link); ?>"
                                               class="readmore text-white"><?php echo e(getLanguage('ViewMore')); ?></a>
                                        </div>
                                    </div>
                                    <div class="col-lg-5"></div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination pagination-banner"></div>
            </div>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php if(count($breadcrumbs) > 0): ?>
        <div class="wp-breadcumb">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="list-breadcumb">
                            <?php $__currentLoopData = $breadcrumbs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $breadcrumb): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li><a href="<?php echo e($breadcrumb['href']); ?>"><span><?php echo e($breadcrumb['text']); ?></span></a></li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="price-fast-sakura pd-sakura">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-lg-10 col-md-10 col-sm-12 col-12 m-auto">
                            <div class="content-price-fast-sakura">

                                <div class="panel-head-price-fast panel-head-general text-center">
                                    <div class="row">
                                        <div class="col-12 m-auto">
                                            <h2 class="heading-2"><?php echo e(getLanguage('quickQuote')); ?></h2>
                                            <div class="rule"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="head-BGN text-center pd-sakura pb-0">
                                            <h3><?php echo e(getLanguage('Depart')); ?></h3>
                                        </div>
                                        <div class="row pd-50">
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                                                <div class="wp-select">
                                                    <div class="row">
                                                        <div class='col-12'>
                                                            <div class="form-group">
                                                                <label for="">
                                                                    <h4 class="title-select heading-3"><?php echo e(getLanguage('ExpectedDepartureDate')); ?></h4>
                                                                </label>
                                                                <div class="box-select">
                                                                    <input type="text" name="expected_departure_date"
                                                                           class="input-text-sakura" value="">
                                                                    <span class="icon-select"><i
                                                                                class="fas fa-angle-down"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                                                <div class="wp-select">
                                                    <div class="row">
                                                        <div class='col-12'>
                                                            <div class="form-group">
                                                                <label for="">
                                                                    <h4 class="title-select heading-3">
                                                                        <?php echo e(getLanguage('ExpectedNumberOfDays')); ?>

                                                                    </h4>
                                                                </label>
                                                                <div class="box-select">
                                                                    <select name="expected_number_of_days"
                                                                            class="input-text-sakura">
                                                                        <option value="0"> <?php echo e(getLanguage('SelectTime')); ?></option>
                                                                        <?php for($i = 0; $i < 20; $i++): ?>
                                                                            <option value="<?php echo e($i+1); ?>"><?php echo e($i + 1); ?> <?php echo e(getLanguage('Day')); ?>

                                                                            </option>
                                                                        <?php endfor; ?>
                                                                    </select>
                                                                    <span class="icon-select"><i
                                                                                class="fas fa-angle-down"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                                                <div class="wp-select">
                                                    <div class="row">
                                                        <div class='col-12'>
                                                            <div class="form-group">
                                                                <label for="">
                                                                    <h4 class="title-select heading-3">
                                                                        <?php echo e(getLanguage('DepartureFromId')); ?>

                                                                    </h4>
                                                                </label>
                                                                <div class="box-select">
                                                                    <select name="departure_from_id"
                                                                            class="input-text-sakura">
                                                                        <option value="0"><?php echo e(getLanguage('SelectCity')); ?></option>
                                                                        <option value="Hà Nội">Hà Nội</option>
                                                                        <option value="TP. Hồ Chí Minh">TP. Hồ Chí Minh</option>
                                                                        <option value="Cần Thơ">Cần Thơ</option>
                                                                        <option value="Đà Nẵng">Đà Nẵng</option>
                                                                        <option value="Hải Phòng">Hải Phòng</option>
                                                                    </select>
                                                                    <span class="icon-select"><i
                                                                                class="fas fa-angle-down"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="head-BGN text-center pd-sakura pb-0">
                                            <h3><?php echo e(getLanguage('NumberOfParticipants')); ?></h3>
                                        </div>
                                        <div class="row pd-50">
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                                                <div class="wp-select">
                                                    <div class="row">
                                                        <div class='col-12'>
                                                            <div class="form-group">
                                                                <label for="">
                                                                    <h4 class="title-select heading-3">
                                                                        <?php echo e(getLanguage('NumberOfAdults')); ?>

                                                                        (<?php echo e(getLanguage('Over12YearsOld')); ?>)
                                                                    </h4>
                                                                </label>
                                                                <div class="box-select">
                                                                    <select name="number_of_adults" id=""
                                                                            class="input-text-sakura">
                                                                        <?php for($i = 0; $i < 40; $i++): ?>
                                                                            <option value="<?php echo e($i+1); ?>"><?php echo e($i + 1); ?> <?php echo e(getLanguage('People')); ?>

                                                                            </option>
                                                                        <?php endfor; ?>
                                                                    </select>
                                                                    <span class="icon-select"><i
                                                                                class="fas fa-angle-down"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                                                <div class="wp-select">
                                                    <div class="row">
                                                        <div class='col-12'>
                                                            <div class="form-group">
                                                                <label for="">
                                                                    <h4 class="title-select heading-3">
                                                                        <?php echo e(getLanguage('NumberOfChildren')); ?>

                                                                        (<?php echo e(getLanguage('From2To11YearsOld')); ?>)
                                                                    </h4>
                                                                </label>
                                                                <div class="box-select">
                                                                    <select name="number_of_children" id=""
                                                                            class="input-text-sakura">
                                                                        <?php for($i = 0; $i <= 40; $i++): ?>
                                                                            <option value="<?php echo e($i); ?>"><?php echo e($i); ?> <?php echo e(getLanguage('People')); ?>

                                                                            </option>
                                                                        <?php endfor; ?>
                                                                    </select>
                                                                    <span class="icon-select"><i
                                                                                class="fas fa-angle-down"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                                                <div class="wp-select">
                                                    <div class="row">
                                                        <div class='col-12'>
                                                            <div class="form-group">
                                                                <label for="">
                                                                    <h4 class="title-select heading-3">
                                                                        <?php echo e(getLanguage('NumberOfBabies')); ?>

                                                                        (<?php echo e(getLanguage('Under2YearsOld')); ?>)
                                                                    </h4></label>
                                                                <div class="box-select">
                                                                    <select name="number_of_babies" id=""
                                                                            class="input-text-sakura">
                                                                        <?php for($i = 0; $i <= 40; $i++): ?>
                                                                            <option value="<?php echo e($i); ?>"><?php echo e($i); ?> <?php echo e(getLanguage('People')); ?>

                                                                            </option>
                                                                        <?php endfor; ?>
                                                                    </select>
                                                                    <span class="icon-select"><i
                                                                                class="fas fa-angle-down"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="head-BGN text-center pd-sakura pb-0">
                                            <h3> <?php echo e(getLanguage('SelectionCriteria')); ?> </h3>
                                        </div>

                                        <div class="row pd-50">
                                            <div class="col-12">
                                                <div class="tourist-area row">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-12 mx-auto">
                                                        <div class="wp-select">
                                                            <div class="row">
                                                                <div class='col-12'>
                                                                    <div class="form-group">
                                                                        <div class="big-select-area">
                                                                            <div class="select-area">
                                                                                <div class="panel-head-select">
                                                                                    <div class="text">
                                                                                        <?php echo e(getLanguage('TouristArea')); ?>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel-body-select">
                                                                                    <div class="select-level-1">
                                                                                        <ul class="list-select-area">
                                                                                            <?php $__currentLoopData = $data['provinces']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                                <li>
                                                                                                    <div class="item">
                                                                                                        <div class="text"><?php echo e($v['area']['title']); ?></div>
                                                                                                        <div class="select-level-2">
                                                                                                            <div class="item-check">
                                                                                                                <?php $__currentLoopData = $v['province']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                                                    <label class="container">
                                                                                                                        <?php echo e($i['title']); ?>

                                                                                                                        <input type="checkbox"
                                                                                                                               name="<?php echo e($v['area']['title']); ?>"
                                                                                                                               value-title="<?php echo e($i['title']); ?>"
                                                                                                                               value="<?php echo e($i['province_id']); ?>">
                                                                                                                        <span class="checkmark"></span>
                                                                                                                    </label>
                                                                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </li>
                                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-6 col-12 mx-auto">
                                                        <div class="wp-select">
                                                            <div class="row">
                                                                <div class='col-12'>
                                                                    <div class="form-group">
                                                                        <div class="box-select">

                                                                            <select class="multiple-select-2"
                                                                                    name="places[]" multiple="multiple">
                                                                                <?php $__currentLoopData = $data['places']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                    <option value="<?php echo e($v->id); ?>"
                                                                                            data-title="<?php echo e($v->title); ?>"
                                                                                            class="option"><?php echo e($v->title); ?></option>
                                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                            </select>
                                                                            <span class="icon-select"><i
                                                                                        class="fas fa-angle-down"></i></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-6 col-12 mx-auto">
                                                        <div class="wp-select">
                                                            <div class="row">
                                                                <div class='col-12'>
                                                                    <div class="form-group">
                                                                        <div class="box-select">
                                                                            <select class="multiple-select-2"
                                                                                    name="requests[]"
                                                                                    multiple="multiple">
                                                                                <?php $__currentLoopData = $data['requests']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                    <option value="<?php echo e($v->id); ?>"
                                                                                            data-title="<?php echo e($v->title); ?>"
                                                                                            class="option"><?php echo e($v->title); ?></option>
                                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                            </select>
                                                                            <span class="icon-select"><i
                                                                                        class="fas fa-angle-down"></i></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="result">
                                                    <h4 class="result-title"></h4>
                                                    <div class="result-text"></div>
                                                    <div class="result-text-places"></div>
                                                    <div class="result-text-requests"></div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>

                                <div class="row pd-50">
                                    <div class="col-12 text-center ">
                                        <label for="">
                                            <h4 class="title-select heading-3"> <?php echo e(getLanguage('SpecialRequirements')); ?> </h4>
                                        </label>
                                        <textarea name="special_requirements" cols="30" rows="10"
                                                  class="w-100 p-3"
                                                  placeholder="Ví dụ: Đoàn có người ăn chay, Dị ứng thực phẩm, Bệnh tim hoặc thể trạng yếu…"></textarea>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="request-quote ">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-lg-8 col-md-10 col-sm-12 col-12 m-auto">
                            <div class="content-request-quote">

                                <div class="panel-head-travel-package panel-head-general text-center">
                                    <div class="row">
                                        <div class="col-12 m-auto">
                                            <h2 class="heading-2"><?php echo e(getLanguage('PersonalInfomation')); ?></h2>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel-body-travel-package">
                                    <form class="form-sakura">
                                        <div class="row">
                                            <input type="hidden" name="token" value="<?php echo e(csrf_token()); ?>">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group-sakura">
                                                            <input type="text" placeholder="<?php echo getLanguage('FullName') ?>" name="fullName"
                                                                   class="input-text w-100">
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group-sakura">
                                                            <input type="text" placeholder="<?php echo getLanguage('Email') ?>" name="email"
                                                                   class="input-text w-100">
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group-sakura">
                                                            <input type="text" placeholder="<?php echo getLanguage('Phone') ?>" name="phone"
                                                                   class="input-text w-100">
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group-sakura">
                                                            <input type="text" placeholder="<?php echo getLanguage('Address') ?>" name="address"
                                                                   class="input-text w-100">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group-sakura">
                                                            <textarea name="node" class="w-100 p-2 textarea" rows="8"
                                                                      placeholder="<?php echo getLanguage('Note') ?>"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-12" style="margin-top: -1px">
                                                        <div class="form-group-sakura">
                                                            <button style="cursor: pointer;" class="btn-submit input-text w-100"><?php echo e(getLanguage('BookTour')); ?></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
    <?php echo $footer; ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
    <script type="text/javascript" src="/js/moment.min.js"></script>
    <script type="text/javascript" src="/js/daterangepicker.min.js"></script>
    <script type="text/javascript" src="/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {

            $('input[name="expected_departure_date"]').daterangepicker({
                singleDatePicker: true,
                locale: {
                    format: 'YYYY/MM/DD'
                }
            });

            function matchCustom(params, data) {
                if ($.trim(params.term).toLowerCase() === '') {
                    return data;
                }
                if (typeof data.text.toLowerCase() === 'undefined') {
                    return null;
                }
                if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
                    var modifiedData = $.extend({}, data, true);
                    return modifiedData;
                }
                return null;
            }

            $('select[name*=places]').select2({
                placeholder: "<?php echo getLanguage('FamousPlaces'); ?>",
                matcher: matchCustom
            });
            $('select[name*=request]').select2({
                placeholder: "<?php echo getLanguage('Requests'); ?>",
                matcher: matchCustom
            });
            $('select[name*=places]').on('select2:close', function () {
                var uldiv = $(this).siblings('span.select2').find('ul')
                let arr = [];
                $(this).find('option:selected').each((i, v) => {
                    arr.push(v.getAttribute('data-title'));
                })
                if (arr.length) {
                    $('.result-text-places').text(` `);
                    $('.result-text-places').append(`<hr><div> Địa điểm nổi tiếng > ${arr.join(', ')}</div>`);
                } else {
                    $('.result-text-places').text(` `);
                }
                var count = uldiv.find('li').length - 1;
                if (count) {
                    uldiv.html("<li> Đã chọn " + count + "</li>")
                }
            });

            $('select[name*=request]').on('select2:close', function () {
                var uldiv = $(this).siblings('span.select2').find('ul')
                let arr = [];
                $(this).find('option:selected').each((i, v) => {
                    arr.push(v.getAttribute('data-title'));
                })
                if (arr.length) {
                    $('.result-text-requests').text(` `);
                    $('.result-text-requests').append(`<hr><div> Yêu cầu > ${arr.join(', ')}</div>`);
                } else {
                    $('.result-text-requests').text(` `);
                }
                var count = uldiv.find('li').length - 1;
                if (count) {
                    uldiv.html("<li> Đã chọn " + count + "</li>")
                }
            });

            let obj = {};
            let list = $('.list-select-area .item .text');
            list.each((k, v) => {
                obj[v.innerText] = []
            })

            function checkHasObjData(obj) {
                for (let v in obj) {
                    if (obj[v].length) {
                        return true;
                    }
                }
                return false;
            }

            $('.item-check').find('input[type=checkbox]').on('change', function () {
                let name = $(this).attr('name');
                if ($(this)[0].checked) {
                    obj[name].push($(this)[0].getAttribute('value-title'))
                } else {
                    obj[name].splice(obj[name].indexOf($(this)[0].getAttribute('value-title')), 1);
                }
                if (checkHasObjData(obj)) {
                    $('.result-title').text('Bạn sẽ tới:');
                    $('.result-text').text('');
                    for (let v in obj) {
                        if (obj[v].length) {
                            let arr = obj[v].join(', ');
                            $('.result-text').append(`<div>${v} > ${arr}</div>`);
                        }
                    }
                } else {
                    $('.result-title').text('');
                    $('.result-text').text('');
                }
            })

            $('.btn-submit').click(function (e) {
                e.preventDefault();
                $('.form-sakura').find('.alert').remove();
                let provinces = [];
                $('.select-level-2').find('input[type=checkbox]').each((k, v) => {
                    if (v.checked) {
                        provinces.push(v.value);
                    }
                })
                let criterias = {
                    'provinces': provinces,
                    'places': $('select[name*=places]').val(),
                    'requests': $('select[name*=requests]').val()
                }
                let formData = {
                    '_token': $('input[name=token]').val(),
                    'expected_departure_date': $('input[name=expected_departure_date]').val(),
                    'expected_number_of_days': $('select[name=expected_number_of_days]').val(),
                    'departure_from_id': $('select[name=departure_from_id]').val(),
                    'number_of_adults': $('select[name=number_of_adults]').val(),
                    'number_of_children': $('select[name=number_of_children]').val(),
                    'number_of_babies': $('select[name=number_of_babies]').val(),
                    'criterias': criterias,
                    'special_requirements': $('textarea[name=special_requirements]').val(),
                    'fullName': $('input[name=fullName]').val(),
                    'email': $('input[name=email]').val(),
                    'phone': $('input[name=phone]').val(),
                    'address': $('input[name=address]').val(),
                    'node': $('textarea[name=node]').val()
                };
                $.ajax({
                    type: 'POST',
                    url: '/quickQuote',
                    data: formData,
                    dataType: 'json',
                    encode: true
                }).done(function(data) {
                    if (!data.success) {
                        for (i in data.errors) {
                            $('.form-sakura').append('<div class="alert alert-danger">' + data.errors[i] + '</div>');
                        }
                    } else {
                        $('.form-sakura').find("input, textarea").val('');
                        $('.form-sakura').append('<div class="alert alert-success">' + data.message + '</div>');
                    }
                });
            })
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('client.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\Amela\sakura.amela\resources\views/client/page/quickQuote.blade.php ENDPATH**/ ?>