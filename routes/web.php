<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/maintenance', ['as' => 'maintenance', 'uses' => 'Client\MaintenanceController@index']);
Route::get('/give-me-csrf', 'Client\CsrfTokenController@index')->name('giveMeCsrf');
Route::post('/give-me-csrf', function () {
    return redirect()->route('home');
});
// ---------- Manager Post-----------//
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/', ['as' => 'admin', 'uses' => 'AdminController@index']);
});
Route::group(['namespace' => 'Client', 'middleware' => ['maintenance', 'cachePage']], function () {
    Route::get('/contact', 'ContactController@index')->name('contact');
    Route::get('/introduction', 'InformationController@introduction')->name('introduction');
    Route::get('/answerQuestion', 'AnswerQuestionController@index')->name('answerQuestion');
    Route::post('/lienheSakura', 'ContactController@contact')->name('lienhe');
    Route::post('/languages', 'LanguageController@index')->name('languages');
    Route::get('/reviews', 'ReviewsController@index')->name('reviews');
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/getBanner', 'HomeController@getBanner')->name('get-banner');
    Route::get('/quickQuote', 'QuickQuoteController@index')->name('quick-quote');
    Route::post('/quickQuote', function () {
        return redirect()->route('home');
    });
    Route::post('/sendQuickQuote', 'QuickQuoteController@store');
    Route::get('/sendQuickQuote', function () {
        return redirect()->route('home');
    });
    Route::get('/c{id}', 'CategoryController@index')->name('category');
    Route::get('/p{id}', 'ProductController@index')->name('product');
    Route::get('/i{id}', 'InformationController@index')->name('information');
    Route::get('/bc{id}', 'BlogCategoryController@index')->name('blogCategory');
    Route::get('/bp{id}', 'BlogPostController@index')->name('blogPost');
    Route::get('/rv{id}', 'ReviewsController@getReviews')->name('detailReviews');
    Route::get('/lienheSakura', function () {
        return redirect()->route('home');
    });
    Route::get('/languages', function () {
        return redirect()->route('home');
    });
});
