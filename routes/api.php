<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => 'Api', 'middleware' => 'api'], function () {
    Route::post('login', 'Auth\AuthController@login');
    Route::post('system/init', 'SettingController@init');
    Route::group(['prefix' => 'password'], function () {
        Route::post('create', 'Auth\PasswordResetController@create')->name('auth.create');
        Route::get('find/{token}', 'Auth\PasswordResetController@find')->name('auth.find');
        Route::post('reset', 'Auth\PasswordResetController@reset')->name('auth.reset');
    });
    Route::get('crawl', 'EmployeeControler@crawl');
});

Route::group([
    'namespace' => 'Api',
    'middleware' => 'api'
], function () {
    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        Route::post('logout', 'Auth\AuthController@logout');
        Route::post('images/upload', 'UploadImageController@uploadImage');
        Route::post('images/delete', 'UploadImageController@deleteImage');
        Route::group(['prefix' => 'language'], function () {
            Route::post('detailLanguage', 'LanguageController@detailLanguage')->name('language.detail');
            Route::post('saveLanguage', 'LanguageController@saveLanguage')->name('language.save');
            Route::post('searchLanguage', 'LanguageController@searchLanguage')->name('language.search');
            Route::post('activeLanguage', 'LanguageController@getActiveLanguage')->name('language.active');
            Route::post('saveLanguageStatic', 'LanguageController@saveLanguageStatic')->name('language.saveLanguageStatic');
            Route::post('searchLanguageStatic', 'LanguageController@searchLanguageStatic')->name('language.searchLanguageStatic');
            Route::post('saveLanguageStaticByLang', 'LanguageController@saveLanguageStaticByLang')->name('language.saveLanguageStaticByLang');
            Route::post('deleteLanguage', 'LanguageController@deleteLanguage')->name('language.delete');
        });
        Route::group(['prefix' => 'category'], function () {
            Route::post('detailCategory', 'CategoryController@detailCategory')->name('category.detail');
            Route::post('saveCategory', 'CategoryController@saveCategory')->name('category.save');
            Route::post('searchCategory', 'CategoryController@searchCategory')->name('category.search');
            Route::post('deleteCategory', 'CategoryController@deleteCategory')->name('category.delete');
        });
        Route::group(['prefix' => 'goodPoint'], function () {
            Route::post('detailGoodPoint', 'GoodPointController@detailGoodPoint')->name('goodPoint.detail');
            Route::post('saveGoodPoint', 'GoodPointController@saveGoodPoint')->name('goodPoint.save');
            Route::post('searchGoodPoint', 'GoodPointController@searchGoodPoint')->name('goodPoint.search');
            Route::post('deleteGoodPoint', 'GoodPointController@deleteGoodPoint')->name('goodPoint.delete');
        });
        Route::group(['prefix' => 'product'], function () {
            Route::post('detailProduct', 'ProductController@detailProduct')->name('product.detail');
            Route::post('saveProduct', 'ProductController@saveProduct')->name('product.save');
            Route::post('searchProduct', 'ProductController@searchProduct')->name('product.search');
            Route::post('deleteProduct', 'ProductController@deleteProduct')->name('product.delete');
        });
        Route::group(['prefix' => 'information'], function () {
            Route::post('detailInformation', 'InformationController@detailInformation')->name('information.detail');
            Route::post('saveInformation', 'InformationController@saveInformation')->name('information.save');
            Route::post('searchInformation', 'InformationController@searchInformation')->name('information.search');
            Route::post('deleteInformation', 'InformationController@deleteInformation')->name('information.delete');
        });
        Route::group(['prefix' => 'reviews'], function () {
            Route::post('detailReviews', 'ReviewsController@detailReviews')->name('reviews.detail');
            Route::post('saveReviews', 'ReviewsController@saveReviews')->name('reviews.save');
            Route::post('searchReviews', 'ReviewsController@searchReviews')->name('reviews.search');
            Route::post('deleteReviews', 'ReviewsController@deleteReviews')->name('reviews.delete');
        });
        Route::group(['prefix' => 'banner'], function () {
            Route::post('detailBanner', 'BannerController@detailBanner')->name('banner.detail');
            Route::post('saveBanner', 'BannerController@saveBanner')->name('banner.save');
            Route::post('searchBanner', 'BannerController@searchBanner')->name('banner.search');
            Route::post('deleteBanner', 'BannerController@deleteBanner')->name('banner.delete');
            Route::post('getBannerByGroupId', 'BannerController@getBannerByGroupId')->name('banner.getBannerByGroupId');

            Route::post('detailBannerGroup', 'BannerController@detailBannerGroup')->name('banner.detailGroup');
            Route::post('saveBannerGroup', 'BannerController@saveBannerGroup')->name('banner.saveGroup');
            Route::post('searchBannerGroup', 'BannerController@searchBannerGroup')->name('banner.searchGroup');
            Route::post('deleteBannerGroup', 'BannerController@deleteBannerGroup')->name('banner.deleteGroup');
        });
        Route::group(['prefix' => 'area'], function () {
            Route::post('detailArea', 'AreaController@detailArea')->name('area.detail');
            Route::post('saveArea', 'AreaController@saveArea')->name('area.save');
            Route::post('searchArea', 'AreaController@searchArea')->name('area.search');
            Route::post('deleteArea', 'AreaController@deleteArea')->name('area.delete');
        });
        Route::group(['prefix' => 'province'], function () {
            Route::post('detailProvince', 'ProvinceController@detailProvince')->name('province.detail');
            Route::post('saveProvince', 'ProvinceController@saveProvince')->name('province.save');
            Route::post('searchProvince', 'ProvinceController@searchProvince')->name('province.search');
            Route::post('deleteProvince', 'ProvinceController@deleteProvince')->name('province.delete');
        });
        Route::group(['prefix' => 'place'], function () {
            Route::post('detailPlace', 'PlaceController@detailPlace')->name('place.detail');
            Route::post('savePlace', 'PlaceController@savePlace')->name('place.save');
            Route::post('searchPlace', 'PlaceController@searchPlace')->name('place.search');
            Route::post('deletePlace', 'PlaceController@deletePlace')->name('place.delete');
        });
        Route::group(['prefix' => 'request'], function () {
            Route::post('detailRequest', 'RequestCmsController@detailRequest')->name('request.detail');
            Route::post('saveRequest', 'RequestCmsController@saveRequest')->name('request.save');
            Route::post('searchRequest', 'RequestCmsController@searchRequest')->name('request.search');
            Route::post('deleteRequest', 'RequestCmsController@deleteRequest')->name('request.delete');
        });
        Route::group(['prefix' => 'quickQuote'], function () {
            Route::post('detailQuickQuote', 'QuickQuoteController@detailQuickQuote')->name('quickQuote.detail');
            Route::post('searchQuickQuote', 'QuickQuoteController@searchQuickQuote')->name('quickQuote.search');
            Route::post('deleteQuickQuote', 'QuickQuoteController@deleteQuickQuote')->name('quickQuote.delete');
        });
        Route::group(['prefix' => 'contact'], function () {
            Route::post('detailContact', 'ContactController@detailContact')->name('contact.detail');
            Route::post('searchContact', 'ContactController@searchContact')->name('contact.search');
            Route::post('readContact', 'ContactController@readContact')->name('contact.read');
        });
        Route::group(['prefix' => 'attribute'], function () {
            Route::post('detailAttribute', 'AttributeController@detailAttribute')->name('attribute.detail');
            Route::post('saveAttribute', 'AttributeController@saveAttribute')->name('attribute.save');
            Route::post('searchAttribute', 'AttributeController@searchAttribute')->name('attribute.search');
            Route::post('deleteAttribute', 'AttributeController@deleteAttribute')->name('attribute.delete');
        });
        Route::group(['prefix' => 'day'], function () {
            Route::post('detailDay', 'DayController@detailDay')->name('day.detail');
            Route::post('saveDay', 'DayController@saveDay')->name('day.save');
            Route::post('searchDay', 'DayController@searchDay')->name('day.search');
            Route::post('deleteDay', 'DayController@deleteDay')->name('day.delete');
        });
        Route::group(['prefix' => 'menu'], function () {
            Route::post('getTreeMenu', 'MenuController@getTreeMenu')->name('menu.getTreeMenu');
            Route::post('detailMenu', 'MenuController@detailMenu')->name('menu.detailMenu');
            Route::post('saveMenu', 'MenuController@saveMenu')->name('menu.save');
            Route::post('deleteMenu', 'MenuController@deleteMenu')->name('menu.delete');
            Route::post('saveOrderMenu', 'MenuController@saveOrderMenu')->name('menu.saveOrder');
            Route::post('importCategory', 'MenuController@importCategory')->name('menu.importCategory');
            Route::post('deleteCategory', 'MenuController@deleteCategory')->name('menu.deleteCategory');

            Route::post('searchMenuGroup', 'MenuController@searchMenuGroup')->name('menu.searchMenuGroup');
            Route::post('detailMenuGroup', 'MenuController@detailMenuGroup')->name('menu.detailMenuGroup');
            Route::post('saveMenuGroup', 'MenuController@saveMenuGroup')->name('menu.saveMenuGroup');
            Route::post('deleteMenuGroup', 'MenuController@deleteMenuGroup')->name('menu.deleteMenuGroup');
        });
        Route::group(['prefix' => 'blogCategory'], function () {
            Route::post('detailBlogCategory', 'BlogCategoryController@detailBlogCategory')->name('blogCategory.detail');
            Route::post('saveBlogCategory', 'BlogCategoryController@saveBlogCategory')->name('blogCategory.save');
            Route::post('searchBlogCategory', 'BlogCategoryController@searchBlogCategory')->name('blogCategory.search');
            Route::post('deleteBlogCategory', 'BlogCategoryController@deleteBlogCategory')->name('blogCategory.delete');
        });
        Route::group(['prefix' => 'blogPost'], function () {
            Route::post('detailBlogPost', 'BlogPostController@detailBlogPost')->name('blogPost.detail');
            Route::post('saveBlogPost', 'BlogPostController@saveBlogPost')->name('blogPost.save');
            Route::post('searchBlogPost', 'BlogPostController@searchBlogPost')->name('blogPost.search');
            Route::post('deleteBlogPost', 'BlogPostController@deleteBlogPost')->name('blogPost.delete');
        });
        Route::group(['prefix' => 'setting'], function () {
            Route::post('getSetting', 'SettingController@getSetting')->name('setting.get');
            Route::post('saveSetting', 'SettingController@saveSetting')->name('setting.save');
        });
    });
});
Route::group([
    'middleware' => 'api',
    'namespace' => 'Api\Extension',
], function () {
    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        Route::group(['prefix' => 'extension'], function () {
            Route::post('searchExtension', 'ExtensionController@searchExtension')->name('extension.getList');
            Route::post('deleteExtension', 'ExtensionController@deleteExtension')->name('extension.delete');
        });
    });
});
Route::group([
    'middleware' => 'api',
    'namespace' => 'Api\Extension\Module',
], function () {
    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        Route::group(['prefix' => 'module'], function () {
            Route::post('detailModule', 'ModuleController@detailModule')->name('module.detail');
            Route::post('saveModule', 'ModuleController@saveModule')->name('module.save');
            Route::post('deleteModule', 'ModuleController@deleteModule')->name('module.delete');
        });
    });
});
Route::group([
    'middleware' => 'api'
], function () {
    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        Route::post('role', 'RoleController@index');
        Route::post('role/detailRole', 'RoleController@detailRole');
        Route::post('role/saveRole', 'RoleController@saveRole');
        Route::post('role/savePermissionRole', 'RoleController@savePermissionRole');
        Route::post('role/searchRole', 'RoleController@index');
        Route::post('role/deleteRole', 'RoleController@deleteRole');

        Route::post('permission', 'Api\Auth\AuthController@getPermission');
        Route::post('saveUser', 'Api\Auth\AuthController@saveUser');
        Route::post('deleteUser', 'Api\Auth\AuthController@deleteUser');
        Route::post('listUser', 'Api\Auth\AuthController@listUser');
        Route::post('detailUser', 'Api\Auth\AuthController@detailUser');
        Route::post('history/login', 'Api\Auth\AuthController@history');
        Route::post('changePass', 'Api\Auth\AuthController@changePass');
        Route::post('deleteCache', 'Api\SettingController@deleteCache');
    });
});

