import Vue from 'vue';
import VueRouter from 'vue-router';

const _import = require('./_import_sync');
import App from '../components/app.vue';
import AppBasic from '../components/app-basic.vue';
import store from '../vuex/index';
import CONSTANTS from '../core/utils/constants';
import ENUM from "../../config/enum";

Vue.use(VueRouter);
let _routers = [
    {
        path: '/',
        component: App,
        children: [
            {
                path:'/logout',
                component: _import('auth/logout'),
                meta:{
                    requiresAuth: true,
                }
            },
            {
                path:'/user',
                component: _import('user/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleSystemSettingCan
                }
            },
            {
                path:'/role',
                component: _import('role/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleSystemSettingCan
                }
            },
            {
                path:'/language',
                component: _import('languages/language/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleSystemSettingCan
                }
            },
            {
                path:'/languageKey',
                component: _import('languages/languageKey/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleSystemSettingCan
                }
            },
            {
                path:'/area',
                component: _import('area/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleSystemSettingCan
                }
            },
            {
                path:'/province',
                component: _import('province/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleSystemSettingCan
                }
            },
            {
                path:'/place',
                component: _import('place/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleSystemSettingCan
                }
            },
            {
                path:'/request',
                component: _import('request/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleSystemSettingCan
                }
            },
            {
                path:'/quickQuote',
                component: _import('quickQuote/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleQuickQuoteCan
                }
            },
            {
                path:'/quickQuote/detail/:id',
                component: _import('quickQuote/detail'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleQuickQuoteCan
                }
            },
            {
                path:'/banner',
                component: _import('banner/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleBannerCan
                }
            },
            {
                path:'/banner/add',
                component: _import('banner/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleBannerCan
                }
            },
            {
                path:'/banner/edit/:id/type/:type',
                component: _import('banner/editListSlider'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleBannerCan
                }
            },
            {
                path:'/menu',
                component: _import('menu/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleMenuCan
                }
            },
            {
                path:'/menu/edit/:id',
                component: _import('menu/editTree'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleMenuCan
                }
            },
            {
                path:'/product',
                component: _import('product/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogRead
                }
            },
            {
                path:'/product/add',
                component: _import('product/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogWrite
                }
            },
            {
                path:'/product/edit/:id',
                component: _import('product/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogWrite
                }
            },
            {
                path:'/category',
                component: _import('category/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogRead
                }
            },
            {
                path:'/category/add',
                component: _import('category/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogWrite
                }
            },
            {
                path:'/category/edit/:id',
                component: _import('category/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogWrite
                }
            },
            {
                path:'/attribute',
                component: _import('attribute/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogRead
                }
            },
            {
                path:'/day',
                component: _import('day/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogRead
                }
            },
            {
                path:'/goodPoint',
                component: _import('goodPoint/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogRead
                }
            },
            {
                path:'/information',
                component: _import('information/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogRead
                }
            },
            {
                path:'/information/add',
                component: _import('information/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogWrite
                }
            },
            {
                path:'/information/edit/:id',
                component: _import('information/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogWrite
                }
            },
            {
                path:'/contact',
                component: _import('contact/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogRead
                }
            },
            {
                path:'/contact/edit/:id',
                component: _import('contact/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogWrite
                }
            },
            {
                path:'/extension',
                component: _import('extension/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogRead
                }
            },
            {
                path:'/extension/module/featured',
                component: _import('extension/module/featured/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogWrite
                }
            },
            {
                path:'/extension/module/featured/:id',
                component: _import('extension/module/featured/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogWrite
                }
            },
            {
                path:'/extension/module/interesting',
                component: _import('extension/module/interesting/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogWrite
                }
            },
            {
                path:'/extension/module/interesting/:id',
                component: _import('extension/module/interesting/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogWrite
                }
            },
            {
                path:'/extension/module/category',
                component: _import('extension/module/category/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogWrite
                }
            },
            {
                path:'/extension/module/category/:id',
                component: _import('extension/module/category/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogWrite
                }
            },
            {
                path:'/extension/module/blogCategory',
                component: _import('extension/module/blog/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogWrite
                }
            },
            {
                path:'/extension/module/blogCategory/:id',
                component: _import('extension/module/blog/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogWrite
                }
            },
            {
                path:'/extension/module/html',
                component: _import('extension/module/html/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogWrite
                }
            },
            {
                path:'/extension/module/html/:id',
                component: _import('extension/module/html/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogWrite
                }
            },
            {
                path:'/blogPost',
                component: _import('blogPost/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleNewsRead
                }
            },
            {
                path:'/blogPost/add',
                component: _import('blogPost/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleNewsWrite
                }
            },
            {
                path:'/blogPost/edit/:id',
                component: _import('blogPost/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleNewsWrite
                }
            },
            {
                path:'/reviews',
                component: _import('reviews/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogRead
                }
            },
            {
                path:'/reviews/add',
                component: _import('reviews/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogWrite
                }
            },
            {
                path:'/reviews/edit/:id',
                component: _import('reviews/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleCatalogWrite
                }
            },
            {
                path:'/blogCategory',
                component: _import('blogCategory/index'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleNewsRead
                }
            },
            {
                path:'/blogCategory/add',
                component: _import('blogCategory/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleNewsWrite
                }
            },
            {
                path:'/blogCategory/edit/:id',
                component: _import('blogCategory/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleNewsWrite
                }
            },
            {
                path:'/setting',
                component: _import('setting/edit'),
                meta:{
                    requiresAuth: true,
                    permission: ENUM.Permission.RoleSystemSettingCan
                }
            },
        ]
    },
    {
        path: '/',
        component: AppBasic,
        children: [
            {
                path: '/login',
                component: _import('auth/login')
            }
        ]
    },
    {
        path: '*',
        component: App,
        children: [
            {
                path: '/',
                component: _import('_shared/404')
            },
            {
                path: '/not-allow-access',
                component: _import('_shared/not-allow-access')
            }
        ]
    }
];
const router = new VueRouter({
    errorHandler(to, from, next, error) {
    },
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition
        } else {
            return {x: 0, y: 0}
        }
    },
    routes: _routers
});
router.beforeEach((to, from, next) => {

    if (to.matched.some(record => record.meta.requiresAuth)) {
        try {
            if (!process.env.VUE_ENV) {
                if (localStorage.getItem(CONSTANTS.ACCESS_TOKEN)) {
                    if(to.meta.permission){
                        if(localStorage.getItem(CONSTANTS.ACCESS_PERMISSION).includes(to.meta.permission)) {
                            return next();
                        }else{
                            router.push({path: '/not-allow-access'});
                        }
                    }else{
                        return next();
                    }
                } else {
                    router.push({path: '/login', query: {returnUrl: to.fullPath}});
                }
            } else {
                if(to.meta.permission){
                    if(localStorage.getItem(CONSTANTS.ACCESS_PERMISSION).includes(to.meta.permission)) {
                        return next();
                    }else{
                        router.push({path: '/not-allow-access'});
                    }
                }else{
                    return next();
                }
            }
        } catch (e) {
            return next();
        }
    } else {
        return next();
    }
});

export default router;
