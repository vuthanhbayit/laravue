require('./bootstrap');

import Vue from 'vue';
import store from './vuex/index';
import router from './router';
import Entry from './components/entry.vue';
import Avatar from './components/_common/avatar.vue';
import PhotoSelect from './components/_common/photo-select.vue';
import Photo from './components/_common/photo.vue';
import Modal from './components/_common/modal.vue';
import Pager from './components/_common/pager.vue';
import Wrapper from './components/_common/wrapper.vue';
import TinyMce from './components/_common/tiny-mce.vue';
import DateTimePicker from './components/_common/datetimepicker.vue';
import checkPermission from './core/mixins/checkPermission';
import alert from './core/plugins/alert';
import appSettings from './core/mixins/appSettings';
import clickOutside from './core/directives/clickOutsite';
import metaInfo from "./core/mixins/metaInfo";
import CurrentInfo from './core/mixins/currentInfo';
import multiLanguage from './core/mixins/multiLanguage';
import Notifications from 'vue-notification';
import VueNestable from 'vue-nestable';
import Select2 from 'v-select2-component';
import {Option, Select, Upload} from 'element-ui';

var Loading = null;
import 'vue-loading-overlay/dist/vue-loading.css';
if (typeof (window) !== 'undefined') {
    Loading = require('vue-loading-overlay');
}
Vue.mixin(metaInfo);
Vue.mixin(checkPermission);
Vue.mixin(multiLanguage);
Vue.mixin(CurrentInfo);
Vue.directive('click-outside', clickOutside);
Vue.use(Notifications);
Vue.use(Loading);
Vue.use(checkPermission);
Vue.use(VueNestable);
Vue.component('tiny-mce', TinyMce);
Vue.component('el-upload', Upload);
Vue.component('el-select', Select);
Vue.component('el-option', Option);
Vue.component('select2', Select2);
Vue.component('datetimepicker', DateTimePicker);
Vue.component('avatar', Avatar);
Vue.component('photo-select', PhotoSelect);
Vue.component('photo', Photo);
Vue.component('modal', Modal);
Vue.component('wrapper', Wrapper);
Vue.component('pager', Pager);
const app = new Vue({
    el: '#app',
    router,
    store,
    ...Entry
});
if (!process.env.VUE_ENV)
    window.app = app;
export {app, router, store};

