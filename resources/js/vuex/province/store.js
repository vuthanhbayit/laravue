import * as actions from './actions';

const store = {
    state: {
        listProvince: null,
        provinceCount: 0,
    },
    mutations: {
        GET_LIST_PROVINCE: (state, payload) => {
            state.listProvince = payload.data;
            state.provinceCount = payload.totalRow;
        },
    },
    actions,
    getters: {
        listProvince: state => state.listProvince,
        provinceCount: state => state.provinceCount,
    }
};
export default store;
