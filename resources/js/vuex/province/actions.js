import axios from '../../core/plugins/http';

export const getListProvince = ({commit}, opts) => {
    return new Promise ((resolve, reject) => {
        return axios({
            data: {
                url: '/api/province/searchProvince',
                ...opts
            }
        }).then(response =>{
            commit("GET_LIST_PROVINCE", response);
            return resolve(response);
        }).catch(err =>{
            commit("GET_LIST_PROVINCE", null);
            return reject(err);
        })
    })
};
export const getDetailProvince = ({commit}, opts) =>{
    return new Promise ((resolve, reject) =>{
        return axios({
            data: {
                url: '/api/province/detailProvince',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(err => {
            return resolve(err);
        })
    })
};
export const saveProvince = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/province/saveProvince',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
export const deleteProvince = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/province/deleteProvince',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
