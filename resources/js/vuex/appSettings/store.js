﻿import * as actions from './actions';
import CONSTANTS from '../../core/utils/constants';

let appSettings = null;
if (typeof (window) !== 'undefined') {
    appSettings = window.appSettings;
}
const store = {
    state: {
        appSettings: appSettings
    },
    mutations: {
        UPDATE_APP_SETTINGS: (state, payload) => {
            state.appSettings = payload;
        }
    },
    actions,
    getters: {
        appSettings: state => state.appSettings
    }
};

export default store;
