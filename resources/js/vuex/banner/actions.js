import axios from '../../core/plugins/http';

export const getListBanner = ({commit}, opts) => {
    return new Promise ((resolve, reject) => {
        return axios({
            data: {
                url: '/api/banner/searchBanner',
                ...opts
            }
        }).then(response =>{
            commit("GET_LIST_BANNER", response);
            return resolve(response);
        }).catch(err =>{
            commit("GET_LIST_BANNER", null);
            return reject(err);
        })
    })
};
export const getDetailBanner = ({commit}, opts) =>{
    return new Promise ((resolve, reject) =>{
        return axios({
            data: {
                url: '/api/banner/detailBanner',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(err => {
            return resolve(err);
        })
    })
};
export const getBannerByGroupId = ({commit}, opts) =>{
    return new Promise ((resolve, reject) =>{
        return axios({
            data: {
                url: '/api/banner/getBannerByGroupId',
                ...opts
            }
        }).then(response => {
            commit("GET_LIST_BANNER_BY_GROUP_ID", response);
            return resolve(response);
        }).catch(err => {
            commit("GET_LIST_BANNER_BY_GROUP_ID", response);
            return resolve(err);
        })
    })
};
export const saveBanner = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/banner/saveBanner',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
export const deleteBanner = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/banner/deleteBanner',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
