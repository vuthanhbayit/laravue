import * as actions from './actions';

const store = {
    state: {
        listBanner: null,
        listBannerByGroupId: null,
        bannerCount: 0,
    },
    mutations: {
        GET_LIST_BANNER: (state, payload) => {
            state.listBanner = payload.data;
            state.bannerCount = payload.totalRow;
        },
        GET_LIST_BANNER_BY_GROUP_ID: (state, payload) => {
            state.listBannerByGroupId = payload.data;
        },
    },
    actions,
    getters: {
        listBanner: state => state.listBanner,
        listBannerByGroupId: state => state.listBannerByGroupId,
        bannerCount: state => state.bannerCount,
    }
};
export default store;
