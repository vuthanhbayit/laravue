import axios from '../../core/plugins/http';

export const getListAttribute = ({commit}, opts) => {
    return new Promise ((resolve, reject) => {
        return axios({
            data: {
                url: '/api/attribute/searchAttribute',
                ...opts
            }
        }).then(response =>{
            commit("GET_LIST_ATTRIBUTE", response);
            return resolve(response);
        }).catch(err =>{
            commit("GET_LIST_ATTRIBUTE", null);
            return reject(err);
        })
    })
};
export const getDetailAttribute = ({commit}, opts) =>{
    return new Promise ((resolve, reject) =>{
        return axios({
            data: {
                url: '/api/attribute/detailAttribute',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(err => {
            return resolve(err);
        })
    })
};
export const saveAttribute = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/attribute/saveAttribute',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
export const deleteAttribute = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/attribute/deleteAttribute',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
