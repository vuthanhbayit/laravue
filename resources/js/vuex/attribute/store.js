import * as actions from './actions';

const store = {
    state: {
        listAttribute: null,
        attributeCount: 0,
    },
    mutations: {
        GET_LIST_ATTRIBUTE: (state, payload) => {
            state.listAttribute = payload.data;
            state.attributeCount = payload.totalRow;
        },
    },
    actions,
    getters: {
        listAttribute: state => state.listAttribute,
        attributeCount: state => state.attributeCount,
    }
};
export default store;
