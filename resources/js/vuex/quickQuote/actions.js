import axios from '../../core/plugins/http';

export const getListQuickQuote = ({commit}, opts) => {
    return new Promise ((resolve, reject) => {
        return axios({
            data: {
                url: '/api/quickQuote/searchQuickQuote',
                ...opts
            }
        }).then(response =>{
            commit("GET_LIST_QUICKQUOTE", response);
            return resolve(response);
        }).catch(err =>{
            commit("GET_LIST_QUICKQUOTE", null);
            return reject(err);
        })
    })
};
export const getDetailQuickQuote = ({commit}, opts) =>{
    return new Promise ((resolve, reject) =>{
        return axios({
            data: {
                url: '/api/quickQuote/detailQuickQuote',
                ...opts
            }
        }).then(response => {
            commit("GET_DETAIL_QUICKQUOTE", response);
            return resolve(response);
        }).catch(err => {
            commit("GET_DETAIL_QUICKQUOTE", null);
            return reject(err);
        })
    })
};
export const deleteQuickQuote = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/quickQuote/deleteQuickQuote',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
