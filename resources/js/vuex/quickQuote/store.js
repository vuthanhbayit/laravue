import * as actions from './actions';

const store = {
    state: {
        listQuickQuote: null,
        quickQuoteCount: 0,
        detailQuickQuote: null,
    },
    mutations: {
        GET_LIST_QUICKQUOTE: (state, payload) => {
            state.listQuickQuote = payload.data;
            state.quickQuoteCount = payload.totalRow;
        },
        GET_DETAIL_QUICKQUOTE: (state, payload) => {
            state.detailQuickQuote = payload.data;
        }
    },
    actions,
    getters: {
        listQuickQuote: state => state.listQuickQuote,
        quickQuoteCount: state => state.quickQuoteCount,
        detailQuickQuote: state => state.detailQuickQuote,
    }
};
export default store;
