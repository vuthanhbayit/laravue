import * as actions from './actions';

const store = {
    state: {
        listPlace: null,
        placeCount: 0,
    },
    mutations: {
        GET_LIST_PLACE: (state, payload) => {
            state.listPlace = payload.data;
            state.placeCount = payload.totalRow;
        },
    },
    actions,
    getters: {
        listPlace: state => state.listPlace,
        placeCount: state => state.placeCount,
    }
};
export default store;
