import axios from '../../core/plugins/http';

export const getListPlace = ({commit}, opts) => {
    return new Promise ((resolve, reject) => {
        return axios({
            data: {
                url: '/api/place/searchPlace',
                ...opts
            }
        }).then(response =>{
            commit("GET_LIST_PLACE", response);
            return resolve(response);
        }).catch(err =>{
            commit("GET_LIST_PLACE", null);
            return reject(err);
        })
    })
};
export const getDetailPlace = ({commit}, opts) =>{
    return new Promise ((resolve, reject) =>{
        return axios({
            data: {
                url: '/api/place/detailPlace',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(err => {
            return resolve(err);
        })
    })
};
export const savePlace = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/place/savePlace',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
export const deletePlace = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/place/deletePlace',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
