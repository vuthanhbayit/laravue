import axios from '../../core/plugins/http';

export const getListContact = ({commit}, opts) => {
    return new Promise ((resolve, reject) => {
        return axios({
            data: {
                url: '/api/contact/searchContact',
                ...opts
            }
        }).then(response =>{
            commit("GET_LIST_CONTACT", response);
            return resolve(response);
        }).catch(err =>{
            commit("GET_LIST_CONTACT", null);
            return reject(err);
        })
    })
};
export const getDetailContact = ({commit}, opts) =>{
    return new Promise ((resolve, reject) =>{
        return axios({
            data: {
                url: '/api/contact/detailContact',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(err => {
            return resolve(err);
        })
    })
};
export const readContact = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/contact/readContact',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
