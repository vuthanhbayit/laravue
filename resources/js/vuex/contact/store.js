import * as actions from './actions';

const store = {
    state: {
        listContact: null,
        contactCount: 0,
    },
    mutations: {
        GET_LIST_CONTACT: (state, payload) => {
            state.listContact = payload.data;
            state.contactCount = payload.totalRow;
        },
    },
    actions,
    getters: {
        listContact: state => state.listContact,
        contactCount: state => state.contactCount,
    }
};
export default store;
