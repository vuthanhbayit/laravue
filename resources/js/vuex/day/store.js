import * as actions from './actions';

const store = {
    state: {
        listDay: null,
        dayCount: 0,
    },
    mutations: {
        GET_LIST_DAY: (state, payload) => {
            state.listDay = payload.data;
            state.dayCount = payload.totalRow;
        },
    },
    actions,
    getters: {
        listDay: state => state.listDay,
        dayCount: state => state.dayCount,
    }
};
export default store;
