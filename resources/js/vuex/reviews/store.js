﻿import * as actions from './actions';

const store = {
    state: {
        listReviews: null,
        reviewsCount: 0,
    },
    mutations: {
        GET_LIST_REVIEWS: (state, payload) => {
            state.listReviews = payload.data;
            state.reviewsCount = payload.totalRow;
        },
    },
    actions,
    getters: {
        listReviews: state => state.listReviews,
        reviewsCount: state => state.reviewsCount,
    }
};
export default store;
