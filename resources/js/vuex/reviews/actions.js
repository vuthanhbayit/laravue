﻿import axios from '../../core/plugins/http';

export const getListReviews = ({commit}, opts) => {
    return new Promise ((resolve, reject) => {
        return axios({
            data: {
                url: '/api/reviews/searchReviews',
                ...opts
            }
        }).then(response =>{
            commit("GET_LIST_REVIEWS", response);
            return resolve(response);
        }).catch(err =>{
            commit("GET_LIST_REVIEWS", null);
            return reject(err);
        })
    })
};
export const getDetailReviews = ({commit}, opts) =>{
    return new Promise ((resolve, reject) =>{
        return axios({
            data: {
                url: '/api/reviews/detailReviews',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(err => {
            return resolve(err);
        })
    })
};
export const saveReviews = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/reviews/saveReviews',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
export const deleteReviews = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/reviews/deleteReviews',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
