import * as actions from './actions';

const store = {
    state: {
        listBlogPost: null,
        blogPostCount: 0,
    },
    mutations: {
        GET_LIST_BLOG_POST: (state, payload) => {
            state.listBlogPost = payload.data;
            state.blogPostCount = payload.totalRow;
        },
    },
    actions,
    getters: {
        listBlogPost: state => state.listBlogPost,
        blogPostCount: state => state.blogPostCount,
    }
};
export default store;
