import axios from '../../core/plugins/http';

export const getListBlogPost = ({commit}, opts) => {
    return new Promise ((resolve, reject) => {
        return axios({
            data: {
                url: '/api/blogPost/searchBlogPost',
                ...opts
            }
        }).then(response =>{
            commit("GET_LIST_BLOG_POST", response);
            return resolve(response);
        }).catch(err =>{
            commit("GET_LIST_BLOG_POST", null);
            return reject(err);
        })
    })
};
export const getDetailBlogPost = ({commit}, opts) =>{
    return new Promise ((resolve, reject) =>{
        return axios({
            data: {
                url: '/api/blogPost/detailBlogPost',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(err => {
            return resolve(err);
        })
    })
};
export const saveBlogPost = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/blogPost/saveBlogPost',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
export const deleteBlogPost = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/blogPost/deleteBlogPost',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
