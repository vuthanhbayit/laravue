import * as actions from './actions';

const store = {
    state: {
        listProduct: null,
        productCount: 0,
    },
    mutations: {
        GET_LIST_PRODUCT: (state, payload) => {
            state.listProduct = payload.data;
            state.productCount = payload.totalRow;
        },
    },
    actions,
    getters: {
        listProduct: state => state.listProduct,
        productCount: state => state.productCount,
    }
};
export default store;
