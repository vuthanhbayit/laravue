import axios from '../../core/plugins/http';

export const getListProduct = ({commit}, opts) => {
    return new Promise ((resolve, reject) => {
        return axios({
            data: {
                url: '/api/product/searchProduct',
                ...opts
            }
        }).then(response =>{
            commit("GET_LIST_PRODUCT", response);
            return resolve(response);
        }).catch(err =>{
            commit("GET_LIST_PRODUCT", null);
            return reject(err);
        })
    })
};
export const getDetailProduct = ({commit}, opts) =>{
    return new Promise ((resolve, reject) =>{
        return axios({
            data: {
                url: '/api/product/detailProduct',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(err => {
            return resolve(err);
        })
    })
};
export const saveProduct = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/product/saveProduct',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
export const deleteProduct = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/product/deleteProduct',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
