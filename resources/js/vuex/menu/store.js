import * as actions from './actions';

const store = {
    state: {
        listMenu: null,
        menuCount: 0,
    },
    mutations: {
        GET_LIST_MENU: (state, payload) => {
            state.listMenu = payload.data;
            state.menuCount = payload.totalRow;
        },
    },
    actions,
    getters: {
        listMenu: state => state.listMenu,
        menuCount: state => state.menuCount,
    }
};
export default store;
