import axios from '../../core/plugins/http';

export const getTreeMenu = ({commit}, opts) =>{
    return new Promise ((resolve, reject) =>{
        return axios({
            data: {
                url: '/api/menu/getTreeMenu',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(err => {
            return resolve(err);
        })
    })
};
export const saveOrderMenu = ({commit}, opts) =>{
    return new Promise ((resolve, reject) =>{
        return axios({
            data: {
                url: '/api/menu/saveOrderMenu',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(err => {
            return resolve(err);
        })
    })
};
export const saveMenu = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/menu/saveMenu',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
export const detailMenu = ({commit}, id) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/menu/detailMenu',
                id: id
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
export const deleteMenu = ({commit}, id) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/menu/deleteMenu',
                id: id
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
export const importCategory = ({commit}, opts) => {
    return new Promise ((resolve, reject) => {
        return axios({
            data: {
                url: '/api/menu/importCategory',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(err =>{
            return reject(err);
        })
    })
};
export const deleteCategoryMenu = ({commit}, opts) => {
    return new Promise ((resolve, reject) => {
        return axios({
            data: {
                url: '/api/menu/deleteCategory',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(err =>{
            return reject(err);
        })
    })
};
