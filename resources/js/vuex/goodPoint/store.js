import * as actions from './actions';

const store = {
    state: {
        listGoodPoint: null,
        goodPointCount: 0,
    },
    mutations: {
        GET_LIST_GOOD_POINT: (state, payload) => {
            state.listGoodPoint = payload.data;
            state.goodPointCount = payload.totalRow;
        },
    },
    actions,
    getters: {
        listGoodPoint: state => state.listGoodPoint,
        goodPointCount: state => state.goodPointCount,
    }
};
export default store;
