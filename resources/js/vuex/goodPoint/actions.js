import axios from '../../core/plugins/http';

export const getListGoodPoint = ({commit}, opts) => {
    return new Promise ((resolve, reject) => {
        return axios({
            data: {
                url: '/api/goodPoint/searchGoodPoint',
                ...opts
            }
        }).then(response =>{
            commit("GET_LIST_GOOD_POINT", response);
            return resolve(response);
        }).catch(err =>{
            commit("GET_LIST_GOOD_POINT", null);
            return reject(err);
        })
    })
};
export const getDetailGoodPoint = ({commit}, opts) =>{
    return new Promise ((resolve, reject) =>{
        return axios({
            data: {
                url: '/api/goodPoint/detailGoodPoint',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(err => {
            return resolve(err);
        })
    })
};
export const saveGoodPoint = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/goodPoint/saveGoodPoint',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
export const deleteGoodPoint = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/goodPoint/deleteGoodPoint',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
