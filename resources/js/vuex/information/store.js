import * as actions from './actions';

const store = {
    state: {
        listInformation: null,
        informationCount: 0,
    },
    mutations: {
        GET_LIST_INFORMATION: (state, payload) => {
            state.listInformation = payload.data;
            state.informationCount = payload.totalRow;
        },
    },
    actions,
    getters: {
        listInformation: state => state.listInformation,
        informationCount: state => state.informationCount,
    }
};
export default store;
