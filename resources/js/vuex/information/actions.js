import axios from '../../core/plugins/http';

export const getListInformation = ({commit}, opts) => {
    return new Promise ((resolve, reject) => {
        return axios({
            data: {
                url: '/api/information/searchInformation',
                ...opts
            }
        }).then(response =>{
            commit("GET_LIST_INFORMATION", response);
            return resolve(response);
        }).catch(err =>{
            commit("GET_LIST_INFORMATION", null);
            return reject(err);
        })
    })
};
export const getDetailInformation = ({commit}, opts) =>{
    return new Promise ((resolve, reject) =>{
        return axios({
            data: {
                url: '/api/information/detailInformation',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(err => {
            return resolve(err);
        })
    })
};
export const saveInformation = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/information/saveInformation',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
export const deleteInformation = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/information/deleteInformation',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
