﻿import * as actions from './actions';
import CONSTANTS from '../../core/utils/constants';

const store = {
    state: {
        system: null
    },
    mutations: {
        INIT_SYSTEM: (state, payload) => {
            state.system = payload.data;
        }
    },
    actions,
    getters: {
        system: state => state.system
    }
};

export default store;
