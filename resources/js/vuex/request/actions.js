import axios from '../../core/plugins/http';

export const getListRequest = ({commit}, opts) => {
    return new Promise ((resolve, reject) => {
        return axios({
            data: {
                url: '/api/request/searchRequest',
                ...opts
            }
        }).then(response =>{
            commit("GET_LIST_REQUEST", response);
            return resolve(response);
        }).catch(err =>{
            commit("GET_LIST_REQUEST", null);
            return reject(err);
        })
    })
};
export const getDetailRequest = ({commit}, opts) =>{
    return new Promise ((resolve, reject) =>{
        return axios({
            data: {
                url: '/api/request/detailRequest',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(err => {
            return resolve(err);
        })
    })
};
export const saveRequest = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/request/saveRequest',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
export const deleteRequest = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/request/deleteRequest',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
