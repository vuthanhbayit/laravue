import * as actions from './actions';

const store = {
    state: {
        listRequest: null,
        requestCount: 0,
    },
    mutations: {
        GET_LIST_REQUEST: (state, payload) => {
            state.listRequest = payload.data;
            state.requestCount = payload.totalRow;
        },
    },
    actions,
    getters: {
        listRequest: state => state.listRequest,
        requestCount: state => state.requestCount,
    }
};
export default store;
