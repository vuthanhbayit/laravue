import axios from '../../core/plugins/http';

export const getListMenuGroup = ({commit}, opts) => {
    return new Promise ((resolve, reject) => {
        return axios({
            data: {
                url: '/api/menu/searchMenuGroup',
                ...opts
            }
        }).then(response =>{
            commit("GET_LIST_MENU_GROUP", response);
            return resolve(response);
        }).catch(err =>{
            commit("GET_LIST_MENU_GROUP", null);
            return reject(err);
        })
    })
};
export const getDetailMenuGroup = ({commit}, opts) =>{
    return new Promise ((resolve, reject) =>{
        return axios({
            data: {
                url: '/api/menu/detailMenuGroup',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(err => {
            return resolve(err);
        })
    })
};
export const saveMenuGroup = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/menu/saveMenuGroup',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
export const deleteMenuGroup = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/menu/deleteMenuGroup',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};

