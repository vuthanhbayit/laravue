import * as actions from './actions';

const store = {
    state: {
        listMenuGroup: null,
        menuGroupCount: 0,
    },
    mutations: {
        GET_LIST_MENU_GROUP: (state, payload) => {
            state.listMenuGroup = payload.data;
            state.menuGroupCount = payload.totalRow;
        },
    },
    actions,
    getters: {
        listMenuGroup: state => state.listMenuGroup,
        menuGroupCount: state => state.menuGroupCount,
    }
};
export default store;
