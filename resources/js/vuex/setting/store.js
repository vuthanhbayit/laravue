import * as actions from './actions';

const store = {
    state: {
        listSetting: null,
    },
    mutations: {
        GET_LIST_SETTING: (state, payload) => {
            state.listSetting = payload.data;
        },
    },
    actions,
    getters: {
        listSetting: state => state.listSetting,
    }
};
export default store;
