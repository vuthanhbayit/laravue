import * as actions from './actions';

const store = {
    state: {
        listExtension: null,
    },
    mutations: {
        GET_LIST_EXTENSION: (state, payload) => {
            state.listExtension = payload.data;
        },
    },
    actions,
    getters: {
        listExtension: state => state.listExtension,
    }
};
export default store;
