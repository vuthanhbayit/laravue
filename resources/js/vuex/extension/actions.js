import axios from '../../core/plugins/http';

export const getListExtension = ({commit}, opts) => {
    return new Promise ((resolve, reject) => {
        return axios({
            data: {
                url: '/api/extension/searchExtension',
                ...opts
            }
        }).then(response =>{
            commit("GET_LIST_EXTENSION", response);
            return resolve(response);
        }).catch(err =>{
            commit("GET_LIST_EXTENSION", null);
            return reject(err);
        })
    })
};
export const deleteExtension = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/extension/deleteExtension',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
