import * as actions from './actions';

const store = {
    state: {
        listArea: null,
        areaCount: 0,
    },
    mutations: {
        GET_LIST_AREA: (state, payload) => {
            state.listArea = payload.data;
            state.areaCount = payload.totalRow;
        },
    },
    actions,
    getters: {
        listArea: state => state.listArea,
        areaCount: state => state.areaCount,
    }
};
export default store;
