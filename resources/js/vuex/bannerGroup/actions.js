import axios from '../../core/plugins/http';

export const getListBannerGroup = ({commit}, opts) => {
    return new Promise ((resolve, reject) => {
        return axios({
            data: {
                url: '/api/banner/searchBannerGroup',
                ...opts
            }
        }).then(response =>{
            commit("GET_LIST_BANNER_GROUP", response);
            return resolve(response);
        }).catch(err =>{
            commit("GET_LIST_BANNER_GROUP", null);
            return reject(err);
        })
    })
};
export const getDetailBannerGroup = ({commit}, opts) =>{
    return new Promise ((resolve, reject) =>{
        return axios({
            data: {
                url: '/api/banner/detailBannerGroup',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(err => {
            return resolve(err);
        })
    })
};
export const saveBannerGroup = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/banner/saveBannerGroup',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
export const deleteBannerGroup = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/banner/deleteBannerGroup',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
