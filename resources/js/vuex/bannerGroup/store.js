import * as actions from './actions';

const store = {
    state: {
        listBannerGroup: null,
        bannerGroupCount: 0,
    },
    mutations: {
        GET_LIST_BANNER_GROUP: (state, payload) => {
            state.listBannerGroup = payload.data;
            state.bannerGroupCount = payload.totalRow;
        },
    },
    actions,
    getters: {
        listBannerGroup: state => state.listBannerGroup,
        bannerGroupCount: state => state.bannerGroupCount,
    }
};
export default store;
