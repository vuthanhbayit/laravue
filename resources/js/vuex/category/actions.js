import axios from '../../core/plugins/http';

export const getListCategory = ({commit}, opts) => {
    return new Promise ((resolve, reject) => {
        return axios({
            data: {
                url: '/api/category/searchCategory',
                ...opts
            }
        }).then(response =>{
            commit("GET_LIST_CATEGORY", response);
            return resolve(response);
        }).catch(err =>{
            commit("GET_LIST_CATEGORY", null);
            return reject(err);
        })
    })
};
export const getDetailCategory = ({commit}, opts) =>{
    return new Promise ((resolve, reject) =>{
        return axios({
            data: {
                url: '/api/category/detailCategory',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(err => {
            return resolve(err);
        })
    })
};
export const saveCategory = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/category/saveCategory',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
export const deleteCategory = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/category/deleteCategory',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
