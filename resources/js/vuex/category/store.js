import * as actions from './actions';

const store = {
    state: {
        listCategory: null,
        categoryCount: 0,
    },
    mutations: {
        GET_LIST_CATEGORY: (state, payload) => {
            state.listCategory = payload.data;
            state.categoryCount = payload.totalRow;
        },
    },
    actions,
    getters: {
        listCategory: state => state.listCategory,
        categoryCount: state => state.categoryCount,
    }
};
export default store;
