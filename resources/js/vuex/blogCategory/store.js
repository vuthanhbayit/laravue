import * as actions from './actions';

const store = {
    state: {
        listBlogCategory: null,
        blogCategoryCount: 0,
    },
    mutations: {
        GET_LIST_BLOG_CATEGORY: (state, payload) => {
            state.listBlogCategory = payload.data;
            state.blogCategoryCount = payload.totalRow;
        },
    },
    actions,
    getters: {
        listBlogCategory: state => state.listBlogCategory,
        blogCategoryCount: state => state.blogCategoryCount,
    }
};
export default store;
