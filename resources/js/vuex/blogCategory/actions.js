import axios from '../../core/plugins/http';

export const getListBlogCategory = ({commit}, opts) => {
    return new Promise ((resolve, reject) => {
        return axios({
            data: {
                url: '/api/blogCategory/searchBlogCategory',
                ...opts
            }
        }).then(response =>{
            commit("GET_LIST_BLOG_CATEGORY", response);
            return resolve(response);
        }).catch(err =>{
            commit("GET_LIST_BLOG_CATEGORY", null);
            return reject(err);
        })
    })
};
export const getDetailBlogCategory = ({commit}, opts) =>{
    return new Promise ((resolve, reject) =>{
        return axios({
            data: {
                url: '/api/blogCategory/detailBlogCategory',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(err => {
            return resolve(err);
        })
    })
};
export const saveBlogCategory = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/blogCategory/saveBlogCategory',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
export const deleteBlogCategory = ({commit}, opts) =>{
    return new Promise((resolve, reject)=>{
        axios({
            data:{
                url: '/api/blogCategory/deleteBlogCategory',
                ...opts
            }
        }).then(response =>{
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
