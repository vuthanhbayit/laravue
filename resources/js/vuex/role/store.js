import * as actions from './actions'

const store = {
    state: {
        listRole: [],
        roleCount: 0,
    },
    mutations: {
        GET_LIST_ROLE: (state, payload) => {
            state.listRole = payload;
            state.roleCount = payload.totalRow;
        },
    },
    actions,
    getters: {
        listRole: state => state.listRole,
        roleCount: state => state.roleCount,
    }
};

export default store;
