<button onclick="topFunction()" id="toppage-btn" title="Go to top"><img src="/images/top-page.png" alt=""
                                                                        class="btn-toppage"></button>
<footer class="footer align-self-end" id="footer">
    <div class="top-footer pd-sakura">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="content-top-footer">
                        <div class="row">
                            @if(!empty($modules['footer1']))
                                <div class="col-lg-5 col-md-12 col-sm-6 col-12 mb-30">
                                    <div class="content-company-sakura list-menu-ft">
                                        @foreach($modules['footer1'] as $item)
                                            @if(isset($item->title))
                                                <div class="name-company-sakura"> @php echo $item->title @endphp</div>
                                            @endif
                                            @if(isset($item->content))
                                                {!! $item->content !!}
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                            @if(!empty($modules['footer2']))
                                <div class="col-lg-2 col-md-4 col-sm-6 col-12 mb-30">
                                    <div class="block-menu-footer list-menu-ft">
                                        @foreach($modules['footer2'] as $item)
                                            @if(isset($item->title))
                                                <div class="name-company-sakura"> @php echo $item->title @endphp</div>
                                            @endif
                                            @if(isset($item->content))
                                                {!! $item->content !!}
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                            @if(!empty($modules['footer3']))
                                <div class="col-lg-2 col-md-4 col-sm-6 col-12 mb-30">
                                    <div class="block-menu-footer list-menu-ft">
                                        @foreach($modules['footer3'] as $item)
                                            @if(isset($item->title))
                                                <div class="name-company-sakura"> @php echo $item->title @endphp</div>
                                            @endif
                                            @if(isset($item->content))
                                                {!! $item->content !!}
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                            @if(!empty($modules['footer4']))
                                <div class="col-lg-3 col-md-4 col-sm-6 col-12 mb-30">
                                    <div class="block-menu-footer list-menu-ft">
                                        @foreach($modules['footer4'] as $item)
                                            @if(isset($item->title))
                                                <div class="name-company-sakura"> @php echo $item->title @endphp</div>
                                            @endif
                                            @if(isset($item->content))
                                                {!! $item->content !!}
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="upper-footer">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="content-upper-ft">
                        <div class="copyright text-white mr-auto">Copyright © 2019 <span
                                class="text-color">Sakura</span></div>
                        <div class="wp-social">
                            <ul class="list-social">
                                <li><a href="{{ getConfig('config_facebook') }}" class="social"><i class="fab fa-facebook-square"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="/js/swiper.min.js"></script>
<script type="text/javascript" src="/js/style.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/jquery.elevateZoom-3.0.8.min.js"></script>
<script src="/js/wow.min.js"></script>
