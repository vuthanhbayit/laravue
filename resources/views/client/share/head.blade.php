<title>@if(isset($title)) {{ $title }} @endif</title>
{{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}
<link href="{{ asset(getConfig('config_icon')) }}" rel="icon"/>
<base href="{{ url('/') }}/">
@if(isset($description))
    <meta name="description" content="{{ $description }}"/>
@endif
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/css/client/fontawesome-all.min.css">
<link rel="stylesheet" href="/css/client/swiper.min.css">
<link rel="stylesheet" href="/css/client/bootstrap.min.css">
<link rel="stylesheet" href="/css/client/animate.min.css">
<link rel="stylesheet" href="/css/client/general.css">
