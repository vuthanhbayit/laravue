<header class="pc-header" id="pc-header">
    <div class="topbar" id="topbar">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="wrap-topbar d-flex">
                        <ul class="list-flag ml-auto">
                            <li class="align-items-center">
                                <div class="wrap-price-head">
                                    <a href="/quickQuote">
                                        <span class="icon"><img src="/images/price.png" alt=""></span>
                                        <span class="text">{{ getLanguage('QuickQuote') }}</span>
                                    </a>
                                </div>
                            </li>
                            @php $languages = getActiveLanguages(); @endphp
                            @if(!empty($languages))
                                <form action="/languages" method="POST" enctype="multipart/form-data"
                                      id="form-language">
                                    @foreach($languages as $item)
                                        <li>
                                            <button class="btn btn-link btn-block language-select" type="button"
                                                    name="{{ $item['code'] }}"><img
                                                    src="{{ asset($item['flagIcon']) }}"
                                                    alt="{{ $item['name'] }}"
                                                    title="{{ $item['name'] }}"/></button>
                                        </li>
                                    @endforeach
                                    <input type="hidden" name="code" value=""/>
                                </form>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="upper-header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="wrap-content-menu-sakura d-flex flex-wrap justify-content-between align-items-center">
                        @foreach(getMenu('top') as $item)
                            @php
                                echo $item;
                            @endphp
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
