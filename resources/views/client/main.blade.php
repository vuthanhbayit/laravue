<html lang="{{ app()->getLocale() }}">
<head>
    @include('client.share.head')
    @yield('style')
</head>
<body>
@include('client.share.menu')
@yield('banner')
@yield('content')
@yield('footer')
@yield('script')
</body>
</html>
