@if(count($data['blogPosts']) > 0)
    <div class="guide-points pd-sakura bg-sakura">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="content-guide-points">
                        <div class="panel-head-travel-package panel-head-general text-center">
                            <div class="row">
                                <div class="col-12 col-sm-10 m-auto">
                                    <h2 class="heading-2 ">{{ $data['descriptions']['title'] }}</h2>
                                    <div class="rule"></div>
                                    <p class="intro heading-3">{{ $data['descriptions']['description'] }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body-guide-points">
                            <ul class="list row">
                                @foreach($data['blogPosts'] as $item)
                                    <li class="col-lg-4 col-md-4 col-sm-4 col-12">
                                        <article class="article-guide-points wow fadeInLeft">
                                            <div class="img-thumb">
                                                <a href="/bp{{ $item['id'] }}" class="image-cover image">
                                                    <img src="{{ asset($item['image']) }}" alt=""
                                                         class="img-fluid image-cover"></a>
                                            </div>
                                            <div class="info">
                                                <h3 class="title"><a href="/bp{{ $item['id'] }}"
                                                                     title="{{ $item['title'] }}"
                                                                     class="heading-3">{{ $item['title'] }}</a>
                                                </h3>
                                            </div>
                                        </article>
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
