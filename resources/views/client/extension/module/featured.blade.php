@if(count($data['product']) > 0)
    <div class="travel-package pd-sakura">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="content-travel-package">
                        <div class="panel-head-travel-package panel-head-general text-center">
                            <div class="row">
                                <div class="col-12 col-sm-10 m-auto">
                                    <h2 class="heading-2">{{ $data['descriptions']['title'] }}</h2>
                                    <div class="rule"></div>
                                    <p class="intro heading-3">{{ $data['descriptions']['description'] }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body-travel-package">
                        <ul class="list row">
                            @foreach($data['product'] as $item)
                                <li class="col-lg-4 col-md-4 col-sm-4 col-12">
                                    <article class="article-travel-package">
                                        <div class="img-thumb">
                                            <a href="/p{{ $item['id'] }}" class="image">
                                                <img src="{{ asset($item['image']) }}" alt="{{ $item['title'] }}"
                                                     class="img-fluid image-cover"></a>
                                        </div>
                                        <div class="info">
                                            <div class="info">
                                                @if($item['dayNumber'])
                                                    <div class="meta">
                                                        {{ $item['dayNumber'] }} {{ getLanguage('StartFromClient') }} {{ $item['price'] }} {{getConfig('config_currency')}}
                                                    </div>
                                                @endif
                                                <h3 class="title"><a
                                                            href="/p{{ $item['id'] }}">{{ $item['title'] }}</a>
                                                </h3>
                                                <div class="view">
                                                    <a href="/p{{ $item['id'] }}"
                                                       class="view-more heading-3">{{ getLanguage('ViewMore') }}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endif
