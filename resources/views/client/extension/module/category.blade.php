<div class="services-sakura">
    <div class="panel-head-services">
        <div class="container">
            <div class="row">
                <div class="col-12 p-0">
                    @php
                        $li = ''; $content = '';
                    @endphp
                    @foreach($data['category'] as $k => $item)
                        @php
                            if ($k == 0){
                                $active = 'active';
                            }else{
                                $active = '';
                            }
                            $li .= '<li class="col-lg-3 col-md-3 col-md-3 col-sm-3 col-3 ">
                                        <a class="item '.$active.'" id="pills-profile-tab" data-toggle="pill" href="#index-'.$k.'" role="tab">
                                            <div class="item-services-sakura align-items-center '.$active.'">
                                                <div class="img-thumb">
                                                    <div class="img-services img-color">
                                                        <img src="' . asset($item['icon']) . '" alt="' . $item['title'] . '" class="img-fluid"></div>
                                                </div>
                                                <div class="text">' . $item['title'] . '</div>
                                            </div>
                                        </a>
                                    </li>';
                            $content .='<div class="tab-pane animated zoomIn '.$active.'" id="index-'.$k.'" role="tabpanel">
                                            <div class="panel-body-services">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="show-panel-body">
                                                            <div class="row">
                                                                <div class="col-lg-7 col-md-6 col-sm-12 wp-info-service">
                                                                    <div class="info">
                                                                        <h2 class="heading-2 title-body-services">' . $item['title'] . '</h2>
                                                                        <p class="heading-3 intro">' . $item['short_description'] . '</p>
                                                                        <div class="view">
                                                                            <a href="/c'.$item['id'].'" class="view-more heading-3">'.getLanguage('ViewServices').'</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-5 col-md-6 col-sm-12 d-lg-block d-md-block d-sm-none d-none wp-thumb-service">
                                                                    <div class="img-thumb">
                                                                        <img src="'.asset($item['image']).'" alt="' . $item['title'] . '" class="img-fluid image-cover">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>';
                        @endphp
                    @endforeach
                    <ul class="nav nav-pills panel-head-commit m-auto row" id="pills-tab" role="tablist">
                        @php echo $li; @endphp
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body-services">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="tab-content" id="pills-tabContent">
                        @php echo $content; @endphp
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
