@extends('client.main')
@section('banner')
    @if(count($data['banner']) > 0)
        <div class="banner-horizontal pt-header">
            <div class="swiper-container wp-slide-banner" id="wp-slide-banner">
                <div class="swiper-wrapper slide-banner">
                    @foreach($data['banner'] as $item)
                        <div class="swiper-slide item-slide">
                            <div class="img-thumb">
                                <a href="{{ $item->link }}" class="item-slide-banner">
                                    <img src="{{ asset($item->image) }}" alt="" class=" image-cover"/>
                                </a>
                            </div>
                            <div class="info-banner container">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="overlay-banner">
                                            <h3 class="title text-white">{{ $item->title }}</h3>
                                            <div class="text text-white subtitle mb-3">
                                                @php echo $item->content; @endphp
                                            </div>
                                            <a href="{{ $item->link }}"
                                               class="readmore text-white">{{ getLanguage('ViewMore') }}</a>
                                        </div>
                                    </div>
                                    <div class="col-lg-5"></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination pagination-banner"></div>
            </div>
        </div>
    @endif
@endsection
@section('content')
    @if(count($breadcrumbs))
    <div class="wp-breadcumb">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul class="list-breadcumb">
                        @foreach ($breadcrumbs as $breadcrumb)
                            <li><a href="{{ $breadcrumb['href'] }}"><span>{{ $breadcrumb['text'] }}</span></a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @endif
    @php
        echo $content_top;
    @endphp
    @if(!empty($data['category']))
    <div class="services-sakura order-study">
        <div class="panel-body-services">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="panel-body-services pd-sakura">
                            <div class="row">
                                <div class="col-12">
                                    <div class="panel-head-content-news panel-head-general text-center">
                                        <div class="row inner">
                                            <div class="col-lg-10 col-md-10 col-sm-12 col-12 m-auto">
                                                <div class="content-news">
                                                    <h2 class="heading-2">@php echo $data['category']['title'] @endphp</h2>
                                                    <div class="rule"></div>
                                                    <div class="intro heading-3 content-custom" id="content-description">
                                                        @php echo $data['category']['description'] @endphp
                                                    </div>
                                                </div>                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if(count($data['goodPoint']) > 0)
        <div class="big-function-sakura pd-sakura">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                            <div class="swiper-container swiper-function-sakura" id="swiper-function">
                                <ul class=" wrap-item-function text-center swiper-wrapper">
                                    @foreach($data['goodPoint'] as $item)
                                        <li class="text-center swiper-slide">
                                            <div class="item-function">
                                                <div class="funtion text-center">
                                                    <div class="image"><img src="{{ asset($item['icon']) }}" alt="" class="icon-function-sakura"></div>
                                                    <div class="text heading-3">{{ $item['content'] }}</div>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="tab-content">
        <div class="hot-package-sakura pd-sakura">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="content-hot-package">
                            <div class="panel-body-hot-package">
                                <ul class="list row">
                                    @foreach($data['product'] as $item)
                                        <li class="col-lg-12 col-md-6 col-sm-6 col-12">
                                            <div class="article-sakura">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-12 col-sm-12 col-12 ">
                                                        <div class="img-thumb">
                                                            <a href="/p{{ $item['id'] }}" class="image">
                                                                <img src="{{ asset($item['image']) }}" alt=""
                                                                     class="img-fluid image-cover">
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-9 col-md-12 col-sm-12 col-12">
                                                        <div class="info">
                                                            <div class="clearfix"></div>
                                                            <a href="/p{{ $item['id'] }}">
                                                                <div class="title-package heading-3 mb-2">{{ $item['title'] }}</div>
                                                            </a>
                                                            <div class="info-package row">
                                                                <div class="col-12">
                                                                    <div class="row">
                                                                        <div class="col-lg-4 col-md-12 col-12 col-12">
                                                                            <div
                                                                                class="wp-item-package-details heading-3">
                                                                                <span class="icon"><img
                                                                                        src="/images/icon-package-1.png"
                                                                                        alt=""></span>
                                                                                <span
                                                                                    class="text">{{ getLanguage('CodeClient') }}: {{ $item['code'] }}</span>
                                                                            </div>
                                                                            <div
                                                                                class="wp-item-package-details heading-3">
                                                                                <span class="icon"><img
                                                                                        src="/images/icon-package-2.png"
                                                                                        alt=""></span>
                                                                                <span class="text">{{ getLanguage('DayNumberClient') }}: {{ $item['dayNumber'] }}</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-4 col-md-12 col-12 col-12">
                                                                            <div
                                                                                class="wp-item-package-details heading-3">
                                                                                <span class="icon"><img
                                                                                        src="/images/icon-package-3.png"
                                                                                        alt=""></span>
                                                                                <span
                                                                                    class="text">{{ getLanguage('DepartAddressClient') }}: {{ $item['departAddress'] }}</span>
                                                                            </div>
                                                                            <div
                                                                                class="wp-item-package-details heading-3">
                                                                                <span class="icon"><img
                                                                                        src="/images/icon-package-4.png"
                                                                                        alt=""></span>
                                                                                <span class="text">{{ getLanguage('SeatsExistClient') }}: {{ $item['seatsExist'] }}</span>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            class="col-lg-4 col-md-12 col-12 col-12 wp-sakura">
                                                                            <div class="wp-price mb-3">
                                                                                <span
                                                                                    class="price text-color">{{ $item['price'] }}{{ getConfig('config_currency') }}</span>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                            <div class="wp-view-package">
                                                                                <a href="/p{{ $item['id'] }}"
                                                                                   class="view-more-package">{{ getLanguage('ViewMore') }}</a>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="view text-center">
                                    <nav>
                                        <ul class="pagination clearfix">
                                            {!! $data['product']->links() !!}
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="request-quote ">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-lg-8 col-md-10 col-sm-12 col-12 m-auto">
                            <div class="content-request-quote">
                                <div class="panel-head-travel-package panel-head-general text-center">
                                    <div class="row">
                                        <div class="col-12 m-auto">
                                            <h2 class="heading-2">{{ getLanguage('TitleRequestQuickQuote') }}</h2>
                                            <div class="rule"></div>
                                            <p class="intro heading-3 mb-30">{{ getLanguage('DescRequestQuickQuote') }}</p>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-12 m-auto">
                                                    <a href="/quickQuote" class="btn-detail">{{ getLanguage('ViewQuickQuote') }}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @php
        echo $content_bottom;
    @endphp
@endsection
@section('footer')
    @php
        echo $footer;
    @endphp
@endsection
