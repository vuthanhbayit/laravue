@extends('client.main')
@section('banner')
    @if(count($banner) > 0)
        <div class="banner-horizontal pt-header">
            <div class="swiper-container wp-slide-banner" id="wp-slide-banner">
                <div class="swiper-wrapper slide-banner">
                    @foreach($banner as $item)
                        <div class="swiper-slide item-slide">
                            <div class="img-thumb">
                                <a href="{{ $item->link }}" class="item-slide-banner">
                                    <img src="{{ asset($item->image) }}" alt="{{ $item->title }}" class="image-cover"/>
                                </a>
                            </div>
                            <div class="info-banner container">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="overlay-banner">
                                            <h3 class="title text-white">{{ $item->title }}</h3>
                                            <div class="text text-white subtitle mb-3">
                                                @php echo $item->content; @endphp
                                            </div>
                                            <a href="{{ $item->link }}"
                                               class="readmore text-white">{{ getLanguage('ViewMore') }}</a>
                                        </div>
                                    </div>
                                    <div class="col-lg-5"></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination pagination-banner"></div>
            </div>
        </div>
    @endif
@endsection
@section('content')
    @php
        echo $content_top;
    @endphp
    @php
        echo $content_bottom;
    @endphp
@endsection
@section('footer')
    @php
        echo $footer;
    @endphp
@endsection
