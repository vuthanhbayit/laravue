@extends('client.main')
@section('content')
    @if(count($breadcrumbs) > 0)
        <div class="wp-breadcumb">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="list-breadcumb">
                            @foreach ($breadcrumbs as $breadcrumb)
                                <li><a href="{{ $breadcrumb['href'] }}"><span>{{ $breadcrumb['text'] }}</span></a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @php
        echo $content_top;
    @endphp
    @if(!empty($data))
        <div class="detail-article-travel-guide">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <article class="article-travel-guide row">
                            <div class="col-lg-10 col-md-10 col-sm-12 col-12 mx-auto">
                                <div class="info text-justify">
                                    <h3 class="heading-3 title">{{ $data['title'] }}</h3>
                                    <div class="meta-date">
                                        <strong>{{ date("d-m-Y", strtotime($data['created_at']))}}</strong></div>
                                    <div class="rule"></div>
                                    {{--<div class="intro heading-3">@php echo $data['description'] @endphp</div>--}}
                                    <div class="content-post">@php echo $data['content'] @endphp</div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @php
        echo $content_bottom;
    @endphp
@endsection
@section('footer')
    @php
        echo $footer;
    @endphp
@endsection
