@extends('client.main')
@section('style')
    <link rel="stylesheet" type="text/css" href="/css/daterangepicker.css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet"/>
@endsection
@section('banner')
    @if(count($banner) > 0)
        <div class="banner-horizontal pt-header">
            <div class="swiper-container wp-slide-banner" id="wp-slide-banner">
                <div class="swiper-wrapper slide-banner">
                    @foreach($banner as $item)
                        <div class="swiper-slide item-slide">
                            <div class="img-thumb">
                                <a href="{{ $item->link }}" class="item-slide-banner">
                                    <img src="{{ asset($item->image) }}" alt="{{ $item->title }}" class="image-cover"/>
                                </a>
                            </div>
                            <div class="info-banner container">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="overlay-banner">
                                            <h3 class="title text-white">{{ $item->title }}</h3>
                                            <div class="text text-white subtitle mb-3">
                                                {!! $item->content !!}
                                            </div>
                                            <a href="{{ $item->link }}"
                                               class="readmore text-white">{{ getLanguage('ViewMore') }}</a>
                                        </div>
                                    </div>
                                    <div class="col-lg-5"></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination pagination-banner"></div>
            </div>
        </div>
    @endif
@endsection

@section('content')
    @if(count($breadcrumbs) > 0)
        <div class="wp-breadcumb">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="list-breadcumb">
                            @foreach ($breadcrumbs as $breadcrumb)
                                <li><a href="{{ $breadcrumb['href'] }}"><span>{{ $breadcrumb['text'] }}</span></a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="price-fast-sakura pd-sakura">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-lg-10 col-md-10 col-sm-12 col-12 m-auto">
                            <div class="content-price-fast-sakura">

                                <div class="panel-head-price-fast panel-head-general text-center">
                                    <div class="row">
                                        <div class="col-12 m-auto">
                                            <h2 class="heading-2">{{getLanguage('quickQuote')}}</h2>
                                            <div class="rule"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="head-BGN text-center pd-sakura pb-0">
                                            <h3>{{getLanguage('Depart')}}</h3>
                                        </div>
                                        <div class="row pd-50">
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                                                <div class="wp-select">
                                                    <div class="row">
                                                        <div class='col-12'>
                                                            <div class="form-group">
                                                                <label for="">
                                                                    <h4 class="title-select heading-3">{{getLanguage('ExpectedDepartureDate')}}</h4>
                                                                </label>
                                                                <div class="box-select">
                                                                    <input type="text" name="expected_departure_date"
                                                                           class="input-text-sakura" value="">
                                                                    <span class="icon-select"><i
                                                                            class="fas fa-angle-down"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                                                <div class="wp-select">
                                                    <div class="row">
                                                        <div class='col-12'>
                                                            <div class="form-group">
                                                                <label for="">
                                                                    <h4 class="title-select heading-3">
                                                                        {{getLanguage('ExpectedNumberOfDays')}}
                                                                    </h4>
                                                                </label>
                                                                <div class="box-select">
                                                                    <select name="expected_number_of_days"
                                                                            class="input-text-sakura">
                                                                        <option
                                                                            value="0"> {{getLanguage('SelectTime')}}</option>
                                                                        @for ($i = 0; $i < 20; $i++)
                                                                            <option
                                                                                value="{{$i+1}}">{{ $i + 1 }} {{getLanguage('Day')}}
                                                                            </option>
                                                                        @endfor
                                                                    </select>
                                                                    <span class="icon-select"><i
                                                                            class="fas fa-angle-down"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                                                <div class="wp-select">
                                                    <div class="row">
                                                        <div class='col-12'>
                                                            <div class="form-group">
                                                                <label for="">
                                                                    <h4 class="title-select heading-3">
                                                                        {{getLanguage('DepartureFromId')}}
                                                                    </h4>
                                                                </label>
                                                                <div class="box-select">
                                                                    <select name="departure_from_id"
                                                                            class="input-text-sakura">
                                                                        <option
                                                                            value="0">{{getLanguage('SelectCity')}}</option>
                                                                        <option value="Hà Nội">Hà Nội</option>
                                                                        <option value="TP. Hồ Chí Minh">TP. Hồ Chí
                                                                            Minh
                                                                        </option>
                                                                        <option value="Cần Thơ">Cần Thơ</option>
                                                                        <option value="Đà Nẵng">Đà Nẵng</option>
                                                                        <option value="Hải Phòng">Hải Phòng</option>
                                                                    </select>
                                                                    <span class="icon-select"><i
                                                                            class="fas fa-angle-down"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="head-BGN text-center pd-sakura pb-0">
                                            <h3>{{getLanguage('NumberOfParticipants')}}</h3>
                                        </div>
                                        <div class="row pd-50">
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                                                <div class="wp-select">
                                                    <div class="row">
                                                        <div class='col-12'>
                                                            <div class="form-group">
                                                                <label for="">
                                                                    <h4 class="title-select heading-3">
                                                                        {{getLanguage('NumberOfAdults')}}
                                                                        ({{getLanguage('Over12YearsOld')}})
                                                                    </h4>
                                                                </label>
                                                                <div class="box-select">
                                                                    <select name="number_of_adults" id=""
                                                                            class="input-text-sakura">
                                                                        @for ($i = 0; $i < 40; $i++)
                                                                            <option
                                                                                value="{{$i+1}}">{{ $i + 1 }} {{getLanguage('People')}}
                                                                            </option>
                                                                        @endfor
                                                                    </select>
                                                                    <span class="icon-select"><i
                                                                            class="fas fa-angle-down"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                                                <div class="wp-select">
                                                    <div class="row">
                                                        <div class='col-12'>
                                                            <div class="form-group">
                                                                <label for="">
                                                                    <h4 class="title-select heading-3">
                                                                        {{getLanguage('NumberOfChildren')}}
                                                                        ({{getLanguage('From2To11YearsOld')}})
                                                                    </h4>
                                                                </label>
                                                                <div class="box-select">
                                                                    <select name="number_of_children" id=""
                                                                            class="input-text-sakura">
                                                                        @for ($i = 0; $i <= 40; $i++)
                                                                            <option
                                                                                value="{{$i}}">{{ $i }} {{getLanguage('People')}}
                                                                            </option>
                                                                        @endfor
                                                                    </select>
                                                                    <span class="icon-select"><i
                                                                            class="fas fa-angle-down"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                                                <div class="wp-select">
                                                    <div class="row">
                                                        <div class='col-12'>
                                                            <div class="form-group">
                                                                <label for="">
                                                                    <h4 class="title-select heading-3">
                                                                        {{getLanguage('NumberOfBabies')}}
                                                                        ({{getLanguage('Under2YearsOld')}})
                                                                    </h4></label>
                                                                <div class="box-select">
                                                                    <select name="number_of_babies" id=""
                                                                            class="input-text-sakura">
                                                                        @for ($i = 0; $i <= 40; $i++)
                                                                            <option
                                                                                value="{{$i}}">{{ $i }} {{getLanguage('People')}}
                                                                            </option>
                                                                        @endfor
                                                                    </select>
                                                                    <span class="icon-select"><i
                                                                            class="fas fa-angle-down"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="head-BGN text-center pd-sakura pb-0">
                                            <h3> {{getLanguage('SelectionCriteria')}} </h3>
                                        </div>

                                        <div class="row pd-50">
                                            <div class="col-12">
                                                <div class="tourist-area row">
                                                    <div class="col-lg-4 col-md-12 col-sm-12 col-12 mx-auto">
                                                        <div class="wp-select">
                                                            <div class="row">
                                                                <div class='col-12'>
                                                                    <div class="form-group">
                                                                        <div class="big-select-area">
                                                                            <div class="select-area">
                                                                                <div class="panel-head-select">
                                                                                    <div class="text">
                                                                                        {{getLanguage('TouristArea')}}
                                                                                    </div>
                                                                                </div>
                                                                                <div class="panel-body-select">
                                                                                    <div class="select-level-1">
                                                                                        <ul class="list-select-area">
                                                                                            @foreach($data['provinces'] as $v)
                                                                                                <li>
                                                                                                    <div class="item">
                                                                                                        <div
                                                                                                            class="text">{{$v['area']['title']}}</div>
                                                                                                        <div
                                                                                                            class="select-level-2">
                                                                                                            <div
                                                                                                                class="item-check">
                                                                                                                @foreach($v['province'] as $i)
                                                                                                                    <label
                                                                                                                        class="container">
                                                                                                                        {{$i['title']}}
                                                                                                                        <input
                                                                                                                            type="checkbox"
                                                                                                                            name="{{$v['area']['title']}}"
                                                                                                                            value-title="{{$i['title']}}"
                                                                                                                            value="{{$i['province_id']}}">
                                                                                                                        <span
                                                                                                                            class="checkmark"></span>
                                                                                                                    </label>
                                                                                                                @endforeach
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </li>
                                                                                            @endforeach
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-6 col-12 mx-auto">
                                                        <div class="wp-select">
                                                            <div class="row">
                                                                <div class='col-12'>
                                                                    <div class="form-group">
                                                                        <div class="box-select">

                                                                            <select class="multiple-select-2"
                                                                                    name="places[]" multiple="multiple">
                                                                                @foreach($data['places'] as $v)
                                                                                    <option value="{{$v->id}}"
                                                                                            data-title="{{$v->title}}"
                                                                                            class="option">{{$v->title}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                            <span class="icon-select"><i
                                                                                    class="fas fa-angle-down"></i></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 col-sm-6 col-12 mx-auto">
                                                        <div class="wp-select">
                                                            <div class="row">
                                                                <div class='col-12'>
                                                                    <div class="form-group">
                                                                        <div class="box-select">
                                                                            <select class="multiple-select-2"
                                                                                    name="requests[]"
                                                                                    multiple="multiple">
                                                                                @foreach($data['requests'] as $v)
                                                                                    <option value="{{$v->id}}"
                                                                                            data-title="{{$v->title}}"
                                                                                            class="option">{{$v->title}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                            <span class="icon-select"><i
                                                                                    class="fas fa-angle-down"></i></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="result">
                                                    <h4 class="result-title"></h4>
                                                    <div class="result-text"></div>
                                                    <div class="result-text-places"></div>
                                                    <div class="result-text-requests"></div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>

                                <div class="row pd-50">
                                    <div class="col-12 text-center ">
                                        <label for="">
                                            <h4 class="title-select heading-3"> {{getLanguage('SpecialRequirements')}} </h4>
                                        </label>
                                        <textarea name="special_requirements" cols="30" rows="10"
                                                  class="w-100 p-3"
                                                  placeholder="Ví dụ: Đoàn có người ăn chay, Dị ứng thực phẩm, Bệnh tim hoặc thể trạng yếu…"></textarea>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="request-quote ">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-lg-8 col-md-10 col-sm-12 col-12 m-auto">
                            <div class="content-request-quote">

                                <div class="panel-head-travel-package panel-head-general text-center">
                                    <div class="row">
                                        <div class="col-12 m-auto">
                                            <h2 class="heading-2">{{getLanguage('PersonalInfomation')}}</h2>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel-body-travel-package">
                                    <form action="/sendQuickQuote" method="POST" class="form-sakura" id="quick-quote">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group-sakura">
                                                            <input type="text"
                                                                   placeholder="<?php echo getLanguage('FullName') ?>"
                                                                   name="fullName"
                                                                   class="input-text w-100">
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group-sakura">
                                                            <input type="text"
                                                                   placeholder="<?php echo getLanguage('Email') ?>"
                                                                   name="email"
                                                                   class="input-text w-100">
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group-sakura">
                                                            <input type="text"
                                                                   placeholder="<?php echo getLanguage('Phone') ?>"
                                                                   name="phone"
                                                                   class="input-text w-100">
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group-sakura">
                                                            <input type="text"
                                                                   placeholder="<?php echo getLanguage('Address') ?>"
                                                                   name="address"
                                                                   class="input-text w-100">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group-sakura">
                                                            <textarea name="node" class="w-100 p-2 textarea" rows="8"
                                                                      placeholder="<?php echo getLanguage('Note') ?>"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-12" style="margin-top: 5px">
                                                        <div class="form-group-sakura">
                                                            <button style="cursor: pointer;"
                                                                    class="btn-submit input-text w-100">{{ getLanguage('BookTour')}}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    {!! $footer !!}
@endsection

@section('script')

    <script type="text/javascript" src="/js/moment.min.js"></script>
    <script type="text/javascript" src="/js/daterangepicker.min.js"></script>
    <script type="text/javascript" src="/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            $('input[name="expected_departure_date"]').daterangepicker({
                singleDatePicker: true,
                locale: {
                    format: 'YYYY/MM/DD'
                }
            });

            function matchCustom(params, data) {
                if ($.trim(params.term).toLowerCase() === '') {
                    return data;
                }
                if (typeof data.text.toLowerCase() === 'undefined') {
                    return null;
                }
                if (data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
                    var modifiedData = $.extend({}, data, true);
                    return modifiedData;
                }
                return null;
            }

            $('select[name*=places]').select2({
                placeholder: "<?php echo getLanguage('FamousPlaces'); ?>",
                matcher: matchCustom
            });
            $('select[name*=request]').select2({
                placeholder: "<?php echo getLanguage('Requests'); ?>",
                matcher: matchCustom
            });
            $('select[name*=places]').on('select2:close', function () {
                var uldiv = $(this).siblings('span.select2').find('ul')
                let arr = [];
                $(this).find('option:selected').each((i, v) => {
                    arr.push(v.getAttribute('data-title'));
                })
                if (arr.length) {
                    $('.result-text-places').text(` `);
                    $('.result-text-places').append(`<hr><div> Địa điểm nổi tiếng > ${arr.join(', ')}</div>`);
                } else {
                    $('.result-text-places').text(` `);
                }
                var count = uldiv.find('li').length - 1;
                if (count) {
                    uldiv.html("<li> Đã chọn " + count + "</li>")
                }
            });

            $('select[name*=request]').on('select2:close', function () {
                var uldiv = $(this).siblings('span.select2').find('ul')
                let arr = [];
                $(this).find('option:selected').each((i, v) => {
                    arr.push(v.getAttribute('data-title'));
                })
                if (arr.length) {
                    $('.result-text-requests').text(` `);
                    $('.result-text-requests').append(`<hr><div> Yêu cầu > ${arr.join(', ')}</div>`);
                } else {
                    $('.result-text-requests').text(` `);
                }
                var count = uldiv.find('li').length - 1;
                if (count) {
                    uldiv.html("<li> Đã chọn " + count + "</li>")
                }
            });

            let obj = {};
            let list = $('.list-select-area .item .text');
            list.each((k, v) => {
                obj[v.innerText] = []
            });

            function checkHasObjData(obj) {
                for (let v in obj) {
                    if (obj[v].length) {
                        return true;
                    }
                }
                return false;
            }

            $('.item-check').find('input[type=checkbox]').on('change', function () {
                let name = $(this).attr('name');
                if ($(this)[0].checked) {
                    obj[name].push($(this)[0].getAttribute('value-title'))
                } else {
                    obj[name].splice(obj[name].indexOf($(this)[0].getAttribute('value-title')), 1);
                }
                if (checkHasObjData(obj)) {
                    $('.result-title').text('Bạn sẽ tới:');
                    $('.result-text').text('');
                    for (let v in obj) {
                        if (obj[v].length) {
                            let arr = obj[v].join(', ');
                            $('.result-text').append(`<div>${v} > ${arr}</div>`);
                        }
                    }
                } else {
                    $('.result-title').text('');
                    $('.result-text').text('');
                }
            });
        });
    </script>
@endsection
