@extends('client.main')
@section('banner')
@endsection
@section('content')
    @php
        echo $content_top;
    @endphp
    <div class="wp-maps pt-header">
        <iframe
                src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3241.762481418186!2d139.6940059156116!3d35.658223138840086!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1s2th+Floor+Noa+Building%2C+2-15-1+Dogenzaka%2C+Shibuya-ku%2C+Tokyo%2C+Japan!5e0!3m2!1sen!2s!4v1559290685756!5m2!1sen!2s"
                width="100%" height="auto" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <div class="wp-breadcumb">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul class="list-breadcumb">
                        @foreach ($breadcrumbs as $breadcrumb)
                            <li><a href="{{ $breadcrumb['href'] }}"><span>{{ $breadcrumb['text'] }}</span></a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="request-quote pd-sakura">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-lg-8 col-md-10 col-sm-12 col-12 m-auto">
                            <div class="content-request-quote">

                                <div class="panel-head-travel-package panel-head-general text-center">
                                    <div class="row">
                                        <div class="col-12 m-auto">
                                            <h2 class="heading-2">{{ getLanguage('ContactClient') }}</h2>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel-body-travel-package">
                                    <form action="/lienheSakura" method="POST" class="form-sakura form-contact" id="form-contact">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group-sakura" id="name-group">
                                                            <input type="text" name="fullName"
                                                                   placeholder="{{ getLanguage('FullNameClient') }}"
                                                                   class="input-text w-100">
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group-sakura" id="email-group">
                                                            <input type="text" name="email"
                                                                   placeholder="{{ getLanguage('EmailClient') }}"
                                                                   class="input-text w-100">
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group-sakura" id="phone-group">
                                                            <input type="text" name="phone"
                                                                   placeholder="{{ getLanguage('PhoneClient') }}"
                                                                   class="input-text w-100">
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group-sakura" id="address-group">
                                                            <input type="text" name="address"
                                                                   placeholder="{{ getLanguage('AddressClient') }}"
                                                                   class="input-text w-100">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group-sakura" id="node-group">
                                                            <textarea name="node" class="w-100 p-2 textarea" rows="8"
                                                                      placeholder="{{ getLanguage('NodeClient') }}"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-12" style="margin-top: 5px">
                                                        <div class="form-group-sakura">
                                                            <input type="submit" class="btn-submit input-text w-100"
                                                                   value="{{ getLanguage('SubmitClient') }}"
                                                                   style="cursor: pointer;"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <style type="text/css">
                                            .has-error .help-block {
                                                color: red;
                                            }
                                        </style>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @php
        echo $content_bottom;
    @endphp
@endsection
@section('footer')
    @php
        echo $footer;
    @endphp
@endsection
