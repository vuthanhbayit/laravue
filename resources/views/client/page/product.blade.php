@extends('client.main')
@section('content')
    @php
        echo $content_top;
    @endphp
    @if(count($breadcrumbs) > 0)
        <div class="product wp-breadcumb">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="list-breadcumb">
                            @foreach ($breadcrumbs as $breadcrumb)
                                <li><a href="{{ $breadcrumb['href'] }}"><span>{{ $breadcrumb['text'] }}</span></a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if(!empty($data['product']))
        <div class="product package-details-sakura pt-header pd-sakura">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="content-package-details">
                            <div class="panel-head-package-details d-flex">
                                <div class="address mr-auto heading-3">{{ $data['product']['title'] }}</div>
                            </div>
                            <div class="panel-body-package-details">
                                <div class="row">
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-12 pr-package-details-0">
                                        <div id="product-preview" class="product-images">
                                            @if($data['product']['image'])
                                                <div class="col-big-img" id="sys_center_img">
                                                    <div class="image col-lg-12 col-md-12">
                                                        <div class="row">
                                                            <div class="cell-center sys_show_img_big"><img
                                                                    src="{{ asset($data['product']['image']) }}"
                                                                    id="v7_BD_ZoomImg"
                                                                    data-zoom-image="{{ asset($data['product']['image']) }}"
                                                                    class="sys_img_big img-responsive"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="preview-thumbs">
                                                <div class="image col-lg-12 col-md-12">
                                                    <div class="row">
                                                        @if($data['images'])
                                                            <div id="sys_col_list_thumb" class="col-list-thumb">
                                                                <div
                                                                    class="wrap-thumb wrap-thumb sys_show_img_big sys_thumb_slide_">
                                                                    <a href="javascript:void(0);"
                                                                       data-image="{{ asset($data['product']['image']) }}"
                                                                       data-zoom-image="{{ asset($data['product']['image']) }}"
                                                                       class="v7_DB_Imgbor">
                                                                        <img
                                                                            data-img-big="{{ asset($data['product']['image']) }}"
                                                                            data-zoom-image="{{ asset($data['product']['image'])  }}"
                                                                            src="{{ asset($data['product']['image'])  }}"
                                                                            style="background-image: url('{{ asset($data['product']['image']) }}')"/>
                                                                    </a>
                                                                </div>
                                                                @foreach($data['images'] as $i => $image)
                                                                    <div
                                                                        class="wrap-thumb wrap-thumb sys_show_img_big sys_thumb_slide_{{ $i }}">
                                                                        <a href="javascript:void(0);"
                                                                           data-image="{{ asset($image['image']) }}"
                                                                           data-zoom-image="{{ asset($image['image']) }}"
                                                                           class="v7_DB_Imgbor">
                                                                            <img
                                                                                data-img-big="{{ asset($image['image']) }}"
                                                                                data-zoom-image="{{ asset($image['image']) }}"
                                                                                src="{{ asset($image['image']) }}"
                                                                                style="background-image: url('{{ asset($image['image']) }}')"/>
                                                                        </a>
                                                                    </div>
                                                                @endforeach
                                                                <div id="sys_btn_slide_thumbs" class="btn-slide-thumbs">
                                                                    <i class="fa fa-angle-right"></i>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{--<div class="img-thumb">--}}
                                        {{--<img src="{{ asset($data['product']['image']) }}" alt="" class="img-fluid">--}}
                                        {{--</div>--}}
                                    </div>
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-12 pl-package-details-0">
                                        <div class="info-package-details">
                                            <ul class="list-info">
                                                <li>
                                                    <div class="wp-item-package-details heading-3">
                                                    <span class="icon">
                                                        <img src="/images/icon-package-1.png" alt="">
                                                    </span>
                                                        <span
                                                            class="text">{{ getLanguage('CodeClient') }}: {{ $data['product']['code'] }}</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="row">
                                                        <div class="col-lg-7 col-md-6 col-sm-6 col-12">
                                                            <div class="wp-item-package-details heading-3">
                                                            <span class="icon">
                                                                <img src="/images/icon-package-2.png" alt="">
                                                            </span>
                                                                <span
                                                                    class="text">{{ getLanguage('DayNumberClient') }}: {{ $data['product']['dayNumber'] }}</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-5 col-md-6 col-sm-6 col-12">
                                                            <div class="wp-item-package-details heading-3">
                                                            <span
                                                                class="text">{{ getLanguage('DepartAddressClient') }}: {{ $data['product']['departAddress'] }}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="row">
                                                        <div class="col-lg-7 col-md-6 col-sm-6 col-12">
                                                            <div class="wp-item-package-details heading-3">
                                                            <span class="icon"><img src="/images/icon-package-3.png"
                                                                                    alt=""></span>
                                                                <span
                                                                    class="text">{{ getLanguage('DepartTimeClient') }}: {{ date("d-m-Y H:i", strtotime($data['product']['departTime']))}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="wp-item-package-details heading-3">
                                                    <span class="icon"><img src="/images/icon-package-4.png"
                                                                            alt=""></span>
                                                        <span
                                                            class="text">{{ getLanguage('SeatsExistClient') }}: {{ $data['product']['seatsExist'] }}</span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="program-tour-sakura pd-sakura">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="content-program-tour">
                            @if(!empty($data['product']))
                            <div class="panel-head-program-tour panel-head-general text-center">
                                <div class="row">
                                    <div class="col-12 col-sm-10 m-auto">
                                        <h2 class="heading-2">{{ getLanguage('TourProgramClient') }}</h2>
                                        <div class="rule"></div>
                                        <div class="intro heading-3">
                                            @php echo $data['product']['description'] @endphp
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if(count($data['day']) > 0)
                                @php
                                    $li = ''; $content = '';
                                @endphp
                                @foreach($data['day'] as $i => $item)
                                    @php
                                        if($i == 0) $active = 'active';else $active = '';
                                        $li .='<li class="tab-big '.$active.'">
                                                <a id="day-'.$i.'-tab" data-toggle="pill" href="#day-'.$i.'" role="tab"
                                                   aria-selected="true">
                                                    <div class="tab tab-1 heading-3">'.$item['title'].'</div>
                                                </a>
                                            </li>';
                                        $content .= '<div class="tab-pane fade '.$active.' show" id="day-'.$i.'">
                                            <div class="content-tab-program-tour animated fadeInUp">
                                                <div class="intro heading-3">
                                                    '.$item['content'].'
                                                </div>
                                            </div>
                                        </div>';
                                    @endphp
                                @endforeach
                                <div class="row">
                                    <div class="col-12 col-sm-10 col-lg-10 col-md-10 m-auto">
                                        <div class="panel-body-program-tour">
                                            <div class="tab-program-tour">
                                                <ul class="nav nav-pills list" id="pills-tab" role="tablist">
                                                    @php echo $li; @endphp
                                                </ul>
                                            </div>

                                            <div class="tab-content" id="pills-tabContent">
                                                @php echo $content; @endphp
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(count($data['attribute']) > 0)
            <div class="detail-tour-sakura pd-sakura">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-detail-tour">
                                <div class="panel-head-detail-tour panel-head-general text-center">
                                    <div class="row">
                                        <div class="col-12 col-sm-10 m-auto">
                                            <h2 class="heading-2">{{ getLanguage('DetailTourClient') }}</h2>
                                            <div class="rule"></div>
                                        </div>
                                    </div>
                                </div>
                                @foreach($data['attribute'] as $i => $item)
                                    <div class="row">
                                        <div class="col-12">
                                            <h4 class="heading-4 name-info-tour mb-30">
                                                <span class="icon"><img src="{{ asset($item['icon']) }}" alt=""></span>
                                                <span class="text">{{ $item['title'] }}</span>
                                            </h4>
                                            <div class="wp-big-info-tour mb-30 row">
                                                <div class="col-12">
                                                    @php echo $item['content']; @endphp
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if(trim($data['product']['content']) != '')
            <div class="information-sakura pd-sakura">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-information-sakura">
                                <div class="panel-head-information panel-head-general text-center">
                                    <div class="row">
                                        <div class="col-12 col-sm-10 m-auto">
                                            <h2 class="heading-2">{{ getLanguage('ContentTourClient') }}</h2>
                                            <div class="rule"></div>
                                            <p class="detail-information-sakura heading-3">
                                                @php echo $data['product']['content'] ; @endphp
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if(count($data['related']) > 0)
            <div class="travel-package pd-sakura">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-travel-package">
                                <div class="panel-head-travel-package panel-head-general text-center">
                                    <div class="row">
                                        <div class="col-12 col-sm-10 m-auto">
                                            <h2 class="heading-2">{{ getLanguage('ProductRelateClient') }}</h2>
                                            <div class="rule"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body-travel-package">
                                    <ul class="list row">
                                        @foreach($data['related'] as $i => $item)
                                            <li class="col-lg-4 col-md-6 col-sm-6 col-12">
                                                <article class="article-travel-package">
                                                    <div class="img-thumb">
                                                        <a href="/p{{$item['id']}}" class="image-cover image">
                                                            <img src="{{ asset($item['image']) }}" alt=""
                                                                 class="img-fluid image-cover">
                                                        </a>
                                                    </div>
                                                    <div class="info">
                                                        <div class="meta">
                                                            {{$item['dayNumber']}} {{ getLanguage('StartFromClient') }} {{$item['price']}} {{ getConfig('config_currency') }}
                                                        </div>
                                                        <h3 class="title"><a  href="/p{{ $item['id'] }}">{{ $item['title'] }}</a>
                                                        </h3>
                                                        {{--<div class="view"><a href="/p{{$item['id']}}"--}}
                                                        {{--class="view-more heading-3">{{ getLanguage('ViewMore') }}</a>--}}
                                                        {{--</div>--}}
                                                    </div>
                                                </article>
                                            </li>
                                        @endforeach

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endif
    @php
        echo $content_bottom;
    @endphp
@endsection
@section('footer')
    @php
        echo $footer;
    @endphp
@endsection
