@extends('client.main')
@section('banner')
    @if(count($banner) > 0)
        <div class="banner-horizontal pt-header">
            <div class="swiper-container wp-slide-banner" id="wp-slide-banner">
                <div class="swiper-wrapper slide-banner">
                    @foreach($banner as $item)
                        <div class="swiper-slide item-slide">
                            <div class="img-thumb">
                                <a href="{{ $item->link }}" class="item-slide-banner ">
                                    <img src="{{ asset($item->image) }}" alt="" class="image-cover"/>
                                </a>
                            </div>
                            <div class="info-banner container">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="overlay-banner">
                                            <h3 class="title text-white">{{ $item->title }}</h3>
                                            <div class="text text-white subtitle mb-3">
                                                @php echo $item->content @endphp
                                            </div>
                                            <a href="{{ $item->link }}"
                                               class="readmore text-white">{{ getLanguage('ViewMore') }}</a>
                                        </div>
                                    </div>
                                    <div class="col-lg-5"></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination pagination-banner"></div>
            </div>
        </div>
    @endif
@endsection
@section('content')
    @if(count($breadcrumbs) > 0)
        <div class="wp-breadcumb">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="list-breadcumb">
                            @foreach ($breadcrumbs as $breadcrumb)
                                <li><a href="{{ $breadcrumb['href'] }}"><span>{{ $breadcrumb['text'] }}</span></a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @php
        echo $content_top;
    @endphp
    <div class="reviews travel-guide-sakura pd-sakura">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="content-travel-guide">
                        <div class="panel-head-travel-package panel-head-general text-center">
                            <div class="row">
                                <div class="col-12 m-auto">
                                    <h2 class="heading-2">{{ getLanguage('TitleReviewsClient') }}</h2>
                                    <div class="rule"></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body-travel-guide">
                            <ul class="list">
                                @foreach($data as $item)
                                    <li>
                                        <article class="article-travel-guide row">
                                            <div class="col-lg-4 col-md-5 col-sm-5 col-12">
                                                <div class="img-thumb">
                                                    <a href="/rv{{ $item['id'] }}" class=" image img-flash">
                                                        <img src="{{ asset($item['image']) }}"
                                                             alt="{{ $item['title'] }}"
                                                             class="img-fluid w-100 image-cover">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-7 col-sm-7 col-12">
                                                <div class="info">
                                                    <h3 class="heading-3 title line-2">
                                                        <a href="/rv{{ $item['id'] }}" title=""
                                                           class="text-color">{{ $item['title'] }}</a>
                                                    </h3>
                                                    <div
                                                        class="meta-date">{{ date("d-m-Y", strtotime($item['created_at']))}}</div>
                                                    <div class="rule"></div>
                                                    <div
                                                        class="intro heading-3 line-4">
                                                        @if($item['description'] != null || trim($item['description']) != '')
                                                            {!! shorten_string($item['description'], 50) !!}
                                                        @else
                                                            {!! shorten_string($item['content'], 50) !!}
                                                        @endif
                                                    </div>
                                                    <div class="view">
                                                        <a href="/rv{{ $item['id'] }}"
                                                           class="view-more heading-3">{{ getLanguage('ViewMore') }}</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="view text-center">
                                <nav>
                                    <ul class="pagination clearfix">
                                        {!! $data->links() !!}
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @php
        echo $content_bottom;
    @endphp
@endsection
@section('footer')
    @php
        echo $footer;
    @endphp
@endsection
