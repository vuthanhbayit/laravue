<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuickQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quick_quotes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('expected_departure_date');
            $table->integer('expected_number_of_days');
            $table->integer('departure_from_id');
            $table->integer('number_of_adults');
            $table->integer('number_of_children');
            $table->integer('number_of_babies');
            $table->integer('customer_id');
            $table->string('special_requirements');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quick_quotes');
    }
}
