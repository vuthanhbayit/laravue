<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Yadahan\AuthenticationLog\AuthenticationLogable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\models\Cms\RoleUser;
class User extends Authenticatable
{
    use Notifiable, HasApiTokens,AuthenticationLogable;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function RoleUser()
    {
        return $this->hasOne(RoleUser::class, 'user_id', 'id');
    }
    public function rule()
    {
        return [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'role' => 'required',
        ];
    }
    public function ruleEdit()
    {
        return [
            
            'password' => 'required',
            
        ];
    }

    public function getListUser()
    {
        $query = $this->with([
                        'RoleUser.Roles'
                    ])
                    ->get();
        return $query;
    }
    public function getDetailUser($id)
    {
        $query = $this->with([
                        'RoleUser.Roles'
                    ])
                    ->where('id',$id)
                    ->first();
        return $query;
    }
}
