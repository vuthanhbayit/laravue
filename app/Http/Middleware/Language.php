<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cookie;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = '';
        $language = new \App\models\Client\Language();
        $languages = $language->getLanguages();
        if ($request->cookie('locale') != null && !array_key_exists($locale, $languages)) {
            $locale = $request->cookie('locale');
        }
        if (!empty($request->server('HTTP_ACCEPT_LANGUAGE')) && !array_key_exists($locale, $languages)) {
            $detect = '';
            $browser_languages = explode(',', $request->server('HTTP_ACCEPT_LANGUAGE'));
            foreach ($browser_languages as $browser_language) {
                foreach ($languages as $key => $value) {
                    $locale = explode(',', $value['locale']);
                    if (in_array($browser_language, $locale)) {
                        $detect = $key;
                        break 2;
                    }
                }
            }
            if (!$detect) {
                foreach ($browser_languages as $browser_language) {
                    if (array_key_exists(strtolower($browser_language), $languages)) {
                        $detect = strtolower($browser_language);
                        break;
                    }
                }
            }
            $locale = $detect ? $detect : '';
        }
        if (!array_key_exists($locale, $languages)) {
            $locale = getConfig('config_language');
        }
        if ($request->cookie('locale') == null || $request->cookie('locale') != $locale) {
            Cookie::queue('locale', $locale, time() + 60 * 60 * 24 * 30);
        }

        app()->setLocale($locale);

        return $next($request);
    }
}
