<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\models\Cms\Setting;

class AdminController extends Controller
{
    public function index(Setting $setting)
    {
        $data = $setting->getSetting();
        if($data['config_mode'] === 'development' ){
            $data['apiUrl'] = $data['storageApiUrlLocal'];
        }else{
            $data['apiUrl'] = $data['storageApiUrlProduct'];
        }
        return view('admin.index', compact('data'));
    }
}
