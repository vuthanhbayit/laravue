<?php

namespace App\Http\Controllers\Client;

use App\models\Client\BannerGroup;
use App\models\Client\BlogCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

class BlogCategoryController extends Controller
{
    public function index(Request $request)
    {
        $blogCategoryId = $request->route('id');
        $blogCategory = new BlogCategory();
        $bannerGroup = new BannerGroup();
        $content_top = App::make('App\Http\Controllers\Client\ContentTopController')->index('blog');
        $content_bottom = App::make('App\Http\Controllers\Client\ContentBottomController')->index('blog');
        $footer = App::make('App\Http\Controllers\Client\FooterController')->index('allPage');
        $data['blog_category'] = $blogCategory->getBlogCategory($blogCategoryId);
        $data['banner'] = $bannerGroup->getBanner('blog', $data['blog_category']['banner_group_id']);
        $data['blog_post'] = $blogCategory->getBlogCategoryPost($blogCategoryId, 10);
        $data['sub_blog_category'] = $blogCategory->getBlogSubCategory($blogCategoryId);

        $title = $data['blog_category']['meta_title'] ? $data['blog_category']['meta_title'] : $data['blog_category']['title'];
        $description = $data['blog_category']['meta_description'] ? shorten_string(html_entity_decode(strip_tags($data['blog_category']['meta_description']), ENT_QUOTES, 'UTF-8'), 70) : shorten_string(html_entity_decode(strip_tags($data['blog_category']['description']), ENT_QUOTES, 'UTF-8'), 70);

        $breadcrumbs = array();
        if (!empty($data['blog_category'])) {
            $breadcrumbs[] = array(
                'text' => getLanguage('HomeClient'),
                'href' => '/',
                'separator' => false
            );
            $breadcrumbs[] = array(
                'text' => $data['blog_category']['title'],
                'href' => '/bc' . $blogCategoryId,
                'separator' => false
            );
        }
        return view('client.page.blog_category', [
            'data' => $data,
            'title' => $title,
            'description' => $description,
            'breadcrumbs' => $breadcrumbs,
            'content_top' => $content_top,
            'content_bottom' => $content_bottom,
            'footer' => $footer,
        ]);
    }
}
