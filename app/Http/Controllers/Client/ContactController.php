<?php

namespace App\Http\Controllers\Client;

use App\models\Client\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

class ContactController extends Controller
{
    public function index()
    {
        $breadcrumbs = array();
        $breadcrumbs[] = array(
            'text' => getLanguage('HomeClient'),
            'href' => '/',
            'separator' => false
        );
        $breadcrumbs[] = array(
            'text' => getLanguage('ContactClient'),
            'href' => '/contact',
            'separator' => false
        );
        $content_top = App::make('App\Http\Controllers\Client\ContentTopController')->index('contact');
        $content_bottom = App::make('App\Http\Controllers\Client\ContentBottomController')->index('contact');
        $footer = App::make('App\Http\Controllers\Client\FooterController')->index('allPage');
        $title = getLanguage('TitleContactSeoClient');
        $description = getLanguage('DescriptionContactSeoClient');
        return view('client.page.contact', [
            'title' => $title,
            'description' => $description,
            'breadcrumbs' => $breadcrumbs,
            'content_top' => $content_top,
            'content_bottom' => $content_bottom,
            'footer' => $footer,
        ]);
    }

    public function contact(Request $request)
    {
        $errors = array();
        if (empty($request->fullName))
            $errors['fullName'] = getLanguage('NameIsRequired');

        if (empty($request->email))
            $errors['email'] = getLanguage('EmailIsRequired');

        if (empty($request->phone))
            $errors['phone'] = getLanguage('PhoneIsRequired');

        if (empty($request->address))
            $errors['address'] = getLanguage('AddressIsRequired');

        if (empty($request->node))
            $errors['node'] = getLanguage('NodeIsRequired');

        if (!empty($errors)) {
            return response()->json([
                'success' => false,
                'message' => "Send error",
                'errors' => $errors,
            ]);
        } else {
            Contact::create([
                'fullName' => $request->fullName, 'email' => $request->email,
                'phone' => $request->phone, 'address' => $request->address, 'node' => $request->node,
            ]);
            return response()->json([
                'success' => true,
                'message' => "You send mail success",
            ]);
        }
    }
}
