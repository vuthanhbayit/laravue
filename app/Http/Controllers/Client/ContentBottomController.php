<?php

namespace App\Http\Controllers\Client;

use App\models\Client\Extension\Module;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
class ContentBottomController extends Controller
{
    public function index($page = 'home')
    {
        $expiresAt = Carbon::now()->addHours(24);
        $modules = array();
        if (Cache::has('contentBottom_' . $page . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $modules = Cache::get('contentBottom_' . $page . '_' . app()->getLocale());
        } else {
            $data = Module::where([
                ['page', '=', $page],
                ['position', '=', 'content_bottom'],
                ['status', '=', 1]
            ])->orderBy('sort_order', 'ASC')
                ->get();

            foreach ($data as $item) {
                $modules[] = \App::make('App\Http\Controllers\Client\Extension\Module\\' . ucfirst($item->code) . 'Controller')->index($item->setting, $item->id);
            }
            if (getConfig('config_debug') == 0) {
                Cache::put('contentBottom_' . $page . '_' . app()->getLocale(), $modules, $expiresAt);
            }
        }
        $html = view('client.layouts.content_bottom', compact('modules'))->render();
        return $html;
    }
}
