<?php

namespace App\Http\Controllers\Client;

use App\models\Client\Category;
use App\models\Client\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $productId = $request->route('id');
        $product = new Product();

        $data['product'] = $product->getProduct($productId);
        $data['attribute'] = $product->getProductAttribute($productId);
        $data['images'] = $product->getProductImage($productId);
        $data['day'] = $product->getProductDay($productId);
        $data['related'] = $product->getProductRelated($productId);

        $content_top = App::make('App\Http\Controllers\Client\ContentTopController')->index('product');
        $content_bottom = App::make('App\Http\Controllers\Client\ContentBottomController')->index('product');
        $footer = App::make('App\Http\Controllers\Client\FooterController')->index('allPage');
        $title = $data['product']['meta_title'] ? $data['product']['meta_title'] : $data['product']['title'];
        $description = $data['product']['meta_description'] ? shorten_string(html_entity_decode(strip_tags($data['product']['meta_description']), ENT_QUOTES, 'UTF-8'), 70) : shorten_string(html_entity_decode(strip_tags($data['product']['description']), ENT_QUOTES, 'UTF-8'), 70);

        Product::where('id', $productId)->increment('viewed', 1);
        $breadcrumbs = array();
        if (!empty($data['product'])) {
            $breadcrumbs[] = array(
                'text' => getLanguage('HomeClient'),
                'href' => '/',
                'separator' => false
            );
            $breadcrumbs[] = array(
                'text' => ucfirst($data['product']['title']),
                'href' => '/p' . $productId,
                'separator' => false
            );
        }
        return view('client.page.product', [
            'data' => $data,
            'title' => $title,
            'description' => $description,
            'breadcrumbs' => $breadcrumbs,
            'content_top' => $content_top,
            'content_bottom' => $content_bottom,
            'footer' => $footer,
        ]);
    }
}
