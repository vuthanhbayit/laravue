<?php

namespace App\Http\Controllers\Client;

use App\models\Client\BannerGroup;
use App\models\Client\Language;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Config;
use App\Library\Locate;
class HomeController extends Controller
{
    public function index()
    {
        $bannerGroup = new BannerGroup();
        $banner = $bannerGroup->getBanner('home');
        $content_top = App::make('App\Http\Controllers\Client\ContentTopController')->index('home');
        $content_bottom = App::make('App\Http\Controllers\Client\ContentBottomController')->index('home');
        $footer = App::make('App\Http\Controllers\Client\FooterController')->index('allPage');
        $title = getConfig('config_meta_title');
        $description = getConfig('config_meta_description');
        return view('client.page.home', [
            'title' => $title,
            'description' => $description,
            'banner' => $banner,
            'content_top' => $content_top,
            'content_bottom' => $content_bottom,
            'footer' => $footer
        ]);
    }
}
