<?php

namespace App\Http\Controllers\Client;

use App\models\Client\Information;
use App\models\Client\BannerGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

class InformationController extends Controller
{
    public function index(Request $request)
    {
        $informationId = $request->route('id');
        $information = new Information();
        $content_top = App::make('App\Http\Controllers\Client\ContentTopController')->index('information');
        $content_bottom = App::make('App\Http\Controllers\Client\ContentBottomController')->index('information');
        $footer = App::make('App\Http\Controllers\Client\FooterController')->index('allPage');
        $data = $information->getInformation($informationId);
        $title = $data['information']['meta_title'] ? $data['information']['meta_title'] : $data['information']['title'];
        $description = $data['information']['meta_description'] ? shorten_string(html_entity_decode(strip_tags($data['information']['meta_description']), ENT_QUOTES, 'UTF-8'),70) : shorten_string(html_entity_decode(strip_tags($data['information']['description']), ENT_QUOTES, 'UTF-8'), 70);

        $breadcrumbs = array();
        if (!empty($data['information'])) {
            $breadcrumbs[] = array(
                'text' => getLanguage('HomeClient'),
                'href' => '/',
                'separator' => false
            );
            $breadcrumbs[] = array(
                'text' => $data['information']['title'],
                'href' => '/i' . $informationId,
                'separator' => false
            );
        }
        return view('client.page.information', [
            'data' => $data,
            'title' => $title,
            'description' => $description,
            'breadcrumbs' => $breadcrumbs,
            'content_top' => $content_top,
            'content_bottom' => $content_bottom,
            'footer' => $footer,
        ]);
    }

    public function introduction()
    {
        $banner = new BannerGroup();
        $content_top = App::make('App\Http\Controllers\Client\ContentTopController')->index('pagestatic');
        $content_bottom = App::make('App\Http\Controllers\Client\ContentBottomController')->index('information');
        $footer = App::make('App\Http\Controllers\Client\FooterController')->index('allPage');
        $banner = $banner->getBanner('pageStatic');

        $title = getLanguage('IntroductionTitleClient');
        $description = getLanguage('IntroductionDescriptionClient');

        $breadcrumbs = array();
        $breadcrumbs[] = array(
            'text' => getLanguage('HomeClient'),
            'href' => '/',
            'separator' => false
        );
        $breadcrumbs[] = array(
            'text' => $title,
            'href' => '/introduction',
            'separator' => false
        );
        return view('client.page.introduction', [
            'banner' => $banner,
            'title' => $title,
            'description' => $description,
            'breadcrumbs' => $breadcrumbs,
            'content_top' => $content_top,
            'content_bottom' => $content_bottom,
            'footer' => $footer,
        ]);
    }
}
