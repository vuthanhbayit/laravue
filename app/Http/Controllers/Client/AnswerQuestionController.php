<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

class AnswerQuestionController extends Controller
{
    public function index()
    {
        $breadcrumbs = array();
        $breadcrumbs[] = array(
            'text' => getLanguage('HomeClient'),
            'href' => '/',
            'separator' => false
        );
        $breadcrumbs[] = array(
            'text' => getLanguage('AnswerQuestions'),
            'href' => '/contact',
            'separator' => false
        );
        $content_top = App::make('App\Http\Controllers\Client\ContentTopController')->index('contact');
        $content_bottom = App::make('App\Http\Controllers\Client\ContentBottomController')->index('contact');
        $footer = App::make('App\Http\Controllers\Client\FooterController')->index('allPage');
        $title = getLanguage('TitleAnswerQuestionsSeo');
        $description = getLanguage('DescriptionAnswerQuestionsSeo');
        return view('client.page.answerQuestions', [
            'title' => $title,
            'description' => $description,
            'breadcrumbs' => $breadcrumbs,
            'content_top' => $content_top,
            'content_bottom' => $content_bottom,
            'footer' => $footer,
        ]);
    }
}
