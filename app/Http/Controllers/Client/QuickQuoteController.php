<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\models\Client\BannerGroup;
use App\models\Client\Contact;
use App\models\Client\Place;
use App\models\Client\Province;
use App\models\Client\RequestClient;
use App\models\Client\QuickQuote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class QuickQuoteController extends Controller
{
    public function index() {
        $bannerGroup = new BannerGroup();
        $places = new Place();
        $requests = new RequestClient();
        $provinces = new Province();
        $banner = $bannerGroup->getBanner('home');
        $content_top = App::make('App\Http\Controllers\Client\ContentTopController')->index('reviews');
        $content_bottom = App::make('App\Http\Controllers\Client\ContentBottomController')->index('reviews');
        $footer = App::make('App\Http\Controllers\Client\FooterController')->index('allPage');
        $title = getLanguage('TitleQuickQuoteClient');
        $description = getLanguage('DescriptionQuickQuoteClient');
        $data = array();
        $data['places'] = $places->getPlace();
        $data['requests'] = $requests->getRequest();
        $data['provinces'] = $provinces->getProvince();
        $breadcrumbs = array();
        $breadcrumbs[] = array(
            'text' => getLanguage('HomeClient'),
            'href' => '/',
            'separator' => false
        );
        $breadcrumbs[] = array(
            'text' => getLanguage('TitleQuickQuoteClient'),
            'href' => '/quickQuote',
            'separator' => false
        );
        return view('client.page.quickQuote', [
            'data' => $data,
            'title' => $title,
            'banner' => $banner,
            'description' => $description,
            'places' => $places,
            'breadcrumbs' => $breadcrumbs,
            'content_top' => $content_top,
            'content_bottom' => $content_bottom,
            'footer' => $footer,
        ]);
    }

    public function store(Request $request) {
        $errors = array();

        if (empty($request->expected_departure_date))
            $errors['expected_departure_date'] = getLanguage('ExpectedDepartureDateIsRequired');

        if (empty($request->expected_number_of_days))
            $errors['expected_number_of_days'] = getLanguage('ExpectedNumberOfDaysIsRequired');

        if (empty($request->departure_from_id))
            $errors['departure_from_id'] = getLanguage('DepartureFromIdIsRequired');

        if (empty($request->number_of_adults))
            $errors['number_of_adults'] = getLanguage('NumberOfAdultsIsRequired');

        if (empty($request->fullName))
            $errors['fullName'] = getLanguage('NameIsRequired');

        if (empty($request->phone))
            $errors['phone'] = getLanguage('PhoneIsRequired');

        if (!empty($errors)) {
            return response()->json([
                'success' => false,
                'message' => "Send error",
                'errors' => $errors,
            ]);
        } else {
            $contact = Contact::create([
                'fullName' => $request->fullName,
                'email' => $request->email,
                'phone' => $request->phone,
                'address' => $request->address,
                'node' => $request->node,
            ]);
            QuickQuote::create([
                'expected_departure_date' => $request->expected_departure_date,
                'expected_number_of_days' => $request->expected_number_of_days,
                'departure_from_id' => $request->departure_from_id,
                'number_of_adults' => $request->number_of_adults,
                'number_of_children' => $request->number_of_children,
                'number_of_babies' => $request->number_of_babies,
                'criterias' => json_encode($request->criterias),
                'contact_id' => $contact->id,
                'special_requirements' => $request->special_requirements || '',
                'status' => 1
            ]);
            return response()->json([
                'success' => true,
                'message' => "You send quick quote success",
            ]);
        }
    }
}
