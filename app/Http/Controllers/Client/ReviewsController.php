<?php

namespace App\Http\Controllers\Client;

use App\models\Client\BannerGroup;
use App\models\Client\Reviews;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

class ReviewsController extends Controller
{
    public function index()
    {
        $reviews = new Reviews();
        $bannerGroup = new BannerGroup();
        $content_top = App::make('App\Http\Controllers\Client\ContentTopController')->index('reviews');
        $content_bottom = App::make('App\Http\Controllers\Client\ContentBottomController')->index('reviews');
        $footer = App::make('App\Http\Controllers\Client\FooterController')->index('allPage');
        $banner = $bannerGroup->getBanner('reviews');
        $data = $reviews->getReviews(10);
        $title = getLanguage('TitleReviewsSeoClient');
        $description = getLanguage('DescriptionReviewsSeoClient');
        $breadcrumbs = array();
        $breadcrumbs[] = array(
            'text' => getLanguage('HomeClient'),
            'href' => '/',
            'separator' => false
        );
        $breadcrumbs[] = array(
            'text' => getLanguage('TitleReviewsClient'),
            'href' => '/reviews',
            'separator' => false
        );
        return view('client.page.reviews', [
            'data' => $data,
            'title' => $title,
            'description' => $description,
            'banner' => $banner,
            'breadcrumbs' => $breadcrumbs,
            'content_top' => $content_top,
            'content_bottom' => $content_bottom,
            'footer' => $footer,
        ]);
    }

    public function getReviews(Request $request)
    {
        $reviewsId = $request->route('id');
        $reviews = new Reviews();
        $data = $reviews->getDetailReviews($reviewsId);
        $content_top = App::make('App\Http\Controllers\Client\ContentTopController')->index('reviews');
        $content_bottom = App::make('App\Http\Controllers\Client\ContentBottomController')->index('reviews');
        $footer = App::make('App\Http\Controllers\Client\FooterController')->index('allPage');
        $title = $data['meta_title'] ? $data['meta_title'] : $data['title'];
        $description = $data['meta_description'] ? shorten_string(html_entity_decode(strip_tags($data['meta_description']), ENT_QUOTES, 'UTF-8'), 70) : shorten_string(html_entity_decode(strip_tags($data['description']), ENT_QUOTES, 'UTF-8'), 70);

        $breadcrumbs = array();
        Reviews::where('id', $reviewsId)->increment('viewed', 1);
        if (!empty($data)) {
            $breadcrumbs[] = array(
                'text' => getLanguage('HomeClient'),
                'href' => '/',
                'separator' => false
            );
            $breadcrumbs[] = array(
                'text' => $data['title'],
                'href' => '/rv' . $reviewsId,
                'separator' => false
            );
        }
        return view('client.page.detail_reviews', [
            'data' => $data,
            'title' => $title,
            'description' => $description,
            'breadcrumbs' => $breadcrumbs,
            'content_top' => $content_top,
            'content_bottom' => $content_bottom,
            'footer' => $footer,
        ]);
    }
}
