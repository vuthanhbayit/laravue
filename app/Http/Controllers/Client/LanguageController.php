<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class LanguageController extends Controller
{
    public function index(Request $request){
        if (isset($request->code)) {
            Cookie::forget('locale');
            Cookie::queue('locale', $request->code, time() + 60 * 60 * 24 * 30);
        }
        return redirect()->back();
    }
}
