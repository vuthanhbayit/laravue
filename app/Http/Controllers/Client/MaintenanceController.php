<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

class MaintenanceController extends Controller
{
    public function index()
    {
        return view('client.maintenance');
    }
}
