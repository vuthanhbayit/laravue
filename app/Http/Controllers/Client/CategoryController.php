<?php

namespace App\Http\Controllers\Client;

use App\models\Client\Category;
use App\models\Client\BannerGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $category = new Category();
        $bannerGroup = new BannerGroup();
        $categoryId = $request->route('id');
        $content_top = App::make('App\Http\Controllers\Client\ContentTopController')->index('category');
        $content_bottom = App::make('App\Http\Controllers\Client\ContentBottomController')->index('category');
        $footer = App::make('App\Http\Controllers\Client\FooterController')->index('allPage');
        $data['category'] = $category->getCategory($categoryId);
        $data['banner'] = $bannerGroup->getBanner('category', $data['category']['banner_group_id']);

        $data['product'] = $category->getCategoryProduct($categoryId, 10);
        $data['goodPoint'] = $category->getGoodPoint($categoryId);

        $title = $data['category']['meta_title'] ? $data['category']['meta_title'] : $data['category']['title'];
        $description = $data['category']['meta_description'] ? shorten_string(html_entity_decode(strip_tags($data['category']['meta_description']), ENT_QUOTES, 'UTF-8'),70) : shorten_string(html_entity_decode(strip_tags($data['category']['description']), ENT_QUOTES, 'UTF-8'), 70);

        $breadcrumbs = array();
        if (!empty($data['category'])) {
            $breadcrumbs[] = array(
                'text' => getLanguage('HomeClient'),
                'href' => '/',
                'separator' => false
            );
            $breadcrumbs[] = array(
                'text' => $data['category']['title'],
                'href' => '/c' . $categoryId,
                'separator' => false
            );
        }
        return view('client.page.category', [
            'data' => $data,
            'title' => $title,
            'description' => $description,
            'breadcrumbs' => $breadcrumbs,
            'content_top' => $content_top,
            'content_bottom' => $content_bottom,
            'footer' => $footer,
        ]);
    }
}
