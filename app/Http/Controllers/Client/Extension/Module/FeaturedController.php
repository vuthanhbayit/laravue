<?php

namespace App\Http\Controllers\Client\Extension\Module;

use App\models\Client\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\models\Client\Extension\ModuleDescription;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use View;

class FeaturedController extends Controller
{
    public function index($setting, $module_id = 0)
    {
        $product = new Product();
        $data = json_decode($setting, true);
        if (!$data['limit']) {
            $data['limit'] = 6;
        }
        $data['descriptions'] = $this->getModuleDescription($module_id);
        if (!empty($data['product'])) {
            $products = array_slice($data['product'], 0, (int)$data['limit']);
            unset($data['product']);
            $data['product'] = array();
            foreach ($products as $i => $item) {
                $detail = $product->getProduct($item['id']);
                if ($detail) {
                    $data['product'][$i] = $detail;
                }
            }
        }
        $html = view('client.extension.module.featured', compact('data'))->render();
        return $html;
    }

    public function getModuleDescription($module_id = 0)
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('moduleDescription_' . $module_id .  '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $data = Cache::get('moduleDescription_' . $module_id .  '_' . app()->getLocale());
            return $data;
        } else {
            $data = ModuleDescription::where([
                ['module_id', '=', $module_id],
                ['language_id', '=', app()->getLocale()]
            ])
                ->select('title', 'description', 'content')
                ->first();
            if (getConfig('config_debug') == 0) {
                Cache::put('moduleDescription_' . $module_id .  '_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }
}
