<?php

namespace App\Http\Controllers\Client\Extension\Module;

use App\models\Client\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LatestController extends Controller
{
    public function index($setting)
    {
        $product = new Product();
        $data = json_decode($setting, true);
        if (!$data['limit']) {
            $data['limit'] = 6;
        }
        $data['products'] = $product->getProducts($data['limit']);
        $html = view('client.extension.module.latest', compact('data'))->render();
        return $html;
    }
}
