<?php

namespace App\Http\Controllers\Client\Extension\Module;

use App\models\Client\BlogCategory;
use App\models\Client\BlogPost;
use App\models\Client\Extension\ModuleDescription;
use App\models\Client\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use View;

class BlogCategoryController extends Controller
{
    public function index($setting, $module_id = 0)
    {
        $data = json_decode($setting, true);
        if (!$data['limit']) {
            $data['limit'] = 4;
        }
        $data['descriptions'] = $this->getModuleDescription($module_id);
        $blog = array();
        $blogPost = new BlogPost();
        $blogCategory = new BlogCategory();
        if (!empty($data['category'])) {
            $categories = array_slice($data['category'], 0, (int)$data['limit']);
            unset($data['category']);
            foreach ($categories as $i => $item) {
                $blog[$i]['category'] = $blogCategory->getBlogCategory($item['id']);
                $blog[$i]['post'] = $blogPost->getBlogPosts(5, $item['id']);
            }
        }
        $html = view('client.extension.module.blog_category', compact('data', 'blog'))->render();
        return $html;
    }

    public function getModuleDescription($module_id = 0)
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('moduleDescription_' . $module_id .  '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $data = Cache::get('moduleDescription_' . $module_id .  '_' . app()->getLocale());
            return $data;
        } else {
            $data = ModuleDescription::where([
                ['module_id', '=', $module_id],
                ['language_id', '=', app()->getLocale()]
            ])
                ->select('title', 'description', 'content')
                ->first();
            if (getConfig('config_debug') == 0) {
                Cache::put('moduleDescription_' . $module_id .  '_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }
}
