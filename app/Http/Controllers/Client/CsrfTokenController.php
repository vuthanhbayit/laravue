<?php

namespace App\Http\Controllers\Client;

use App\models\Cms\Extension\Module;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class CsrfTokenController extends Controller
{
    public function index()
    {
        return response()->json([
            'success' => true,
            "message" => "Get success",
            'data' => Session::token()
        ]);
    }
}
