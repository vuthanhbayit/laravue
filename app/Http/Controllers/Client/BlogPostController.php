<?php

namespace App\Http\Controllers\Client;

use App\models\Client\BlogCategory;
use App\models\Client\BlogPost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

class BlogPostController extends Controller
{
    public function index(Request $request)
    {
        $blogPostId = $request->route('id');
        $blogPost = new BlogPost();
        $data = $blogPost->getBlogPost($blogPostId);
        $content_top = App::make('App\Http\Controllers\Client\ContentTopController')->index('blog');
        $content_bottom = App::make('App\Http\Controllers\Client\ContentBottomController')->index('blog');
        $footer = App::make('App\Http\Controllers\Client\FooterController')->index('allPage');
        $title = $data['meta_title'] ? $data['meta_title'] : $data['title'];
        $description = $data['meta_description'] ? shorten_string(html_entity_decode(strip_tags($data['meta_description']), ENT_QUOTES, 'UTF-8'), 70) : shorten_string(html_entity_decode(strip_tags($data['description']), ENT_QUOTES, 'UTF-8'), 70);

        BlogPost::where('id', $blogPostId)->increment('viewed', 1);
        $breadcrumbs = array();
        if (!empty($data)) {
            $breadcrumbs[] = array(
                'text' => getLanguage('HomeClient'),
                'href' => '/',
                'separator' => false
            );
            $breadcrumbs[] = array(
                'text' => $data['title'],
                'href' => '/bp' . $blogPostId,
                'separator' => false
            );
        }

        return view('client.page.blog_post', [
            'data' => $data,
            'title' => $title,
            'description' => $description,
            'breadcrumbs' => $breadcrumbs,
            'content_top' => $content_top,
            'content_bottom' => $content_bottom,
            'footer' => $footer,
        ]);
    }
}
