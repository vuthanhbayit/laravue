<?php

namespace App\Http\Controllers\Client;

use App\models\Client\Extension\Module;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
class ContentTopController extends Controller
{
    public function index($page = 'home')
    {
        $expiresAt = Carbon::now()->addHours(24);
        $modules = array();
        if (Cache::has('contentTop_' . $page . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $modules = Cache::get('contentTop_' . $page . '_' . app()->getLocale());
        } else {
            $data = Module::where([
                ['module.page', '=', $page],
                ['module.position', '=', 'content_top'],
                ['module.status', '=', 1],
                ['module_description.language_id', '=', app()->getLocale()]
            ])
                ->join('module_description', 'module_description.module_id', '=', 'module.id')
                ->select('module.*', 'module_description.title AS title', 'module_description.description AS description', 'module_description.content AS content')
                ->orderBy('sort_order', 'ASC')
                ->get();
            foreach ($data as $item) {
                $modules[] = \App::make('App\Http\Controllers\Client\Extension\Module\\' . ucfirst($item->code) . 'Controller')->index($item->setting, $item->id);
            }
            if (getConfig('config_debug') == 0) {
                Cache::put('contentTop_' . $page . '_' . app()->getLocale(), $modules, $expiresAt);
            }
        }
        $html = view('client.layouts.content_top', compact('modules'))->render();
        return $html;
    }
}
