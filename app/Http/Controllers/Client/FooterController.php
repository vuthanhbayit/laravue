<?php

namespace App\Http\Controllers\Client;

use App\models\Cms\Extension\Module;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
class FooterController extends Controller
{
    public function index($page = 'allPage')
    {
        $expiresAt = Carbon::now()->addHours(24);
        $modules['footer1'] = array();
        $modules['footer2'] = array();
        $modules['footer3'] = array();
        $modules['footer4'] = array();
        if (Cache::has('footer1_' . $page . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $modules['footer1'] = Cache::get('footer1_' . $page . '_' . app()->getLocale());
        } else {
            $query1 = Module::where([
                ['page', '=', $page],
                ['position', '=', 'footer1'],
                ['status', '=', 1]
            ])
                ->select('module.*')
                ->orderBy('sort_order', 'ASC')
                ->get();
            foreach ($query1 as $item) {
                $modules['footer1'][] = \App::make('App\Http\Controllers\Client\Extension\Module\\' . ucfirst($item->code) . 'Controller')->index($item->setting, $item->id);
            }
            if (getConfig('config_debug') == 0) {
                Cache::put('footer1_' . $page . '_' . app()->getLocale(), $modules['footer1'], $expiresAt);
            }
        }
        if (Cache::has('footer2_' . $page . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $modules['footer2'] = Cache::get('footer2_' . $page . '_' . app()->getLocale());
        } else {
            $query2 = Module::where([
                ['page', '=', $page],
                ['position', '=', 'footer2'],
                ['status', '=', 1]
            ])
                ->select('module.*')
                ->orderBy('sort_order', 'ASC')
                ->get();
            foreach ($query2 as $item) {
                $modules['footer2'][] = \App::make('App\Http\Controllers\Client\Extension\Module\\' . ucfirst($item->code) . 'Controller')->index($item->setting, $item->id);
            }
            if (getConfig('config_debug') == 0) {
                Cache::put('footer2_' . $page . '_' . app()->getLocale(), $modules['footer2'], $expiresAt);
            }
        }

        if (Cache::has('footer3_' . $page . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $modules['footer3'] = Cache::get('footer3_' . $page . '_' . app()->getLocale());
        } else {
            $query3 = Module::where([
                ['page', '=', $page],
                ['position', '=', 'footer3'],
                ['status', '=', 1]
            ])
                ->select('module.*')
                ->orderBy('sort_order', 'ASC')
                ->get();
            foreach ($query3 as $item) {
                $modules['footer3'][] = \App::make('App\Http\Controllers\Client\Extension\Module\\' . ucfirst($item->code) . 'Controller')->index($item->setting, $item->id);
            }
            if (getConfig('config_debug') == 0) {
                Cache::put('footer3_' . $page . '_' . app()->getLocale(), $modules['footer3'], $expiresAt);
            }
        }

        if (Cache::has('footer4_' . $page . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $modules['footer4'] = Cache::get('footer4_' . $page . '_' . app()->getLocale());
        } else {
            $query4 = Module::where([
                ['page', '=', $page],
                ['position', '=', 'footer4'],
                ['status', '=', 1]
            ])
                ->select('module.*')
                ->orderBy('sort_order', 'ASC')
                ->get();
            foreach ($query4 as $item) {
                $modules['footer4'][] = \App::make('App\Http\Controllers\Client\Extension\Module\\' . ucfirst($item->code) . 'Controller')->index($item->setting, $item->id);
            }
            if (getConfig('config_debug') == 0) {
                Cache::put('footer4_' . $page . '_' . app()->getLocale(), $modules['footer4'], $expiresAt);
            }
        }
        $html = view('client.layouts.footer', compact('modules'))->render();
        return $html;
    }
}
