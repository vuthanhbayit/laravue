<?php

namespace App\Http\Controllers\Api;

use App\models\Cms\Language;
use App\models\Cms\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class SettingController extends Controller
{
    public function init()
    {
        $objLang = new Language();
        $data['languages'] = $objLang->get();

        $objSetting = new Setting();
        $data['configs'] = $objSetting->get();
        $langStatic = DB::table('language_static')->get();
        $dataLangStatic = [];
        foreach ($langStatic as $i => $item) {
            $dataLangStatic [$i]['languageKey'] = $item->languageKey;
            $dataLangStatic [$i]['languageDefaultValue'] = $item->languageDefaultValue;
        }
        $langStaticByLang = DB::table('language_static_by_lang')->get();
        $dataLangStaticByLang = [];
        foreach ($langStaticByLang as $k => $item) {
            $dataLangStaticByLang [$k]['languageStaticKey'] = $item->languageStaticKey;
            $dataLangStaticByLang [$k]['languageCode'] = $item->languageCode;
            $dataLangStaticByLang [$k]['languageValue'] = $item->languageValue;
        }
        $data['staticTexts'] = [];
        for ($i = 0; $i < count($dataLangStatic); $i++) {
            $vco = [];
            for ($k = 0; $k < count($dataLangStaticByLang); $k++) {
                if ($dataLangStaticByLang[$k]['languageStaticKey'] === $dataLangStatic[$i]['languageKey'] && $dataLangStaticByLang[$k]['languageCode'] !== '') {
                    $vco[$dataLangStaticByLang[$k]['languageCode']] = $dataLangStaticByLang[$k]['languageValue'];
                }
            }
            $data['staticTexts'][$i]['info'] = $dataLangStatic[$i];
            $data['staticTexts'][$i]['lang'] = (object)$vco;
        }
        return response()->json([
            'success' => true,
            "message" => "",
            'data' => $data,
        ]);
    }

    public function getSetting()
    {
        $data = array();
        $query = Setting::select()->get();
        foreach ($query as $item) {
            if (!$item['serialized']) {
                $data[$item['key']] = $item['value'];
            } else {
                $data[$item['key']] = unserialize($item['value']);
            }
        }
        return response()->json([
            'success' => true,
            "message" => "Get success",
            'data' => $data,
        ]);
    }

    public function saveSetting(Request $request)
    {
        Setting::truncate();
        foreach ($request->all() as $key => $value) {
            if (!is_array($value)) {
                Setting::create(['key' => $key, 'value' => $value]);
            } else {
                Setting::create(['key' => $key, 'value' => serialize($value), 'serialized' => 1]);
            }
        }
        return response()->json([
            'success' => true,
            "message" => "Save success",
        ]);
    }

    public function deleteCache()
    {
        $is_delete = File::deleteDirectory(storage_path('framework/cache'));
        if ($is_delete) {
            return response()->json([
                'success' => true,
                "message" => "Delete success",
            ]);
        } else {
            return response()->json([
                'success' => false,
                "message" => "Delete failure",
            ]);
        }

    }
}
