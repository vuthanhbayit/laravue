<?php

namespace App\Http\Controllers\Api;

use App\models\Cms\Place;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlaceController extends Controller
{
    public function searchPlace(Request $request, Place $place)
    {
        $data = $place->search($request);
        return $data;
    }

    public function detailPlace(Request $request, Place $place)
    {
        $data = $place->detail($request);
        return $data;
    }

    public function savePlace(Request $request, Place $place)
    {
        $data = $place->saveData($request);
        return $data;
    }

    public function deletePlace(Request $request, Place $place)
    {
        $data = $place->changeStatus($request);
        return $data;
    }
}
