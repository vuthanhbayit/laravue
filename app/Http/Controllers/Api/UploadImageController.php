<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

class UploadImageController extends Controller
{
    public function uploadImage(Request $request)
    {
        $param = $request->data;
        $folder = 'uploads';
        if (strpos($param, ';') !== false) {
            list($extension, $content) = explode(';', $param);
            $tmpExtension = explode('/', $extension);
            preg_match('/.([0-9]+) /', microtime(), $m);
            $fileName = sprintf('img%s%s.%s', date('YmdHis'), $m[1], $tmpExtension[1]);
            $content = explode(',', $param);
            $path = public_path() . '/' . $folder . '/' . date('Y-m-d');
            if (!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
            if (file_put_contents($path . '/' . $fileName, base64_decode($content[1]))) {

            } else {
                die;
            }
            return response()->json([
                'success' => true,
                'message' => "Upload success",
                'data' => $folder . '/' . date('Y-m-d') . '/' . $fileName,
            ]);
        } else {
            $uploadedFile = $request->file('file');
            if ($uploadedFile != null) {
                $filename = time() . '-' . $uploadedFile->getClientOriginalName();
                $path = public_path() . '/' . $folder . '/' . date('Y-m-d');
                if (!File::exists($path)) {
                    File::makeDirectory($path, 0777, true, true);
                }
                $request->file('file')->move($path, $filename);
                return response()->json([
                    'location' => $folder . '/' . date('Y-m-d') . '/' . $filename,
                ]);
            }
        }
    }

    public function deleteImage(Request $request)
    {
        $image_path = $request->image;
        if ($image_path === 'assets/no-img.png' || $image_path === 'assets/file-upload.jpg') {
            return response()->json([
                'success' => false,
                'message' => "Cannot delete photo",
            ]);
        } else {
            if (File::exists($image_path)) {
                File::delete($image_path);
                return response()->json([
                    'success' => true,
                    'message' => "Delete success",
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => "Image not found",
                ]);
            }
        }
    }
}
