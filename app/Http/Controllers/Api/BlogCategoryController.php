<?php

namespace App\Http\Controllers\Api;

use App\models\Cms\BlogCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogCategoryController extends Controller
{
    public function searchBlogCategory(Request $request, BlogCategory $blogCategory)
    {
        $data = $blogCategory->search($request);
        return $data;
    }

    public function detailBlogCategory(Request $request, BlogCategory $blogCategory)
    {
        $data = $blogCategory->detail($request);
        return $data;
    }

    public function saveBlogCategory(Request $request, BlogCategory $blogCategory)
    {
        $data = $blogCategory->saveData($request);
        return $data;
    }

    public function deleteBlogCategory(Request $request, BlogCategory $blogCategory)
    {
        $data = $blogCategory->changeStatus($request);
        return $data;
    }
}
