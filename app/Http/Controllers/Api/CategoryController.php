<?php

namespace App\Http\Controllers\Api;

use App\models\Cms\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class CategoryController extends Controller
{

    public function detailCategory(Request $request, Category $category)
    {
        $data = $category->detail($request);
        return $data;
    }

    public function saveCategory(Request $request, Category $category)
    {
        $data = $category->saveData($request);
        return $data;
    }

    public function searchCategory(Request $request, Category $category)
    {
        $data = $category->search($request);
        return $data;
    }

    public function deleteCategory(Request $request, Category $category)
    {
        $data = $category->changeStatus($request);
        return $data;
    }
}
