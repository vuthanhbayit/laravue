<?php

namespace App\Http\Controllers\Api;

use App\models\Cms\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function searchContact(Request $request, Contact $contact)
    {
        $data = $contact->search($request);
        return $data;
    }

    public function detailContact(Request $request, Contact $contact)
    {
        $data = $contact->detail($request);
        return $data;
    }

    public function readContact(Request $request, Contact $contact)
    {
        $data = $contact->readContact($request);
        return $data;
    }
}
