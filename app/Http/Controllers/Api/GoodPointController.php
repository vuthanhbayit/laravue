<?php

namespace App\Http\Controllers\Api;

use App\models\Cms\Category;
use App\models\Cms\GoodPoint;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class GoodPointController extends Controller
{
    public function detailGoodPoint(Request $request, GoodPoint $goodPoint)
    {
        $data = $goodPoint->detail($request);
        return $data;
    }

    public function saveGoodPoint(Request $request, GoodPoint $goodPoint)
    {
        $data = $goodPoint->saveData($request);
        return $data;
    }

    public function searchGoodPoint(Request $request, GoodPoint $goodPoint)
    {
        $data = $goodPoint->search($request);
        return $data;
    }

    public function deleteGoodPoint(Request $request, GoodPoint $goodPoint)
    {
        $data = $goodPoint->changeStatus($request);
        return $data;
    }
}
