<?php

namespace App\Http\Controllers\Api;

use App\models\Cms\Area;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AreaController extends Controller
{
    public function searchArea(Request $request, Area $area)
    {
        $data = $area->search($request);
        return $data;
    }

    public function detailArea(Request $request, Area $area)
    {
        $data = $area->detail($request);
        return $data;
    }

    public function saveArea(Request $request, Area $area)
    {
        $data = $area->saveData($request);
        return $data;
    }

    public function deleteArea(Request $request, Area $area)
    {
        $data = $area->changeStatus($request);
        return $data;
    }
}
