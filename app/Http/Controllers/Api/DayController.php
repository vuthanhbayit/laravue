<?php

namespace App\Http\Controllers\Api;

use App\models\Cms\Day;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DayController extends Controller
{
    public function searchDay(Request $request, Day $day)
    {
        $data = $day->search($request);
        return $data;
    }

    public function detailDay(Request $request, Day $day)
    {
        $data = $day->detail($request);
        return $data;
    }

    public function saveDay(Request $request, Day $day)
    {
        $data = $day->saveDay($request);
        return $data;
    }

    public function deleteDay(Request $request, Day $day)
    {
        $data = $day->changeStatus($request);
        return $data;
    }
}
