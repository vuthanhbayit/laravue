<?php

namespace App\Http\Controllers\Api\Extension\Module;

use App\models\Cms\Extension\Module;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ModuleController extends Controller
{
    public function detailModule(Request $request, Module $module)
    {
        $data = $module->detail($request);
        return $data;
    }
    public function saveModule(Request $request, Module $module)
    {
        $data = $module->saveData($request);
        return $data;
    }
    public function deleteModule(Request $request, Module $module)
    {
        $data = $module->changeStatus($request);
        return $data;
    }
}
