<?php

namespace App\Http\Controllers\Api\Extension;

use App\models\Cms\Extension\Extension;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExtensionController extends Controller
{
    public function searchExtension(Request $request, Extension $extension)
    {
        $data = $extension->searchExtension($request);
        return $data;
    }
    public function deleteExtension(Request $request, Extension $extension)
    {
        $data = $extension->changeStatus($request);
        return $data;
    }
}
