<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth, DB;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use App\Role;
use App\Permission;
use App\models\Cms\RoleUser;
use App\models\Cms\AuthenticationLog;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    public function login(Request $request)
    {
        $arr = request(['name', 'password']);
        if (!Auth::attempt($arr)) {
            return response()->json([
                'message' => 'đăng nhập thất bại'
            ], 401);
        } else {
            $user = $request->user();
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            if ($request->remember_me) {
                $token->expires_at = Carbon::now()->addWeeks(1);
            }
            $token->save();
            return response()->json([
                'success' => true,
                'message' => '',
                'data' => [
                    'access_token' => $tokenResult->accessToken,
                    'token_type' => 'Bearer',
                    'user' => $user->toArray(),
                    'permission_role' => $this->getPermission($user->id),
                    'expires_at' => Carbon::parse(
                        $tokenResult->token->expires_at
                    )->toDateTimeString()
                ]
            ]);
        }
    }

    public function logout(Request $request)
    {
        $accessToken = Auth::user()->token();
        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);

        $accessToken->revoke();
        return response()->json([
            'message' => 'Logout Success'
        ], 200);
    }

    public function listUser(User $user)
    {
        $data = $user->getListUser();
        return response()->json([
            'success' => true,
            'message' => '',
            'data' => $data
        ], 200);
    }

    public function deleteUser(Request $request)
    {
        $id = $request->id;
        $user_id = Auth::user()->id;
        if ($id == $user_id) {
            return response()->json([
                'success' => false,
                'message' => 'Cannot delete user. Here are you!',
            ], 200);
        } else {
            User::find($id)->delete();
            DB::table('role_user')->where('user_id', $id)->delete();
            return response()->json([
                'success' => true,
                'message' => 'Delete user success',
            ], 200);
        }
    }

    public function detailUser(Request $request, User $user)
    {
        $id = $request->id;
        $data = $user->getDetailUser($id);
        return response()->json([
            'success' => true,
            'message' => 'Get user success',
            'data' => $data
        ], 200);
    }

    public function saveUser(Request $request, User $users)
    {
        $id = $request->id;
        if ($id > 0) {
            if ($request->password != null) {
                $user = User::find($id);
                $user->email = $request->email;
                $user->password = bcrypt($request->password);
                $user->save();
                DB::table('role_user')->where('user_id', $id)->delete();
                $user->attachRole(Role::where('name', $request->role)->first());
                return response()->json([
                    'success' => true,
                    'message' => 'Get user success',
                    'data' => $user
                ], 200);
            } else {
                $user = User::find($id);
                $user->email = $request->email;
                $user->save();
                DB::table('role_user')->where('user_id', $id)->delete();
                $user->attachRole(Role::where('name', $request->role)->first());
                return response()->json([
                    'success' => true,
                    'message' => 'Get user success',
                    'data' => $user
                ], 200);
            }
        } else {
            $request->validate($users->rule());
            $user = new User();
            $isUserName = User::where('name', $request->name)->first();
            if ($isUserName) {
                return response()->json([
                    'success' => false,
                    'message' => 'Username is exists',
                ], 200);
            }
            $isUserEmail = User::where('email', $request->email)->first();
            if ($isUserEmail) {
                return response()->json([
                    'success' => false,
                    'message' => 'Email is exists',
                ], 200);
            }
            $user->name = trim($request->name);
            $user->email = trim($request->email);
            $user->password = bcrypt($request->password);
            $user->save();
            $user->attachRole(Role::where('name', $request->role)->first());
            return response()->json([
                'success' => true,
                'message' => 'Get user success',
                'data' => $user
            ], 200);
        }
    }

    public function history(Request $request, AuthenticationLog $auth)
    {
        $data = $auth->getHistory($request);
        return response()->json([
            'data' => $data
        ], 200);
    }

    public function changePass(Request $request)
    {
        if (Auth::Check()) {
            $validator = $this->adminCredentialRules($request->all());
            if ($validator->fails()) {
                return response()->json([
                    'data' => $validator->getMessageBag()->toArray(),
                    'success' => false,
                    'message' => 'Error'
                ], 400);
            } else {
                $currentPassword = Auth::User()->password;
                if (Hash::check($request['currentPassword'], $currentPassword)) {
                    $user_id = Auth::User()->id;
                    $obj_user = User::find($user_id);
                    $obj_user->password = Hash::make($request['password']);
                    $obj_user->save();
                    return response()->json([
                        'success' => true,
                        'message' => 'Change pass success'
                    ], 200);
                } else {
                    return response()->json([
                        'success' => false,
                        'message' => 'Please enter correct current password'
                    ], 400);
                }
            }
        } else {
            return response()->json([
                'success' => false,
                'message' => 'You must login Cms'
            ], 400);
        }
    }

    public function getPermission($user_id)
    {
        $roleUser = new RoleUser();
        $data = $roleUser->getListpermission($user_id);
        $permission_arr = [];
        if ($data) {
            foreach ($data as $value) {
                $permission_arr[] = $value->id;
            }
        }
        return $permission_arr;
    }

    public function adminCredentialRules($request)
    {
        $messages = [
            'currentPassword.required' => 'Please enter current password',
            'password.required' => 'Please enter password',
        ];

        $validator = Validator::make($request, [
            'currentPassword' => 'required',
            'password' => 'required|same:password',
            'passwordConfirmation' => 'required|same:password',
        ], $messages);

        return $validator;
    }
}
