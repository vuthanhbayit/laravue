<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\User;
use App\models\Cms\PassswordReset;
use Carbon\Carbon;

class PasswordResetController extends Controller
{
    public function create(Request $request)
    {
    	$user = User::where('email',$request->email)->first();
    	if (!$user) {
    		return response()->json([
    			'message'=>'email không chính xác'
    		],404);
    	}else{
    		$passwordReset = PassswordReset::updateOrCreate(
    			['email'=>$user->email],
    			[
    				'email' => $user->email,
    				'token' => str_random(60)
    			]
    		);
    	}
    	if ($user && $passwordReset) {
    		$user->notify(
                new PasswordResetRequest($passwordReset->token)
            );
            return response()->json([
	            'message' => 'Vui lòng check mail của bạn'
	        ],200);
    	}
    }
    public function find($token)
    {
    	$passwordReset = PassswordReset::where('token', $token)
            ->first();
        if (!$passwordReset)
            return response()->json([
                'message' => "token không chính xác"
            ], 404);
        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        }
        return response()->json([
        	'message' => 'success',
        	'data' => $passwordReset
        ],200);
    }
    public function reset(Request $request)
    {
    	$request->validate([
            'email' => 'required|string|email',
            'password' => 'required',
            'token' => 'required'
        ]);
        $passwordReset = PassswordReset::where([
            ['token', $request->token],
            ['email', $request->email]
        ])->first();
        if (!$passwordReset){
        	return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        }else{
        	$user = User::where('email', $passwordReset->email)->first();
        	if (!$user) {
        		return response()->json([
	                'message' => 'Không tìm thấy email yêu cầu'
	            ], 404);
        	}else{
        		$user->password = bcrypt($request->password);
        		$user->save();
        		$passwordReset->delete();
        		return response()->json([
        			'message'=>'success',
        			'data'=>$user
        		],200);
        	}
        }
        
            
    }
}
