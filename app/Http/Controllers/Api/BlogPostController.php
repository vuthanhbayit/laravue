<?php

namespace App\Http\Controllers\Api;

use App\models\Cms\BlogPost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogPostController extends Controller
{
    public function searchBlogPost(Request $request, BlogPost $blogPost)
    {
        $data = $blogPost->search($request);
        return $data;
    }

    public function detailBlogPost(Request $request, BlogPost $blogPost)
    {
        $data = $blogPost->detail($request);
        return $data;
    }

    public function saveBlogPost(Request $request, BlogPost $blogPost)
    {
        $data = $blogPost->saveData($request);
        return $data;
    }

    public function deleteBlogPost(Request $request, BlogPost $blogPost)
    {
        $data = $blogPost->changeStatus($request);
        return $data;
    }
}
