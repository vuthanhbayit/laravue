<?php

namespace App\Http\Controllers\Api;

use App\models\Cms\Category;
use App\models\Cms\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function searchProduct(Request $request, Product $product){
        $data = $product->search($request);
        return $data;
    }
    public function detailProduct(Request $request, Product $product, Category $category){
        $product_id = $request->product['id'];
        $data['product'] = $product->detail($request);
        $data['descriptions'] = $product->getProductDescriptions($product_id);
        $data['product_att'] = $product->getProductAttribute($product_id);
        $data['days'] = $product->getProductDay($product_id);
        $data['product_image'] = $product->getProductImage($product_id);
        $data['product_related'] = $product->getProductRelated($product_id);
       // $data['product_category'] = $product->getProductCategory($product_id);
        $categories = $product->getProductCategory($product_id);
        $data['product_category'] = array();
        foreach ($categories as $category_id) {
            $category_info = $category->getCategory($category_id);
            if ($category_info) {
                $data['product_category'][] = array(
                    'id'        => $category_info->id,
                    'title'     => ($category_info->path) ? $category_info->path . ' > ' . $category_info->title : $category_info->title
                );
            }
        }
        return response()->json([
            'success' => true,
            'message' => "Search success",
            'data' => $data
        ]);
    }
    public function saveProduct(Request $request, Product $product){
        $data = $product->saveData($request);
        return $data;
    }
    public function deleteProduct(Request $request, Product $product){
        $data = $product->changeStatus($request);
        return $data;
    }
}
