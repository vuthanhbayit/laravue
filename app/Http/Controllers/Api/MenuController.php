<?php

namespace App\Http\Controllers\Api;

use App\models\Cms\Menu;
use App\models\Cms\MenuGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MenuController extends Controller
{
    public function searchMenuGroup(Request $request, MenuGroup $menuGroup)
    {
        $data = $menuGroup->search($request);
        return $data;
    }
    public function detailMenuGroup(Request $request, MenuGroup $menuGroup)
    {
        $data = $menuGroup->detail($request);
        return $data;
    }

    public function saveMenuGroup(Request $request, MenuGroup $menuGroup)
    {
        $data = $menuGroup->saveData($request);
        return $data;
    }

    public function deleteMenuGroup(Request $request, MenuGroup $menuGroup){
        $data = $menuGroup->changeStatus($request);
        return $data;
    }
    public function getTreeMenu(Request $request, Menu $menu)
    {
        $data = $menu->getTreeMenu(null, $request);
        return $data;
    }

    public function detailMenu(Request $request, Menu $menu)
    {
        $data = $menu->detailMenu($request);
        return $data;
    }

    public function saveMenu(Request $request, Menu $menu)
    {
        $data = $menu->saveMenu($request);
        return $data;
    }

    public function saveOrderMenu(Request $request,  Menu $menu)
    {
        $data = $menu->saveOrderMenu($request->all());
        return  $data;
    }

    public function importCategory(Request $request, Menu $menu)
    {
        $menu->importCategory($request);
        $data = $menu->getTreeMenu(null, $request);
        return $data;
    }

    public function deleteCategory(Request $request, Menu $menu)
    {
        $data = $menu->deleteCategory($request);
        return $data;
    }

    public function deleteMenu(Request $request, Menu $menu)
    {
        $data = $menu->del($request);
        return $data;
    }

}
