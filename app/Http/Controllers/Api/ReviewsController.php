<?php

namespace App\Http\Controllers\Api;

use App\models\Cms\Reviews;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReviewsController extends Controller
{
    public function searchReviews(Request $request, Reviews $reviews)
    {
        $data = $reviews->search($request);
        return $data;
    }

    public function detailReviews(Request $request, Reviews $reviews)
    {
        $data = $reviews->detail($request);
        return $data;
    }

    public function saveReviews(Request $request, Reviews $reviews)
    {
        $data = $reviews->saveData($request);
        return $data;
    }

    public function deleteReviews(Request $request, Reviews $reviews)
    {
        $data = $reviews->changeStatus($request);
        return $data;
    }
}
