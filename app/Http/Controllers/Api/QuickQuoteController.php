<?php

namespace App\Http\Controllers\Api;

use App\models\Cms\QuickQuote;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuickQuoteController extends Controller
{
    public function searchQuickQuote(Request $request, QuickQuote $quickQuote)
    {
        $data = $quickQuote->search($request);
        return $data;
    }

    public function detailQuickQuote(Request $request, QuickQuote $quickQuote)
    {
        $data = $quickQuote->detail($request);
        return $data;
    }

    public function deleteQuickQuote(Request $request, QuickQuote $quickQuote)
    {
        $data = $quickQuote->changeStatus($request);
        return $data;
    }
}
