<?php

namespace App\Http\Controllers\Api;

use App\models\Cms\Banner;
use App\models\Cms\BannerGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BannerController extends Controller
{
    public function searchBanner(Request $request, Banner $banner)
    {
        $data = $banner->search($request);
        return $data;
    }

    public function detailBanner(Request $request, Banner $banner)
    {
        $data = $banner->detail($request);
        return $data;
    }
    public function getBannerByGroupId(Request $request, Banner $banner)
    {
        $data = $banner->getBannerByGroupId($request);
        return $data;
    }
    public function saveBanner(Request $request, Banner $banner)
    {
        $data = $banner->saveData($request);
        return $data;
    }

    public function deleteBanner(Request $request, Banner $banner)
    {
        $data = $banner->del($request);
        return $data;
    }

    public function searchBannerGroup(Request $request, BannerGroup $bannerGroup)
    {
        $data = $bannerGroup->search($request);
        return $data;
    }

    public function detailBannerGroup(Request $request, BannerGroup $bannerGroup)
    {
        $data = $bannerGroup->detail($request);
        return $data;
    }

    public function saveBannerGroup(Request $request, BannerGroup $bannerGroup)
    {
        $data = $bannerGroup->saveData($request);
        return $data;
    }

    public function deleteBannerGroup(Request $request, BannerGroup $bannerGroup)
    {
        $data = $bannerGroup->changeStatus($request);
        return $data;
    }
}
