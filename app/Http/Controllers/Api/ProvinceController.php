<?php

namespace App\Http\Controllers\Api;

use App\models\Cms\Province;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProvinceController extends Controller
{
    public function searchProvince(Request $request, Province $province)
    {
        $data = $province->search($request);
        return $data;
    }

    public function detailProvince(Request $request, Province $province)
    {
        $data = $province->detail($request);
        return $data;
    }

    public function saveProvince(Request $request, Province $province)
    {
        $data = $province->saveData($request);
        return $data;
    }

    public function deleteProvince(Request $request, Province $province)
    {
        $data = $province->changeStatus($request);
        return $data;
    }
}
