<?php

namespace App\Http\Controllers\Api;

use App\models\Cms\RequestCms;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RequestCmsController extends Controller
{
    public function searchRequest(Request $request, RequestCms $requestcms)
    {
        $data = $requestcms->search($request);
        return $data;
    }

    public function detailRequest(Request $request, RequestCms $requestcms)
    {
        $data = $requestcms->detail($request);
        return $data;
    }

    public function saveRequest(Request $request, RequestCms $requestcms)
    {
        $data = $requestcms->saveData($request);
        return $data;
    }

    public function deleteRequest(Request $request, RequestCms $requestcms)
    {
        $data = $requestcms->changeStatus($request);
        return $data;
    }
}
