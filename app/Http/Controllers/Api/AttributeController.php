<?php

namespace App\Http\Controllers\Api;

use App\models\Cms\Attribute;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AttributeController extends Controller
{
    public function searchAttribute(Request $request, Attribute $attribute)
    {
        $data = $attribute->search($request);
        return $data;
    }

    public function detailAttribute(Request $request, Attribute $attribute)
    {
        $data = $attribute->detail($request);
        return $data;
    }

    public function saveAttribute(Request $request, Attribute $attribute)
    {
        $data = $attribute->saveAttribute($request);
        return $data;
    }

    public function deleteAttribute(Request $request, Attribute $attribute)
    {
        $data = $attribute->changeStatus($request);
        return $data;
    }
}
