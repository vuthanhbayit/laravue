<?php

namespace App\Http\Controllers\Api;

use App\models\Cms\Information;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InformationController extends Controller
{
    public function searchInformation(Request $request, Information $information)
    {
        $data = $information->search($request);
        return $data;
    }

    public function detailInformation(Request $request, Information $information)
    {
        $data = $information->detail($request);
        return $data;
    }

    public function saveInformation(Request $request, Information $information)
    {
        $data = $information->saveData($request);
        return $data;
    }

    public function deleteInformation(Request $request, Information $information)
    {
        $data = $information->changeStatus($request);
        return $data;
    }
}
