<?php

namespace App\Imports;

use App\models\WorkData;
use Maatwebsite\Excel\Concerns\ToModel;

class WorkDataImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    protected  $company_id;
    protected  $day_import;
    public function  __construct($company_id, $day_import)
    {
        $this->company_id = $company_id;
        $this->day_import = $day_import;
    }
    public function model(array $row)
    {
        var_dump($row[1]);
        $data = new WorkData();
        $data->employee_number = $row[0];
        $data->starthour = $row[1];
        $data->startminute = $row[2];
        $data->endhour = $row[3];
        $data->endminutes = $row[4];
        $data->day_import = $this->day_import;
        $data->company_id = $this->company_id;
        $data->save();
    }
}
