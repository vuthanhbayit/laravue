<?php

namespace App;

use Zizaco\Entrust\EntrustRole;
use App\models\Cms\RolePermission;

class Role extends EntrustRole
{
    protected $fillable = ['name', 'display_name', 'description'];

    public function RolePermission()
    {
        return $this->hasMany(RolePermission::class, 'role_id', 'id');
    }

    public function getDataRole()
    {
        $query = $this
            ->with(['RolePermission.Permission'])
            ->get();
        return $query;
    }
    public function addRole($request)
    {
        $query = Role::create(handle($request));
        return $query;
    }
    public function editRole($request)
    {
        $query = Role::where('id',$request->id)->update(handle($request));
        return $query;
    }
}
