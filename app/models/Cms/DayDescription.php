<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DayDescription extends Model
{
    protected $table = 'day_description';

    protected $primaryKey = 'day_id';

    protected $fillable = [
        'day_id', 'language_id', 'title'
    ];
    protected $guarded = [
        'day_id', 'language_id', 'title'
    ];
}
