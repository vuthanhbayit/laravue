<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Information extends Model
{
    protected $table = 'information';

    protected $primaryKey = 'id';

    protected $fillable = [
        'banner_group_id', 'title', 'description', 'content', 'meta_title', 'meta_description', 'status'
    ];
    protected $guarded = [
        'banner_group_id', 'title', 'description', 'content', 'meta_title', 'meta_description', 'status'
    ];

    public $timestamps = true;

    public function search($request)
    {
        $query = Information::where(function ($query) use ($request) {
            if (isset($request->title) && $request->title != '') {
                $query->where('information_description.title', 'like', '%' . $request->title . '%');
            }
            if (isset($request->status) && $request->status != -1) {
                $query->where('information.status', '=', $request->status);
            }
            if (isset($request->languageCode) && $request->languageCode != '') {
                $query->where('information_description.language_id', '=', $request->languageCode);
            } else {
                $query->where('information_description.language_id', '=', getConfig('config_language_admin'));
            }
        })->join('information_description', 'information_description.information_id', '=', 'information.id')
            ->select('information.id AS id', 'information_description.title AS title', 'information_description.description AS description', 'information.status AS status')
            ->orderBy('id', 'DESC');

        $totalRow = $query->get();

        if (isset($request->pageIndex) && $request->pageIndex > 0) {
            if ($request->pageIndex == 1) {
                $query->offset(0);
            } else {
                $query->offset(($request->pageIndex - 1) * $request->pageSize);
            }
        }

        if ($request->pageSize == null) {
            $data = $query->get();
        } else {
            $data = $query->limit($request->pageSize)->get();
        }

        return response()->json([
            'success' => true,
            'message' => "Search success",
            'data' => $data,
            'totalRow' => $totalRow->count(),
        ]);
    }

    public function getInformationDescriptions($informationId)
    {
        $description = array();
        $query = InformationDescription::where('information_id', $informationId)->get();
        foreach ($query as $i => $item) {
            $description[$i] = array(
                'languageCode' => $item['language_id'],
                'title' => $item['title'],
                'description' => $item['description'],
                'content' => $item['content'],
                'meta_title' => $item['meta_title'],
                'meta_description' => $item['meta_description'],
            );
        }
        return $description;
    }

    public function detail($request)
    {
        $data = Information::where('id', $request->id)->first();
        $data['descriptions'] = $this->getInformationDescriptions($request->id);
        return response()->json([
            'success' => true,
            'message' => "Get success",
            'data' => $data,
        ]);
    }

    public function saveData($request)
    {
        if ($request->id > 0) {
            Information::where('id', $request->id)->update(['banner_group_id' => $request->banner_group_id, 'status' => $request->status]);
            InformationDescription::where('information_id', $request->id)->delete();
            foreach ($request->descriptions as $item) {
                if (trim($item['title']) != '') {
                    InformationDescription::create([
                        'information_id' => $request->id, 'language_id' => $item['languageCode'], 'title' => $item['title'], 'description' => $item['description'], 'content' => $item['content'],
                        'meta_title' => $item['meta_title'], 'meta_description' => $item['meta_description']
                    ]);
                }
            }
            return response()->json([
                'success' => true,
                'message' => "Update success",
            ]);
        } else {
            $data = Information::create(['banner_group_id' => $request->banner_group_id, 'status' => $request->status]);
            $informationId = $data->id;
            foreach ($request->descriptions as $item) {
                if (trim($item['title']) != '') {
                    InformationDescription::create([
                        'information_id' => $informationId, 'language_id' => $item['languageCode'], 'title' => $item['title'], 'description' => $item['description'], 'content' => $item['content'],
                        'meta_title' => $item['meta_title'], 'meta_description' => $item['meta_description']
                    ]);
                }
            }
            return response()->json([
                'success' => true,
                'message' => "Add success",
            ]);
        }
    }

    public function changeStatus($request)
    {
        $updateInformation = Information::where('id', $request->id)->first();
        $updateInformation->status = $request->status;
        $updateInformation->save();
        return response()->json([
            'success' => true,
            'message' => "Change status success",
        ]);
    }
}
