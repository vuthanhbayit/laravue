<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;

class BannerGroup extends Model
{
    protected $table = 'banner_group';

    protected $primaryKey = 'id';

    protected $fillable = [
        'title', 'page', 'type', 'status'
    ];
    protected $guarded = [
        'title', 'page', 'type', 'status'
    ];

    public function search($request)
    {
        $query = BannerGroup::where(function ($query) use ($request) {
            if (isset($request->title) && $request->title != '') {
                $query->where('title', 'like', '%' . $request->title . '%');
            }
            if (isset($request->status) && $request->status != -1) {
                $query->where('status', '=', $request->status);
            }
        })
            ->orderBy('id', 'DESC');
        if (isset($request->pageIndex) && $request->pageIndex > 0) {
            if ($request->pageIndex == 1) {
                $query->offset(0);
            } else {
                $query->offset(($request->pageIndex - 1) * $request->pageSize);
            }
        }
        if ($request->pageSize == null) {
            $data = $query->get();
        } else {
            $data = $query->limit($request->pageSize)->get();
        }

        $totalRow = BannerGroup::where(function ($query) use ($request) {
            if (isset($request->title) && $request->title != '') {
                return $query->where('title', 'like', '%' . $request->title . '%');
            }
            if (isset($request->status) && $request->status != -1) {
                return $query->where('status', '=', $request->status);
            }
        })
            ->orderBy('id', 'DESC')
            ->get();

        return response()->json([
            'success' => true,
            'message' => "Search success",
            'data' => $data,
            'totalRow' => $totalRow->count(),
        ]);
    }

    public function detail($request)
    {
        $arrPage = array();
        $data = BannerGroup::where('id', $request->id)->first();
        if (strpos($data->page, ',') !== false) {
            $array = explode(',', $data->page);
            foreach ($array as $k => $item) {
                $arrPage[$k] = ['page' => $item, 'name' => ucfirst($item)];
            }
            $data['page'] = $arrPage;
        } else {
            $data['page'] = array('page' => $data->page, 'name' => ucfirst($data->page));
        }
        return response()->json([
            'success' => true,
            'message' => "Get success",
            'data' => $data,
        ]);
    }

    public function saveData($request)
    {
        if ($request->id > 0) {
            $page = '';
            if (isset($request->page) && is_array($request->page)) {
                foreach ($request->page as $k => $item) {
                    if ($k == count($request->page) - 1) {
                        $page .= $item['page'];
                    } else {
                        $page .= $item['page'] . ',';
                    }
                }
            }
            BannerGroup::where('id', $request->id)->update(['title' => $request->title, 'page' => $page, 'type' => $request->type,
                'status' => $request->status]);
            return response()->json([
                'success' => true,
                'message' => "Update success",
            ]);
        } else {
            $page = '';
            if (isset($request->page) && is_array($request->page)) {
                foreach ($request->page as $k => $item) {
                    if ($k == count($request->page) - 1) {
                        $page .= $item['page'];
                    } else {
                        $page .= $item['page'] . ',';
                    }
                }
            }
            BannerGroup::create(['title' => $request->title, 'page' => $page, 'type' => $request->type,
                'status' => $request->status]);
            return response()->json([
                'success' => true,
                'message' => "Add success",
            ]);
        }
    }

    public function changeStatus($request)
    {
        $updateBanner = BannerGroup::where('id', $request->id)->first();
        $updateBanner->status = $request->status;
        $updateBanner->save();
        return response()->json([
            'success' => true,
            'message' => "Change status success",
        ]);
    }
}
