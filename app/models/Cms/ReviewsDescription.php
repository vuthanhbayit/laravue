<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ReviewsDescription extends Model
{
    protected $table = 'reviews_description';

    protected $primaryKey = 'reviews_id';

    protected $fillable = [
        'reviews_id', 'language_id', 'title', 'description', 'content', 'meta_title', 'meta_description'
    ];
    protected $guarded = [
        'reviews_id', 'language_id', 'title', 'description', 'content', 'meta_title', 'meta_description'
    ];
}
