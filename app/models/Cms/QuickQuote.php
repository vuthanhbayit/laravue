<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class QuickQuote extends Model
{

    protected $table = 'quick_quotes';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'expected_departure_date', 'expected_number_of_days', 'departure_from_id', 'number_of_adults', 'number_of_children', 'number_of_babies', 'criterias', 'contact_id', 'special_requirements', 'status'
    ];
    protected $guarded = [
        'id', 'expected_departure_date', 'expected_number_of_days', 'departure_from_id', 'number_of_adults', 'number_of_children', 'number_of_babies', 'criterias', 'contact_id', 'special_requirements', 'status'
    ];

    public $timestamps = true;

    public function search($request)
    {
        $query = QuickQuote::where(function ($query) use ($request) {
            if (isset($request->status) && $request->status != -1) {
                $query->where('status', '=', $request->status);
            }
            if (isset($request->keyword) && $request->keyword != '') {
                $query->where('contact.fullName', 'like', '%' . $request->keyword . '%')
                    ->orWhere('contact.phone', 'like', '%' . $request->keyword . '%');
            }
        })
            ->join('contact', 'quick_quotes.contact_id', '=', 'contact.id')
            ->select(
                'quick_quotes.id AS id',
                'quick_quotes.expected_departure_date AS expected_departure_date',
                'quick_quotes.expected_number_of_days AS expected_number_of_days',
                'quick_quotes.departure_from_id AS departure_from_id',
                'quick_quotes.number_of_adults AS number_of_adults',
                'quick_quotes.number_of_children AS number_of_children',
                'quick_quotes.number_of_babies AS number_of_babies',
                'quick_quotes.criterias AS criterias',
                'quick_quotes.contact_id AS contact_id',
                'contact.fullName AS contact_fullName',
                'contact.email AS contact_email',
                'contact.phone AS contact_phone',
                'contact.address AS contact_address',
                'contact.node AS contact_node',
                'quick_quotes.special_requirements AS special_requirements',
                'quick_quotes.status AS status'
            )
            ->orderBy('id', 'DESC');
        $totalRow = $query->get();

        if (isset($request->pageIndex) && $request->pageIndex > 0) {
            if ($request->pageIndex == 1) {
                $query->offset(0);
            } else {
                $query->offset(($request->pageIndex - 1) * $request->pageSize);
            }
        }
        if ($request->pageSize == null) {
            $data = $query->get();
        } else {
            $data = $query->limit($request->pageSize)->get();
        }

        return response()->json([
            'success' => true,
            'message' => "Search success",
            'data' => $data,
            'totalRow' => $totalRow->count(),
        ]);
    }

    public function detail($request)
    {
        $data = DB::table('quick_quotes')
            ->where('quick_quotes.id', $request->id)
            ->join('contact', 'contact.id', '=', 'quick_quotes.contact_id')
            ->select(
                'quick_quotes.id AS id',
                'quick_quotes.expected_departure_date AS expected_departure_date',
                'quick_quotes.expected_number_of_days AS expected_number_of_days',
                'quick_quotes.departure_from_id AS departure_from_id',
                'quick_quotes.number_of_adults AS number_of_adults',
                'quick_quotes.number_of_children AS number_of_children',
                'quick_quotes.number_of_babies AS number_of_babies',
                'quick_quotes.criterias AS criterias',
                'quick_quotes.contact_id AS contact_id',
                'contact.fullName AS contact_fullName',
                'contact.email AS contact_email',
                'contact.phone AS contact_phone',
                'contact.address AS contact_address',
                'contact.node AS contact_node',
                'quick_quotes.special_requirements AS special_requirements',
                'quick_quotes.status AS status'
            )
            ->orderBy('id', 'DESC')
            ->first();
        $criterias = json_decode($data->criterias);
        $data->provinces = array();
        $data->places = array();
        $data->requests = array();
        if (isset($criterias->provinces)) {
            foreach ($criterias->provinces as $i => $v) {
                $item = DB::table('province')
                    ->where([
                        ['province.id', $v],
                        ['province_description.province_id', $v],
                        ['province_description.language_id', app()->getLocale()]
                    ])
                    ->join('province_description', 'province_description.province_id', '=', 'province.id')
                    ->select('province.id AS id', 'province_description.title AS title', 'province.status AS status')
                    ->first();
                array_push($data->provinces, $item);
            }
        }
        if (isset($criterias->places)) {
            foreach ($criterias->places as $i => $v) {
                $item = DB::table('place')
                    ->where([
                        ['place.id', $v],
                        ['place_description.place_id', $v],
                        ['place_description.language_id', app()->getLocale()]
                    ])
                    ->join('place_description', 'place_description.place_id', '=', 'place.id')
                    ->select('place.id AS id', 'place_description.title AS title', 'place.status AS status')
                    ->first();
                array_push($data->places, $item);
            }
        }
        if (isset($criterias->requests)) {
            foreach ($criterias->requests as $i => $v) {
                $item = DB::table('request')
                    ->where([
                        ['request.id', $v],
                        ['request_description.request_id', $v],
                        ['request_description.language_id', app()->getLocale()]
                    ])
                    ->join('request_description', 'request_description.request_id', '=', 'request.id')
                    ->select('request.id AS id', 'request_description.title AS title', 'request.status AS status')
                    ->first();
                array_push($data->requests, $item);
            }
        }
        return response()->json([
            'success' => true,
            'message' => "Get detail success",
            'data' => $data,
        ]);
    }

    public function changeStatus($request)
    {
        $updateQuickQuote = QuickQuote::where('id', $request->id)->first();
        $updateQuickQuote->status = $request->status;
        $updateQuickQuote->save();
        return response()->json([
            'success' => true,
            'message' => "Change status success",
        ]);
    }

}
