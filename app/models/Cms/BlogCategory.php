<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;
use DB;

class BlogCategory extends Model
{
    protected $table = 'blog_category';

    protected $primaryKey = 'id';

    protected $fillable = [
        'parent_id', 'image', 'icon', 'banner_group_id', 'status'
    ];
    protected $guarded = [
        'parent_id', 'image', 'icon', 'banner_group_id', 'status'
    ];

    public function search($request)
    {
        $query = DB::table('blog_category_path AS cp')->select('c1.parent_id', 'cp.category_id AS id', 'c1.image', 'c1.status')
            ->selectRaw("GROUP_CONCAT(cd1.title ORDER BY cp.level SEPARATOR ' > ') AS title")
            ->leftJoin('blog_category AS c1', 'cp.category_id', '=', 'c1.id')
            ->leftJoin('blog_category AS c2', 'cp.path_id', '=', 'c2.id')
            ->leftJoin('blog_category_description AS cd1', 'cp.path_id', '=', 'cd1.category_id')
            ->leftJoin('blog_category_description AS cd2', 'cp.category_id', '=', 'cd2.category_id')
            ->groupBy('cp.category_id')
            ->where(function ($query) use ($request) {
                if (isset($request->title) && $request->title != '') {
                    $query->where('cd2.title', 'like', '%' . $request->title . '%');
                }
                if (isset($request->status) && $request->status != -1) {
                    $query->where('c1.status', '=', $request->status);
                }
                if (isset($request->languageCode) && $request->languageCode != '') {
                    $query->where([
                        ['cd1.language_id', '=', $request->languageCode],
                        ['cd2.language_id', '=', $request->languageCode]
                    ]);
                } else {
                    $query->where([
                        ['cd1.language_id', '=', getConfig('config_language_admin')],
                        ['cd2.language_id', '=', getConfig('config_language_admin')]
                    ]);
                }
            })
            ->orderBy('title', 'ASC');

        $totalRow = $query->get();

        if (isset($request->pageIndex) && $request->pageIndex > 0) {
            if ($request->pageIndex == 1) {
                $query->offset(0);
            } else {
                $query->offset(($request->pageIndex - 1) * $request->pageSize);
            }
        }
        if ($request->pageSize == null) {
            $data = $query->get();
        } else {
            $data = $query->limit($request->pageSize)->get();
        }
        foreach ($data as $k => $v) {
            $filename = $v->image;
            if ($filename) {
                resizeImage($filename, 'cache/' . $filename);
                $v->image = 'cache/' . $filename;
            }
        }
        return response()->json([
            'success' => true,
            "message" => "Search success",
            'data' => $data,
            'totalRow' => $totalRow->count(),
        ]);
    }

    public function detail($request)
    {
        $data = BlogCategory::where('id', $request->id)->first();
        $data['descriptions'] = $this->getBlogCategoryDescriptions($request->id);
        return response()->json([
            'success' => true,
            "message" => "Get success",
            'data' => $data
        ]);
    }

    public function getBlogCategoryDescriptions($categoryId)
    {
        $description = array();
        $query = BlogCategoryDescription::where('category_id', $categoryId)->get();
        foreach ($query as $i => $item) {
            $description[$i] = array(
                'languageCode' => $item['language_id'],
                'title' => $item['title'],
                'description' => $item['description'],
                'meta_title' => $item['meta_title'],
                'meta_description' => $item['meta_description'],
            );
        }
        return $description;
    }

    public function saveData($request)
    {
        $blog_category_id = $request->id;
        if ($blog_category_id > 0) {
            $parent_id = $request->parent_id;
            if ($parent_id == $blog_category_id) {
                BlogCategory::where('id', $blog_category_id)->update(['banner_group_id' => $request->banner_group_id, 'icon' => $request->icon, 'image' => $request->image,
                    'status' => $request->status]);
            } else {
                BlogCategory::where('id', $blog_category_id)->update(['banner_group_id' => $request->banner_group_id, 'icon' => $request->icon, 'image' => $request->image,
                    'parent_id' => $parent_id, 'status' => $request->status]);
            }
            BlogCategoryDescription::where('category_id', $blog_category_id)->delete();
            foreach ($request->descriptions as $item) {
                if (trim($item['title']) != '') {
                    BlogCategoryDescription::create([
                        'category_id' => $blog_category_id, 'language_id' => $item['languageCode'], 'title' => $item['title'], 'description' => $item['description'],
                        'meta_title' => $item['meta_title'], 'meta_description' => $item['meta_description']
                    ]);
                }
            }
            if ($parent_id != $blog_category_id) {
                $queryPath = BlogCategoryPath::where('path_id', $blog_category_id)->orderBy('level', 'ASC')->get();
                if (count($queryPath)) {
                    foreach ($queryPath as $result) {
                        BlogCategoryPath::where([['category_id', '=', $result->category_id], ['level', '<', $result->level]])->delete();
                        $path = array();
                        $query = BlogCategoryPath::where('category_id', $parent_id)->orderBy('level', 'ASC')->get();
                        foreach ($query as $item) {
                            $path[] = $item->path_id;
                        }
                        $query = BlogCategoryPath::where('category_id', $result->category_id)->orderBy('level', 'ASC')->get();
                        foreach ($query as $item) {
                            $path[] = $item->path_id;
                        }
                        $level = 0;
                        foreach ($path as $path_id) {
                            DB::statement("REPLACE INTO `blog_category_path` SET category_id = '" . (int)$result->category_id . "', `path_id` = '" . (int)$path_id . "', level = '" . (int)$level . "'");
                            $level++;
                        }
                    }
                } else {
                    BlogCategoryPath::where('category_id', $blog_category_id)->delete();
                    $level = 0;
                    $query = BlogCategoryPath::where('category_id', $parent_id)->orderBy('level', 'ASC')->get();
                    foreach ($query as $item) {
                        BlogCategoryPath::create(['category_id' => (int)$blog_category_id, 'path_id' => (int)$item->path_id, 'level' => (int)$level]);
                        $level++;
                    }
                    DB::statement("REPLACE INTO `blog_category_path` SET category_id = '" . (int)$blog_category_id . "', `path_id` = '" . (int)$blog_category_id . "', level = '" . (int)$level . "'");
                }
            }
            return response()->json([
                'success' => true,
                "message" => "Update success",
            ]);
        } else {
            $parent_id = $request->parent_id;
            $data = BlogCategory::create([
                'banner_group_id' => $request->banner_group_id, 'icon' => $request->icon, 'image' => $request->image,
                'parent_id' => $parent_id, 'status' => $request->status
            ]);
            $blog_category_id = $data->id;
            foreach ($request->descriptions as $item) {
                if (trim($item['title']) != '') {
                    BlogCategoryDescription::create([
                        'category_id' => $blog_category_id, 'language_id' => $item['languageCode'], 'title' => $item['title'], 'description' => $item['description'],
                        'meta_title' => $item['meta_title'], 'meta_description' => $item['meta_description']
                    ]);
                }
            }
            $level = 0;
            $path = BlogCategoryPath::where('category_id', $parent_id)->orderBy('level', 'ASC')->get();
            foreach ($path as $row) {
                BlogCategoryPath::create(['path_id' => $row->path_id, 'category_id' => $blog_category_id, 'level' => (int)$level]);
                $level++;
            }
            BlogCategoryPath::create(['path_id' => $blog_category_id, 'category_id' => $blog_category_id, 'level' => (int)$level]);
            return response()->json([
                'success' => true,
                "message" => "Add success",
            ]);
        }
    }

    public function changeStatus($request)
    {
        $updateBlogCate = BlogCategory::where('id', $request->id)->first();
        $updateBlogCate->status = $request->status;
        $updateBlogCate->save();
        return response()->json([
            'success' => true,
            'message' => "Change status success",
        ]);
    }

    public function hasChild($id, $module_id = 0)
    {
        return isset($this->children[$id . '_' . $module_id]);
    }

    /**
     * get collection of menu childrens by parent ID.
     */
    public function getNodes($id, $module_id = 0)
    {
        return $this->children[$id . '_' . $module_id];
    }
}
