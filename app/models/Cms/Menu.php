<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;
use DB;

class Menu extends Model
{
    protected $table = 'menu';

    protected $primaryKey = 'menu_id';

    protected $fillable = [
        'parent_id', 'item_id', 'position', 'type', 'status', 'group_id'
    ];
    protected $guarded = [
        'parent_id', 'item_id', 'position', 'type', 'status', 'group_id'
    ];
    protected $children = array();
    public $timestamps = true;

    public function getMenuDescriptions($menuId)
    {
        $description = array();
        $query = MenuDescription::where('menu_id', $menuId)->get();
        foreach ($query as $i => $item) {
            $description[$i] = array(
                'languageCode' => $item['language_id'],
                'title' => $item['title'],
                'link' => $item['link'],
            );
        }
        return $description;
    }

    public function detailMenu($request)
    {
        $data = Menu::where('menu_id', $request->id)->first();
        $data['descriptions'] = $this->getMenuDescriptions($request->id);
        return response()->json([
            'success' => true,
            'message' => "Get success",
            'data' => $data,
        ]);
    }

    public function saveMenu($request)
    {
        $menuId = $request->menu_id;
        if ($menuId > 0) {
            Menu::where('menu_id', $menuId)->update(['item_id' => $request->item_id, 'parent_id' => $request->parent_id, 'position' => $request->position, 'type' => $request->type,
                'group_id' => $request->group_id, 'status' => $request->status]);
            MenuDescription::where('menu_id', $menuId)->delete();
            foreach ($request->descriptions as $item) {
                if (trim($item['title']) != '') {
                    MenuDescription::create([
                        'menu_id' => $menuId, 'language_id' => $item['languageCode'], 'title' => $item['title'], 'link' => $item['link']
                    ]);
                }
            }
            return response()->json([
                'success' => true,
                'message' => "Update success",
            ]);
        } else {
            $data = Menu::create(['item_id' => $request->item_id, 'parent_id' => $request->parent_id, 'position' => $request->position, 'type' => $request->type,
                'group_id' => $request->group_id, 'status' => $request->status]);
            $menuId = $data->menu_id;
            foreach ($request->descriptions as $item) {
                if (trim($item['title']) != '') {
                    MenuDescription::create([
                        'menu_id' => $menuId, 'language_id' => $item['languageCode'], 'title' => $item['title'], 'link' => $item['link']
                    ]);
                }
            }
            return response()->json([
                'success' => true,
                'message' => "Add success",
            ]);
        }
    }

    public function del($request)
    {
        Menu::where('menu_id', $request->id)->delete();
        MenuDescription::where('menu_id', $request->id)->delete();
        return response()->json([
            'success' => true,
            'message' => "Delete status success",
        ]);
    }

    public function changeStatus($request)
    {
        $updateMenu = Menu::where('menu_id', $request->id)->first();
        $updateMenu->status = $request->status;
        $updateMenu->save();
        return response()->json([
            'success' => true,
            'message' => "Change status success",
        ]);
    }

    public function deleteCategory($request)
    {
        $query = Menu::where([
            ['type', '=', 'category'],
            ['group_id', '=', $request->group_id]
        ])->get();
        if ($query->count() > 0) {
            foreach ($query as $item) {
                MenuDescription::where('menu_id', $item['menu_id'])->delete();
            }
        }
        Menu::where([
            ['type', '=', 'category'],
            ['group_id', '=', $request->group_id]
        ])->delete();
        return response()->json([
            'success' => true,
            'message' => "Delete category success",
        ]);
    }

    public function importCategory($request)
    {
        $categoryDescription = new CategoryDescription();
        $menu_group_id = $request->menu_group_id;
        $query = DB::table('category AS c')->select('cd.title', 'c.*')
            ->leftJoin('category_description AS cd', 'c.id', '=', 'cd.category_id')
            ->where([
                ['cd.language_id', '=', getConfig('config_language_admin')],
                ['c.status', '=', 1],
            ])
            ->orderBy('parent_id', 'ASC')
            ->get();
        if ($query->count()) {
            $categories = $query;
        }
        foreach ($categories as $item) {
            $category = $categoryDescription->getCategoryDescriptions($item->id);
            if ($this->checkExitItemMenu($item->id, $menu_group_id) == 0) {
                if ((int)$item->parent_id > 0) {
                    $query1 = Menu::where([
                        ['type', '=', 'category'],
                        ['item_id', '=', $item->parent_id],
                        ['group_id', '=', $request->menu_group_id]
                    ])->select()->first();
                    if ($query1) {
                        $menu_parent_id = (int)$query1->menu_id;
                    } else {
                        $menu_parent_id = 1;
                    }
                } else {
                    $menu_parent_id = 1;
                }
                $this->insertCategory($item, $menu_parent_id, $menu_group_id, $category);
            }
        }
    }

    public function checkExitItemMenu($category_id, $menu_group_id)
    {
        $data = Menu::where([
            ['type', '=', 'category'],
            ['item_id', '=', $category_id],
            ['group_id', '=', $menu_group_id]
        ])->get();
        return $data->count();
    }

    public function insertCategory($item, $menu_parent_id, $menu_group_id, $category)
    {
        $data = Menu::create(['item_id' => $item->id, 'parent_id' => $menu_parent_id, 'type' => 'category', 'position' => 99, 'status' => 1, 'group_id' => $menu_group_id]);
        $menu_id = $data->menu_id;
        if (count($category) > 0) {
            MenuDescription::where('menu_id', $menu_id)->delete();
            foreach ($category as $result) {
                MenuDescription::create(['menu_id' => $menu_id, 'title' => $result['title'], 'language_id' => $result['languageCode']]);
            }
        }
    }

    public function hasChild($id, $menu_group_id = 0)
    {
        return isset($this->children[$id . '_' . $menu_group_id]);
    }

    public function getNodes($id, $menu_group_id = 0)
    {
        return $this->children[$id . '_' . $menu_group_id];
    }

    public function getTreeMenu($id = null, $request)
    {
        $selected_id = $request->selected_id;
        $menu_group_id = $request->menu_group_id;
        $childs = $this->getChild($id, $request);
        foreach ($childs as $child) {
            $this->children[$child->parent_id . '_' . $menu_group_id][] = $child;
        }
        $parent = 1;
        $output = $this->genTree($parent, 1, $selected_id, $menu_group_id);
        return response()->json([
            'success' => true,
            'message' => "Get success",
            'data' => $output,
        ]);
    }

    public function getChild($id = null, $request)
    {
        $query = Menu::select()
            ->where(function ($query) use ($request) {
//                if ($id != null) {
//                    return $query->where('parent_id', '=', $id);
//                }
                if ($request->menu_group_id != 0) {
                    return $query->where('group_id', '=', $request->menu_group_id);
                }
            })
            ->where('menu_description.language_id', getConfig('config_language_admin'))
            ->join('menu_description', 'menu_description.menu_id', '=', 'menu.menu_id')
            ->select('menu.menu_id AS menu_id', 'menu.item_id AS item_id', 'menu.parent_id AS parent_id', 'menu_description.link AS link', 'menu.position AS position',
                'menu.type AS type', 'menu.status AS status', 'menu.group_id AS group_id', 'menu_description.title AS title')
            ->orderBy('position', 'ASC')
            ->get();
        return $query;
    }

    public function genTree($parent, $level, $selected = 0, $menu_group_id)
    {
        if ($this->hasChild($parent, $menu_group_id)) {
            $output_data = [];
            $data = $this->getNodes($parent, $menu_group_id);
            for ($i = 0; $i < count($data); $i++) {
                $output_data[$i]['id'] = $data[$i]->menu_id;
                $output_data[$i]['text'] = $data[$i]->title;
                $output_data[$i]['position'] = $data[$i]->position;
                if ($this->genTree($data[$i]->menu_id, $level + 1, $selected, $menu_group_id) != null) {
                    $output_data[$i]['children'] = $this->genTree($data[$i]->menu_id, $level + 1, $selected, $menu_group_id);
                }
            }
            return $output_data;
        }
        return null;
    }

    public function saveOrderMenu($request)
    {
        foreach ($request as $i => $item) {
            Menu::where('menu_id', $item['id'])->update(['position' => $i, 'parent_id' => 1]);
            $menu_parent_id = $item['id'];
            if (isset($item['children']) && $item['children'] != null) {
                foreach ($item['children'] as $j => $child1) {
                    $menu_parent_2_id = $child1['id'];
                    Menu::where('menu_id', $child1['id'])->update(['parent_id' => $menu_parent_id, 'position' => $j]);
                    if (isset($child1['children']) && $child1['children'] != null) {
                        foreach ($child1['children'] as $k => $child2) {
                            Menu::where('menu_id', $child2['id'])->update(['parent_id' => $menu_parent_2_id, 'position' => $k]);
                            $menu_parent_3_id = $child2['id'];
                            if (isset($child2['children']) && $child2['children'] != null) {
                                foreach ($child2['children'] as $g => $child3) {
                                    Menu::where('menu_id', $child3['id'])->update(['parent_id' => $menu_parent_3_id, 'position' => $g]);
                                }
                            }
                        }
                    }
                }
            } else {
                Menu::where('menu_id', $item['id'])->update(['position' => $i]);
            }
        }
        return response()->json([
            'success' => true,
            'message' => "Update order success",
        ]);
    }
}
