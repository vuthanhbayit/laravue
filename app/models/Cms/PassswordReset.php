<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;

class PassswordReset extends Model
{
	protected $table="password_resets";
    protected $fillable = [
        'email', 'token'
    ];
}
