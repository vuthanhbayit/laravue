<?php

namespace App\models\Cms;

use App\models\Cms\AreaDescription;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{

    protected $table = 'area';

    protected $fillable = [
        'id', 'status'
    ];
    protected $guarded = [
        'id', 'status'
    ];

    public $timestamps = true;

    public function search($request)
    {
        $query = Area::where(function ($query) use ($request) {
            if (isset($request->title) && $request->title != '') {
                $query->where('area_description.title', 'like', '%' . $request->title . '%');
            }
            if (isset($request->status) && $request->status != -1) {
                $query->where('status', '=', $request->status);
            }
            if (isset($request->languageCode) && $request->languageCode != '') {
                $query->where('area_description.language_id', '=', $request->languageCode);
            } else {
                $query->where('area_description.language_id', '=', getConfig('config_language_admin'));
            }
        })->join('area_description', 'area_description.area_id', '=', 'area.id')
            ->select('area.id AS id', 'area_description.title AS title', 'area.status AS status')
            ->orderBy('id', 'DESC');

        $totalRow = $query->get();

        if (isset($request->pageIndex) && $request->pageIndex > 0) {
            if ($request->pageIndex == 1) {
                $query->offset(0);
            } else {
                $query->offset(($request->pageIndex - 1) * $request->pageSize);
            }
        }
        if ($request->pageSize == null) {
            $data = $query->get();
        } else {
            $data = $query->limit($request->pageSize)->get();
        }

        return response()->json([
            'success' => true,
            'message' => "Search success",
            'data' => $data,
            'totalRow' => $totalRow->count(),
        ]);
    }

    public function detail($request)
    {
        $data = Area::where('id', $request->id)->first();
        $data['titles'] = $this->getAreaDescriptions($request->id);
        return response()->json([
            'success' => true,
            'message' => "Get detail success",
            'data' => $data,
        ]);
    }

    public function getAreaDescriptions($areaId)
    {
        $description = array();
        $query = AreaDescription::where('area_id', $areaId)->get();
        foreach ($query as $i => $item) {
            $description[$i] = array(
                'languageCode' => $item['language_id'],
                'title' => $item['title'],
            );
        }
        return $description;
    }

    public function saveData($request)
    {
        if ($request->id > 0) {
            Area::where('id', $request->id)->update([
                'status' => $request->status
            ]);
            AreaDescription::where('area_id', $request->id)->delete();
            foreach ($request->titles as $item) {
                if (trim($item['title']) != '') {
                    AreaDescription::create([
                        'area_id' => $request->id, 'language_id' => $item['languageCode'], 'title' => $item['title']
                    ]);
                }
            }
            return response()->json([
                'success' => true,
                'message' => "Update success",
            ]);
        } else {
            $data = Area::create([
                'status' => $request->status
            ]);
            $areaId = $data->id;
            AreaDescription::where('area_id', $areaId)->delete();
            foreach ($request->titles as $item) {
                if (trim($item['title']) != '') {
                    AreaDescription::create([
                        'area_id' => $areaId, 'language_id' => $item['languageCode'], 'title' => $item['title']
                    ]);
                }
            }
            return response()->json([
                'success' => true,
                'message' => "Add success",
            ]);
        }
    }

    public function changeStatus($request)
    {
        $updateArea = Area::where('id', $request->id)->first();
        $updateArea->status = $request->status;
        $updateArea->save();
        return response()->json([
            'success' => true,
            'message' => "Change status success",
        ]);
    }

}
