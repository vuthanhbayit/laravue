<?php

namespace App\models\Cms;

use App\models\Cms\ProvinceDescription;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{

    protected $table = 'province';

    protected $fillable = [
        'id', 'area_id', 'status'
    ];
    protected $guarded = [
        'id', 'area_id', 'status'
    ];

    public $timestamps = true;

    public function search($request)
    {
        $query = Province::where(function ($query) use ($request) {
            if (isset($request->title) && $request->title != '') {
                $query->where('province_description.title', 'like', '%' . $request->title . '%');
            }
            if (isset($request->area_id) && $request->area_id != -1) {
                $query->where('province.area_id', 'like', '%' . $request->area_id . '%');
            }
            if (isset($request->status) && $request->status != -1) {
                $query->where('province.status', '=', $request->status);
            }
            if (isset($request->languageCode) && $request->languageCode != '') {
                $query->where('province_description.language_id', '=', $request->languageCode);
                $query->where('area_description.language_id', '=', $request->languageCode);
            } else {
                $query->where('province_description.language_id', '=', getConfig('config_language_admin'));
                $query->where('area_description.language_id', '=', getConfig('config_language_admin'));
            }
        })->join('province_description', 'province_description.province_id', '=', 'province.id')
            ->join('area','area.id','=','province.area_id')
            ->join('area_description', 'area_description.area_id', '=', 'area.id')
            ->select('province.id AS id', 'province.area_id AS area_id','area_description.title AS area_title', 'province_description.title AS title', 'province.status AS status')
            ->orderBy('id', 'DESC');
        
        $totalRow = $query->get();

        if (isset($request->pageIndex) && $request->pageIndex > 0) {
            if ($request->pageIndex == 1) {
                $query->offset(0);
            } else {
                $query->offset(($request->pageIndex - 1) * $request->pageSize);
            }
        }
        if ($request->pageSize == null) {
            $data = $query->get();
        } else {
            $data = $query->limit($request->pageSize)->get();
        }

        return response()->json([
            'success' => true,
            'message' => "Search success",
            'data' => $data,
            'totalRow' => $totalRow->count(),
        ]);
    }

    public function detail($request)
    {
        $data = Province::where('id', $request->id)->first();
        $data['titles'] = $this->getProvinceDescriptions($request->id);
        return response()->json([
            'success' => true,
            'message' => "Get detail success",
            'data' => $data,
        ]);
    }

    public function getProvinceDescriptions($provinceId)
    {
        $description = array();
        $query = ProvinceDescription::where('province_id', $provinceId)->get();
        foreach ($query as $i => $item) {
            $description[$i] = array(
                'languageCode' => $item['language_id'],
                'title' => $item['title'],
            );
        }
        return $description;
    }

    public function saveData($request)
    {
        if ($request->id > 0) {
            Province::where('id', $request->id)->update([
                'status' => $request->status,
                'area_id' => $request->area_id
            ]);
            ProvinceDescription::where('province_id', $request->id)->delete();
            foreach ($request->titles as $item) {
                if (trim($item['title']) != '') {
                    ProvinceDescription::create([
                        'province_id' => $request->id, 'language_id' => $item['languageCode'], 'title' => $item['title']
                    ]);
                }
            }
            return response()->json([
                'success' => true,
                'message' => "Update success",
            ]);
        } else {
            $data = Province::create([
                'status' => $request->status,
                'area_id' => $request->area_id
            ]);
            $provinceId = $data->id;
            ProvinceDescription::where('province_id', $provinceId)->delete();
            foreach ($request->titles as $item) {
                if (trim($item['title']) != '') {
                    ProvinceDescription::create([
                        'province_id' => $provinceId, 'language_id' => $item['languageCode'], 'title' => $item['title']
                    ]);
                }
            }
            return response()->json([
                'success' => true,
                'message' => "Add success",
            ]);
        }
    }

    public function changeStatus($request)
    {
        $updateProvince = Province::where('id', $request->id)->first();
        $updateProvince->status = $request->status;
        $updateProvince->save();
        return response()->json([
            'success' => true,
            'message' => "Change status success",
        ]);
    }

}
