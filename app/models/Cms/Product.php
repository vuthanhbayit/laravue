<?php

namespace App\models\Cms;

use DB;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';
    protected $primaryKey = 'id';
    protected $fillable = [
        'code', 'departTime', 'seatsExist', 'price', 'image', 'viewed', 'status'
    ];

    protected $guarded = [
        'code', 'departTime', 'seatsExist', 'price', 'image', 'viewed', 'status'
    ];

    public function search($request)
    {
        $query = DB::table('product AS p')
            ->where(function ($query) use ($request) {
                if (isset($request->category_id) && $request->category_id > 0) {
                    $query->leftJoin('product_category AS p2c', 'p.id', '=', 'p2c.product_id');
                }
                if (isset($request->title) && $request->title != '') {
                    $query->where('pd.title', 'like', '%' . $request->title . '%');
                }
                if (isset($request->status) && $request->status != -1) {
                    $query->where('p.status', '=', $request->status);
                }
                if (isset($request->code) && $request->code != '') {
                    $query->where('p.code', 'like', '%' . $request->code . '%');
                }
                if (isset($request->languageCode) && $request->languageCode != '') {
                    $query->where('pd.language_id', '=', $request->languageCode);
                } else {
                    $query->where('pd.language_id', '=', getConfig('config_language_admin'));
                }
            })
            ->join('product_description AS pd', 'pd.product_id', '=', 'p.id')
            ->select('p.id AS id', 'pd.title AS title', 'p.image AS image', 'p.code AS code', 'pd.description AS description', 'p.status AS status')
            ->orderBy('id', 'DESC');

        $totalRow = $query->get();
        if (isset($request->pageIndex) && $request->pageIndex > 0) {
            if ($request->pageIndex == 1) {
                $query->offset(0);
            } else {
                $query->offset(($request->pageIndex - 1) * $request->pageSize);
            }
        }

        if ($request->pageSize == null) {
            $data = $query->get();
        } else {
            $data = $query->limit($request->pageSize)->get();
        }
        foreach ($data as $k => $v) {
            $filename = $v->image;
            if ($filename) {
                resizeImage($filename, 'cache/' . $filename);
                $v->image = 'cache/' . $filename;
            }
        }
        return response()->json([
            'success' => true,
            'message' => "Search success",
            'data' => $data,
            'totalRow' => $totalRow->count(),
        ]);
    }

    public function detail($request)
    {
        $product_id = $request->product['id'];
        $data = Product::where('id', $product_id)->first();
        return $data;
    }

    public function getProductDescriptions($productId)
    {
        $description = array();
        $query = ProductDescription::where('product_id', $productId)->get();
        foreach ($query as $i => $item) {
            $description[$i] = array(
                'languageCode' => $item['language_id'],
                'dayNumber' => $item['dayNumber'],
                'departAddress' => $item['departAddress'],
                'title' => $item['title'],
                'description' => $item['description'],
                'content' => $item['content'],
                'meta_title' => $item['meta_title'],
                'meta_description' => $item['meta_description'],
            );
        }
        return $description;
    }

    public function getProductImage($product_id)
    {
        $data = ProductImage::select('product_image.id as id', 'product_image.image as image', 'product_image.position as position')
            ->join('product', 'product.id', '=', 'product_image.product_id')
            ->where([
                ['product_id', '=', $product_id],
            ])
            ->get();
        return $data;
    }

    public function getProductAttribute($product_id)
    {
        $attributes = [];
        $query = ProductAttribute::where('product_id', $product_id)->groupBy('attribute_id')->select('attribute_id')->get();
        foreach ($query as $i => $item) {
            $product_attribute_description_data = [];
            $query_description = ProductAttribute::where([
                ['product_id', '=', $product_id],
                ['attribute_id', '=', $item['attribute_id']],
            ])->get();
            foreach ($query_description as $k => $item_description) {
                $product_attribute_description_data[] = array(
                    'languageCode' => $item_description['language_id'],
                    'content' => $item_description['content']
                );
            }
            $attributes[$i] = array(
                'attribute_id' => $item['attribute_id'],
                'product_attribute' => $product_attribute_description_data
            );
        }
        return $attributes;
    }

    public function getProductDay($product_id)
    {
        $days = [];
        $query = ProductDay::where([
            ['product_id', '=', $product_id],
        ])->groupBy('day_id')->select('day_id')->get();

        foreach ($query as $i => $item) {
            $product_day_description_data = [];
            $query_description = ProductDay::where([
                ['product_id', '=', $product_id],
                ['day_id', '=', $item['day_id']],
            ])->get();
            foreach ($query_description as $k => $item_description) {
                $product_day_description_data[] = array(
                    'languageCode' => $item_description['language_id'],
                    'content' => $item_description['content']
                );
            }
            $days[$i] = array(
                'day_id' => $item['day_id'],
                'product_day' => $product_day_description_data
            );
        }
        return $days;
    }

    public function getProductRelated($product_id)
    {
        $data = Product::select('product_related.related_id as id', 'product_related.product_id as product_id', 'product_description.title as title')
            ->join('product_related', 'product_related.related_id', '=', 'product.id')
            ->join('product_description', 'product_description.product_id', '=', 'product.id')
            ->where([
                ['product_related.product_id', '=', $product_id],
                ['product_description.language_id', '=', getConfig('config_language_admin')],
            ])
            ->get();
        return $data;
    }

    public function getProductCategory($product_id)
    {
        $data = array();
        $query = ProductCategory::select('category_id')
            ->where('product_id', $product_id)
            ->get();
        foreach ($query as $item) {
            $data[] = $item['category_id'];
        }
        return $data;
    }

    public function saveData($request)
    {
        $product_id = $request->product['id'];
        if ($product_id > 0) {
            if ($request->has('product')) {
                Product::where('id', $product_id)->update([
                    'code' => $request->product['code'], 'departTime' => $request->product['departTime'], 'seatsExist' => $request->product['seatsExist'],
                    'price' => $request->product['price'], 'image' => $request->product['image'], 'viewed' => $request->product['viewed'], 'status' => $request->product['status']
                ]);
            }
            if ($request->has('descriptions')) {
                ProductDescription::where('product_id', $product_id)->delete();
                if (!empty($request->descriptions)) {
                    foreach ($request->descriptions as $item) {
                        ProductDescription::create(['product_id' => $product_id, 'language_id' => $item['languageCode'],
                            'title' => $item['title'], 'description' => $item['description'], 'meta_title' => $item['meta_title'], 'meta_description' => $item['meta_description'],
                            'dayNumber' => $item['dayNumber'], 'departAddress' => $item['departAddress'], 'content' => $item['content']]);
                    }
                }
            }
            if ($request->has('product_att')) {
                ProductAttribute::where('product_id', $product_id)->delete();
                if (!empty($request->product_att)) {
                    foreach ($request->product_att as $item) {
                        if ($item['attribute_id']) {
                            ProductAttribute::where([
                                ['product_id', '=', $product_id],
                                ['attribute_id', '=', $item['attribute_id']]
                            ])->delete();
                            foreach ($item['product_attribute'] as $value) {
                                ProductAttribute::create(['attribute_id' => $item['attribute_id'], 'language_id' => $value['languageCode'], 'content' => $value['content'], 'product_id' => $product_id]);
                            }
                        }
                    }
                }
            }
            if ($request->has('days') && $product_id > 0) {
                ProductDay::where('product_id', $product_id)->delete();
                if (!empty($request->days)) {
                    foreach ($request->days as $item) {
                        if ($item['day_id']) {
                            ProductDay::where([
                                ['product_id', '=', $product_id],
                                ['day_id', '=', $item['day_id']]
                            ])->delete();
                            foreach ($item['product_day'] as $value) {
                                ProductDay::create(['day_id' => $item['day_id'], 'language_id' => $value['languageCode'], 'product_id' => $product_id, 'content' => $value['content']]);
                            }
                        }
                    }
                }
            }
            if ($request->has('product_image')) {
                ProductImage::where('product_id', $product_id)->delete();
                if (!empty($request->product_image)) {
                    foreach ($request->product_image as $item) {
                        ProductImage::create(['position' => $item['position'], 'image' => $item['image'], 'product_id' => $product_id]);
                    }
                }
            }

            if ($request->has('product_category')) {
                ProductCategory::where('product_id', $product_id)->delete();
                if (!empty($request->product_category)) {
                    foreach ($request->product_category as $item) {
                        ProductCategory::create(['category_id' => $item['id'], 'product_id' => $product_id]);
                    }
                }
            }
            if ($request->has('product_related')) {
                ProductRelated::where('product_id', $product_id)->delete();
                ProductRelated::where('related_id', $product_id)->delete();
                if (!empty($request->product_related)) {
                    foreach ($request->product_related as $item) {
                        ProductRelated::where([
                            ['related_id', '=', (int)$item['id']],
                            ['product_id', '=', (int)$product_id]
                        ])->delete();
                        ProductRelated::create(['related_id' => $item['id'], 'product_id' => $product_id]);
                        ProductRelated::where([
                            ['product_id', '=', (int)$item['id']],
                            ['related_id', '=', (int)$product_id]
                        ])->delete();
                        ProductRelated::create(['related_id' => (int)$product_id, 'product_id' => (int)$item['id']]);
                    }
                }
            }
            return response()->json([
                'success' => true,
                "message" => "Edit success",
            ]);
        } else {
            $product_id = 0;
            if ($request->has('product')) {
                $data = Product::create([
                    'code' => $request->product['code'], 'departTime' => $request->product['departTime'], 'seatsExist' => $request->product['seatsExist'],
                    'price' => $request->product['price'], 'image' => $request->product['image'], 'viewed' => $request->product['viewed'], 'status' => $request->product['status']
                ]);
                $product_id = $data->id;
            }
            if ($request->has('descriptions') && $product_id > 0) {
                if (!empty($request->descriptions)) {
                    foreach ($request->descriptions as $item) {
                        ProductDescription::create(['product_id' => $product_id, 'language_id' => $item['languageCode'],
                            'title' => $item['title'], 'description' => $item['description'], 'meta_title' => $item['meta_title'], 'meta_description' => $item['meta_description'],
                            'dayNumber' => $item['dayNumber'], 'departAddress' => $item['departAddress'], 'content' => $item['content']]);
                    }
                }
            }
            if ($request->has('product_att') && $product_id > 0) {
                if (!empty($request->product_att)) {
                    foreach ($request->product_att as $item) {
                        if ($item['attribute_id']) {
                            ProductAttribute::where([
                                ['product_id', '=', $product_id],
                                ['attribute_id', '=', $item['attribute_id']]
                            ])->delete();
                            foreach ($item['product_attribute'] as $value) {
                                ProductAttribute::where([
                                    ['product_id', '=', $product_id],
                                    ['attribute_id', '=', $item['attribute_id']],
                                    ['language_id', '=', $value['languageCode']]
                                ])->delete();
                                ProductAttribute::create(['attribute_id' => $item['attribute_id'], 'language_id' => $value['languageCode'], 'content' => $value['content'], 'product_id' => $product_id]);
                            }
                        }
                    }
                }
            }
            if ($request->has('days') && $product_id > 0) {
                if (!empty($request->days)) {
                    foreach ($request->days as $item) {
                        if ($item['day_id']) {
                            ProductDay::where([
                                ['product_id', '=', $product_id],
                                ['day_id', '=', $item['day_id']]
                            ])->delete();
                            foreach ($item['product_day'] as $value) {
                                ProductDay::where([
                                    ['product_id', '=', $product_id],
                                    ['day_id', '=', $item['day_id']],
                                    ['language_id', '=', $value['languageCode']]
                                ])->delete();
                                ProductDay::create(['day_id' => $item['day_id'], 'language_id' => $value['languageCode'], 'product_id' => $product_id, 'content' => $value['content']]);
                            }
                        }
                    }
                }
            }
            if ($request->has('product_image') && $product_id > 0) {
                if (!empty($request->product_image)) {
                    foreach ($request->product_image as $item) {
                        ProductImage::create(['position' => $item['position'], 'image' => $item['image'], 'product_id' => $product_id]);
                    }
                }
            }

            if ($request->has('product_category') && $product_id > 0) {
                if (!empty($request->product_category)) {
                    foreach ($request->product_category as $item) {
                        ProductCategory::create(['category_id' => $item['id'], 'product_id' => $product_id]);
                    }
                }
            }
            if ($request->has('product_related') && $product_id > 0) {
                if (!empty($request->product_related)) {
                    foreach ($request->product_related as $item) {
                        ProductRelated::where([
                            ['related_id', '=', (int)$item['id']],
                            ['product_id', '=', (int)$product_id]
                        ])->delete();
                        ProductRelated::create(['related_id' => $item['id'], 'product_id' => $product_id]);
                        ProductRelated::where([
                            ['product_id', '=', (int)$item['id']],
                            ['related_id', '=', (int)$product_id]
                        ])->delete();
                        ProductRelated::create(['related_id' => (int)$product_id, 'product_id' => (int)$item['id']]);
                    }
                }
            }
            return response()->json([
                'success' => true,
                "message" => "Add success",
            ]);
        }

    }

    public function changeStatus($request)
    {
        $updateProduct = Product::where('id', $request->id)->first();
        $updateProduct->status = $request->status;
        $updateProduct->save();
        return response()->json([
            'success' => true,
            "message" => "Change status success",
        ]);
    }
}
