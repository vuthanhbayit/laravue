<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProductDescription extends Model
{
    protected $table = 'product_description';
    protected $primaryKey = 'product_id';
    protected $fillable = [
        'product_id', 'language_id', 'dayNumber', 'departAddress', 'title', 'description', 'content', 'meta_title', 'meta_description'
    ];
    protected $guarded = [
        'product_id', 'language_id', 'dayNumber', 'departAddress', 'title', 'description', 'content', 'meta_title', 'meta_description'
    ];
}
