<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Role;

class RoleUser extends Model
{
    protected $table = 'role_user';

    protected $fillable = [
    	'user_id','role_id'
    ];
    protected $guarded = [
    	'user_id','role_id'
    ];
    public function Roles()
    {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }
    public function getListpermission($user_id)
    {
        $query = DB::table('role_user')
            ->join('permission_role', 'permission_role.role_id', '=', 'role_user.role_id')
            ->join('permissions', 'permissions.id', '=', 'permission_role.permission_id')
            ->where('role_user.user_id', $user_id)->get();
       return $query;
    }
}
