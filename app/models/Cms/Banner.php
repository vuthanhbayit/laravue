<?php

namespace App\models\Cms;

use Illuminate\Support\Facades\File;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = 'banner';

    protected $primaryKey = 'id';

    protected $fillable = [
        'banner_group_id', 'link', 'image', 'status'
    ];
    protected $guarded = [
        'banner_group_id', 'link', 'image', 'status'
    ];

    public $timestamps = true;

    public function search($request)
    {
        $query = Banner::where(function ($query) use ($request) {
            if (isset($request->title) && $request->title != '') {
                $query->where('title', 'like', '%' . $request->title . '%');
            }
            if (isset($request->status) && $request->status != -1) {
                $query->where('status', '=', $request->status);
            }
        })
            ->orderBy('id', 'DESC');
        if (isset($request->pageIndex) && $request->pageIndex > 0) {
            if ($request->pageIndex == 1) {
                $query->offset(0);
            } else {
                $query->offset(($request->pageIndex - 1) * $request->pageSize);
            }
        }

        if ($request->pageSize == null) {
            $data = $query->get();
        } else {
            $data = $query->limit($request->pageSize)->get();
        }

        $totalRow = Banner::where(function ($query) use ($request) {
            if (isset($request->title) && $request->title != '') {
                $query->where('title', 'like', '%' . $request->title . '%');
            }
            if (isset($request->status) && $request->status != -1) {
                $query->where('status', '=', $request->status);
            }
        })->orderBy('id', 'DESC')
            ->get();

        return response()->json([
            'success' => true,
            'message' => "Search success",
            'data' => $data,
            'totalRow' => $totalRow->count(),
        ]);
    }

    public function getBannerDescriptions($bannerId)
    {
        $description = array();
        $query = BannerDescription::where('banner_id', $bannerId)->get();
        foreach ($query as $i => $item) {
            $description[$i] = array(
                'languageCode' => $item['language_id'],
                'title' => $item['title'],
                'content' => $item['content'],
            );
        }
        return $description;
    }

    public function detail($request)
    {
        $data = Banner::where('id', $request->id)->first();
        $data['descriptions'] = $this->getBannerDescriptions($request->id);
        return response()->json([
            'success' => true,
            'message' => "Get success",
            'data' => $data,
        ]);
    }

    public function getBannerByGroupId($request)
    {
        $data = Banner::where('banner_group_id', $request->banner_group_id)->get();
        foreach ($data as $k => $v) {
            $filename = $v->image;
            if ($filename) {
                resizeImage($filename, 'cache/' . $filename, 230, 100);
                $v->image = 'cache/' . $filename;
            }
        }
        return response()->json([
            'success' => true,
            'message' => "Get success",
            'data' => $data,
        ]);
    }

    public function saveData($request)
    {
        $bannerId = $request->id;
        if ($bannerId > 0) {
            Banner::where('id', $request->id)->update(['banner_group_id' => $request->banner_group_id, 'link' => $request->link,
                'image' => $request->image, 'status' => $request->status]);
            BannerDescription::where('banner_id', $bannerId)->delete();
            foreach ($request->descriptions as $item) {
                if (trim($item['title']) != '') {
                    BannerDescription::create(['banner_id' => $bannerId, 'language_id' => $item['languageCode'], 'title' => $item['title'], 'content' => $item['content']]);
                }
            }
            return response()->json([
                'success' => true,
                'message' => "Update success",
            ]);
        } else {
            $data = Banner::create(['banner_group_id' => $request->banner_group_id, 'link' => $request->link,
                'image' => $request->image, 'status' => $request->status]);
            $bannerId = $data->id;
            foreach ($request->descriptions as $item) {
                if (trim($item['title']) != '') {
                    BannerDescription::create(['banner_id' => $bannerId, 'language_id' => $item['languageCode'], 'title' => $item['title'], 'content' => $item['content']]);
                }
            }
            return response()->json([
                'success' => true,
                'message' => "Add success",
            ]);
        }
    }

    public function del($request)
    {
        Banner::where([
            ['id', $request->id],
            ['banner_group_id', $request->banner_group_id],
        ])->delete();
        BannerDescription::where('banner_id', $request->id)->delete();
        $isDeleteImage = $this->deleteImage($request);
        if ($isDeleteImage == true) {
            $data = 'Delete image success';
        } else {
            $data = 'Delete image failed';
        }
        return response()->json([
            'success' => true,
            'message' => "Delete success",
            'data' => $data
        ]);
    }

    public function deleteImage($request)
    {
        $image_path = $request->image;
        if ($image_path === 'assets/no-img.png' || $image_path === 'assets/file-upload.jpg') {
            return false;
        } else {
            if (File::exists($image_path)) {
                File::delete($image_path);
                return true;
            } else {
                return false;
            }
        }
    }
}
