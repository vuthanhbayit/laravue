<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;

class ProductDay extends Model
{
    protected $table = 'product_day';
    protected $primaryKey = 'day_id';
    protected $fillable = [
        'day_id', 'product_id', 'language_id', 'content'
    ];
    protected $guarded = [
        'day_id', 'product_id', 'language_id', 'content'
    ];
}
