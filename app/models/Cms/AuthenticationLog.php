<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;
use App\User;
use DateTime;

class AuthenticationLog extends Model
{
    protected $table = 'authentication_log';

    protected $fillable = [
        'id', 'authenticatable_type', 'authenticatable_id', 'ip_address', 'user_agent', 'login_at', 'logout_at'
    ];
    protected $guarded = [
        'id', 'authenticatable_type', 'authenticatable_id', 'ip_address', 'user_agent', 'login_at', 'logout_at'
    ];

    public function User()
    {
        return $this->hasOne(User::class, 'id', 'authenticatable_id');
    }

    public function getHistory($request)
    {
        $user_id = $request->user_id;
        $start = date_format(new DateTime($request->start_date), 'Y-m-d h:i:s');
        $end = date_format(new DateTime($request->end_date), 'Y-m-d h:i:s');
        $pageIndex = $request->pageIndex;
        $pageSize = $request->pageSize;
        $offset = ($pageIndex - 1) * $pageSize;
        if ($user_id) {
            if ($request->start_date == null && $request->end_date == null) {
                $history = $this
                    ->with(['User'])
                    ->where('authenticatable_id', $user_id)
                    ->offset($offset)
                    ->limit($pageSize)
                    ->orderBy('id', 'DESC')
                    ->get();

                $totalRow =  $this
                    ->with(['User'])
                    ->where('authenticatable_id', $user_id)
                    ->orderBy('id', 'DESC')
                    ->get();
            } elseif ($request->start_date) {
                $history = $this->with([
                    'User'
                ])
                    ->where('authenticatable_id', $user_id)
                    ->where('login_at', '>=', $start)
                    ->offset($offset)
                    ->limit($pageSize)
                    ->orderBy('id', 'DESC')
                    ->get();

                $totalRow = $this->with([
                    'User'
                ])
                    ->where('authenticatable_id', $user_id)
                    ->where('login_at', '>=', $start)
                    ->orderBy('id', 'DESC')
                    ->get();
            } elseif ($request->end_date) {
                $history = $this->with([
                    'User'
                ])
                    ->where('authenticatable_id', $user_id)
                    ->where('login_at', '<', $end)
                    ->offset($offset)
                    ->limit($pageSize)
                    ->orderBy('id', 'DESC')
                    ->get();

                $totalRow = $this->with([
                    'User'
                ])
                    ->where('authenticatable_id', $user_id)
                    ->where('login_at', '<', $end)
                    ->orderBy('id', 'DESC')
                    ->get();
            } else {
                $history = $this->with([
                    'User'
                ])
                    ->where('authenticatable_id', $user_id)
                    ->where('login_at', '>=', $start)
                    ->where('login_at', '<', $end)
                    ->offset($offset)
                    ->limit($pageSize)
                    ->orderBy('id', 'DESC')
                    ->get();

                $totalRow = $this->with([
                    'User'
                ])
                    ->where('authenticatable_id', $user_id)
                    ->where('login_at', '>=', $start)
                    ->where('login_at', '<', $end)
                    ->orderBy('id', 'DESC')
                    ->get();
            }
        } else {
            $history = $this
                ->with(['User'])
                ->offset($offset)
                ->limit($pageSize)
                ->orderBy('id', 'DESC')
                ->get();

            $totalRow = $this
                ->with(['User'])
                ->orderBy('id', 'DESC')
                ->get();
        }
        return array(
            'data' => isset($history) ? $history : array(),
            'totalRow' => isset($totalRow) ? $totalRow->count() : 0,
        );
    }
}
