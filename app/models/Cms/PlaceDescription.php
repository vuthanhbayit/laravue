<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;

class PlaceDescription extends Model
{
    protected $table = 'place_description';

    protected $primaryKey = 'place_id';
    protected $fillable = [
        'place_id', 'language_id', 'title'
    ];
    protected $guarded = [
        'place_id', 'language_id', 'title'
    ];
}
