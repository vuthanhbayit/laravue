<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;

class GoodPoint extends Model
{
    protected $table = 'good_point';

    protected $primaryKey = 'id';

    protected $fillable = [
        'category_id', 'icon', 'position', 'status'
    ];
    protected $guarded = [
        'category_id', 'icon', 'position', 'status'
    ];

    public function search($request)
    {
        $query = GoodPoint::where(function ($query) use ($request) {
            if (isset($request->content) && $request->content != '') {
                $query->where('good_point_description.content', 'like', '%' . $request->content . '%');
            }
            if (isset($request->status) && $request->status != -1) {
                $query->where('good_point.status', '=', $request->status);
            }
            if (isset($request->category_id) && $request->category_id > 0) {
                $query->where('good_point.category_id', '=', $request->category_id);
            }
            if (isset($request->languageCode) && $request->languageCode != '') {
                $query->where('good_point_description.language_id', '=', $request->languageCode);
                $query->where('category_description.language_id', '=', $request->languageCode);
            } else {
                $query->where('good_point_description.language_id', '=', getConfig('config_language_admin'));
                $query->where('category_description.language_id', '=', getConfig('config_language_admin'));
            }
        })->join('good_point_description', 'good_point_description.good_point_id', '=', 'good_point.id')
            ->join('category', 'category.id', '=', 'good_point.category_id')
            ->join('category_description', 'category_description.category_id', '=', 'category.id')
            ->select('good_point.id AS id', 'good_point_description.content AS content', 'category_description.title AS title', 'good_point.status AS status')
            ->orderBy('id', 'DESC');

        $totalRow = $query->get();

        if (isset($request->pageIndex) && $request->pageIndex > 0) {
            if ($request->pageIndex == 1) {
                $query->offset(0);
            } else {
                $query->offset(($request->pageIndex - 1) * $request->pageSize);
            }
        }
        if ($request->pageSize == null) {
            $data = $query->get();
        } else {
            $data = $query->limit($request->pageSize)->get();
        }

        return response()->json([
            'success' => true,
            'message' => "Search success",
            'data' => $data,
            'totalRow' => $totalRow->count(),
        ]);
    }

    public function detail($request)
    {
        $data = GoodPoint::where('id', $request->id)->first();
        $data['contents'] = $this->getGoodPointDescriptions($request->id);
        return response()->json([
            'success' => true,
            'message' => "Get detail success",
            'data' => $data,
        ]);
    }

    public function getGoodPointDescriptions($goodPointId)
    {
        $description = array();
        $query = GoodPointDescription::where('good_point_id', $goodPointId)->get();
        foreach ($query as $i => $item) {
            $description[$i] = array(
                'languageCode' => $item['language_id'],
                'content' => $item['content'],
            );
        }
        return $description;
    }

    public function saveData($request)
    {
        $good_point_id = $request->id;
        if ($good_point_id > 0) {
            GoodPoint::where('id', $good_point_id)->update([
                'category_id' => $request->category_id, 'icon' => $request->icon, 'position' => $request->position, 'status' => $request->status
            ]);
            GoodPointDescription::where('good_point_id', $good_point_id)->delete();
            foreach ($request->contents as $item) {
                if (trim($item['content']) != '') {
                    GoodPointDescription::create([
                        'good_point_id' => $good_point_id, 'language_id' => $item['languageCode'], 'content' => $item['content']
                    ]);
                }
            }
            return response()->json([
                'success' => true,
                'message' => "Update success",
            ]);
        } else {
            $data = GoodPoint::create([
                'category_id' => $request->category_id, 'icon' => $request->icon, 'position' => $request->position, 'status' => $request->status
            ]);
            $good_point_id = $data->id;
            GoodPointDescription::where('good_point_id', $good_point_id)->delete();
            foreach ($request->contents as $item) {
                if (trim($item['content']) != '') {
                    GoodPointDescription::create([
                        'good_point_id' => $good_point_id, 'language_id' => $item['languageCode'], 'content' => $item['content']
                    ]);
                }
            }
            return response()->json([
                'success' => true,
                'message' => "Add success",
            ]);
        }
    }

    public function changeStatus($request)
    {
        GoodPoint::where('id', $request->id)->update(['status' => $request->status]);
        return response()->json([
            'success' => true,
            'message' => "Change status success",
        ]);
    }
}
