<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AttributeDescription extends Model
{
    protected $table = 'attribute_description';

    protected $primaryKey = 'attribute_id';

    protected $fillable = [
        'attribute_id', 'language_id', 'title'
    ];
    protected $guarded = [
        'attribute_id', 'language_id', 'title'
    ];
}
