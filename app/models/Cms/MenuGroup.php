<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;

class MenuGroup extends Model
{
    protected $table = 'menu_group';

    protected $primaryKey = 'id';

    protected $fillable = [
        'title', 'position', 'status'
    ];
    protected $guarded = [
        'title', 'position', 'status'
    ];

    public function search($request)
    {
        $query = MenuGroup::where(function ($query) use ($request) {
            if (isset($request->title) && $request->title != '') {
                $query->where('title', 'like', '%' . $request->title . '%');
            }
            if (isset($request->status) && $request->status != -1) {
                $query->where('status', '=', $request->status);
            }
        })
            ->orderBy('id', 'DESC');
        if (isset($request->pageIndex) && $request->pageIndex > 0) {
            if ($request->pageIndex == 1) {
                $query->offset(0);
            } else {
                $query->offset(($request->pageIndex - 1) * $request->pageSize);
            }
        }

        if ($request->pageSize == null) {
            $data = $query->get();
        } else {
            $data = $query->limit($request->pageSize)->get();
        }

        $totalRow = MenuGroup::where(function ($query) use ($request) {
            if (isset($request->title) && $request->title != '') {
                $query->where('title', 'like', '%' . $request->title . '%');
            }
            if (isset($request->status) && $request->status != -1) {
                $query->where('status', '=', $request->status);
            }
        })
            ->orderBy('id', 'DESC')
            ->get();

        return response()->json([
            'success' => true,
            'message' => "Search success",
            'data' => $data,
            'totalRow' => $totalRow->count(),
        ]);
    }

    public function saveData($request)
    {
        if ($request->id > 0) {
            MenuGroup::where('id', $request->id)->update(handle($request));
            return response()->json([
                'success' => true,
                'message' => "Update success",
            ]);
        } else {
            MenuGroup::create(handle($request));
            return response()->json([
                'success' => true,
                'message' => "Add success",
            ]);
        }
    }

    public function detail($request)
    {
        $data = MenuGroup::where('id', $request->id)->first();
        return response()->json([
            'success' => true,
            'message' => "Get success",
            'data' => $data,
        ]);
    }

    public function changeStatus($request)
    {
        MenuGroup::where('id', $request->id)->update(['status' => $request->status]);
        return response()->json([
            'success' => true,
            'message' => "Change status success",
        ]);
    }
}
