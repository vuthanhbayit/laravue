<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    protected $table = 'category';

    protected $primaryKey = 'id';

    protected $fillable = [
        'parent_id', 'banner_group_id', 'position', 'status', 'image', 'icon'
    ];
    protected $guarded = [
        'parent_id', 'banner_group_id', 'position', 'status', 'image', 'icon'
    ];

    public function search($request)
    {
        $query = DB::table('category_path AS cp')->select('cp.category_id AS id', 'c1.image AS image', 'c1.parent_id AS parent_id', 'c1.position AS `position`', 'c1.status')
            ->selectRaw("GROUP_CONCAT(cd1.title ORDER BY cp.level SEPARATOR ' > ') AS title")
            ->leftJoin('category AS c1', 'cp.category_id', '=', 'c1.id')
            ->leftJoin('category AS c2', 'cp.id', '=', 'c2.id')
            ->leftJoin('category_description AS cd1', 'cp.id', '=', 'cd1.category_id')
            ->leftJoin('category_description AS cd2', 'cp.category_id', '=', 'cd2.category_id')
            ->groupBy('cp.category_id')
            ->where(function ($query) use ($request) {
                if (isset($request->title) && $request->title != '') {
                    $query->where('cd2.title', 'like', '%' . $request->title . '%');
                }
                if (isset($request->status) && $request->status != -1) {
                    $query->where('c1.status', '=', $request->status);
                }
                if (isset($request->languageCode) && $request->languageCode != '') {
                    $query->where([
                        ['cd1.language_id', '=', $request->languageCode],
                        ['cd2.language_id', '=', $request->languageCode],
                    ]);
                } else {
                    $query->where([
                        ['cd1.language_id', '=', getConfig('config_language_admin')],
                        ['cd2.language_id', '=', getConfig('config_language_admin')],
                    ]);
                }
            })
            ->orderBy('title', 'ASC');

        $totalRow = $query->get();

        if (isset($request->pageIndex) && $request->pageIndex > 0) {
            if ($request->pageIndex == 1) {
                $query->offset(0);
            } else {
                $query->offset(($request->pageIndex - 1) * $request->pageSize);
            }
        }

        if ($request->pageSize == null) {
            $data = $query->get();
        } else {
            $data = $query->limit($request->pageSize)->get();
        }
        foreach ($data as $k => $v) {
            $filename = $v->image;
            if ($filename) {
                resizeImage($filename, 'cache/' . $filename);
                $v->image = 'cache/' . $filename;
            }
        }
        return response()->json([
            'success' => true,
            "message" => "Search success",
            'data' => $data,
            'totalRow' => $totalRow->count(),
        ]);
    }

    public function detail($request)
    {
        $data['category'] = Category::where('id', $request->category['id'])->first();
        $data['descriptions'] = $this->getCategoryDescriptions($request->category['id']);
        return response()->json([
            'success' => true,
            "message" => "Get success",
            'data' => $data
        ]);
    }
    public function getCategoryDescriptions($categoryId)
    {
        $description = array();
        $query = CategoryDescription::where('category_id', $categoryId)->get();
        foreach ($query as $i => $item) {
            $description[$i] = array(
                'languageCode' => $item['language_id'],
                'title' => $item['title'],
                'short_description' => $item['short_description'],
                'description' => $item['description'],
                'meta_title' => $item['meta_title'],
                'meta_description' => $item['meta_description'],
            );
        }
        return $description;
    }

    public function getCategory($category_id) {
//        DB::table('category_path AS cp')->select('*')
//            ->selectRaw("GROUP_CONCAT(cd1.name ORDER BY level SEPARATOR ' > ') FROM category_path cp LEFT JOIN category_description cd1 ON
//            (cp.path_id = cd1.category_id AND cp.category_id != cp.path_id) WHERE cp.category_id = c.category_id AND cd1.language_id = '" . getConfig('config_language_admin') . "'
//            GROUP BY cp.category_id) AS path");

        $query = "SELECT id, title, (SELECT GROUP_CONCAT(cd1.title ORDER BY level SEPARATOR ' > ') FROM category_path cp LEFT JOIN category_description cd1 
        ON (cp.id = cd1.category_id AND cp.category_id != cp.id) WHERE cp.category_id = c.id AND cd1.language_id = '" . getConfig('config_language_admin') . "' 
        GROUP BY cp.category_id) AS path FROM category c LEFT JOIN category_description cd2 ON (c.id = cd2.category_id) WHERE c.id = '" . (int)$category_id . "' 
        AND cd2.language_id = '" . getConfig('config_language_admin') . "'";
        $data = DB::select($query);
        return (!empty($data)) ? $data[0] : [];
    }
    public function saveData($request)
    {
        $category_id = $request->category['id'];
        if ($category_id > 0) {
            $parent_id = 0;
            if ($request->has('category')) {
                $parent_id = $request->category['parent_id'];
                if ($parent_id == $category_id) {
                    Category::where('id', $category_id)->update(['position' => $request->category['position'], 'status' => $request->category['status'],
                        'image' => $request->category['image']]);
                } else {
                    Category::where('id', $category_id)->update($request->category);
                }
            }
            if ($request->has('descriptions')) {
                CategoryDescription::where('category_id', $category_id)->delete();
                foreach ($request->descriptions as $item) {
                    if (trim($item['title']) != '') {
                        CategoryDescription::where('category_id', $category_id)->create([
                            'category_id' => $category_id, 'language_id' => $item['languageCode'], 'title' => $item['title'], 'description' => $item['description'], 'short_description' => $item['short_description'],
                            'meta_title' => $item['meta_title'], 'meta_description' => $item['meta_description']
                        ]);
                    }
                }
            }
            if ($parent_id != $category_id) {
                $queryPath = CategoryPath::where('id', $category_id)->orderBy('level', 'ASC')->get();
                if (count($queryPath)) {
                    foreach ($queryPath as $result) {
                        CategoryPath::where([['category_id', '=', $result->category_id], ['level', '<', $result->level]])->delete();
                        $path = array();
                        $query = CategoryPath::where('category_id', $parent_id)->orderBy('level', 'ASC')->get();
                        foreach ($query as $item) {
                            $path[] = $item->id;
                        }
                        $query = CategoryPath::where('category_id', $result->category_id)->orderBy('level', 'ASC')->get();
                        foreach ($query as $item) {
                            $path[] = $item->id;
                        }
                        $level = 0;
                        foreach ($path as $path_id) {
                            DB::statement("REPLACE INTO `category_path` SET category_id = '" . (int)$result->category_id . "', `id` = '" . (int)$path_id . "', level = '" . (int)$level . "'");
                            $level++;
                        }
                    }
                } else {
                    CategoryPath::where('category_id', $category_id)->delete();
                    $level = 0;
                    $query = CategoryPath::where('category_id', $parent_id)->orderBy('level', 'ASC')->get();
                    foreach ($query as $item) {
                        CategoryPath::create(['category_id' => (int)$category_id, 'id' => (int)$item->id, 'level' => (int)$level]);
                        $level++;
                    }
                    DB::statement("REPLACE INTO `category_path` SET category_id = '" . (int)$category_id . "', `id` = '" . (int)$category_id . "', level = '" . (int)$level . "'");
                }
            }
            return response()->json([
                'success' => true,
                "message" => "Update success",
            ]);
        } else {
            $parent_id = 0;
            if ($request->has('category')) {
                $data = Category::create($request->category);
                $parent_id = $request->category['parent_id'];
                $category_id = $data->id;
            }
            if ($request->has('descriptions') && $category_id > 0) {
                CategoryDescription::where('category_id', $category_id)->delete();
                foreach ($request->descriptions as $item) {
                    if (trim($item['title']) != '') {
                        CategoryDescription::where('category_id', $category_id)->create([
                            'category_id' => $category_id, 'language_id' => $item['languageCode'], 'title' => $item['title'], 'description' => $item['description'], 'short_description' => $item['short_description'],
                            'meta_title' => $item['meta_title'], 'meta_description' => $item['meta_description']
                        ]);
                    }
                }
            }
            $level = 0;
            $path = CategoryPath::where('category_id', $parent_id)->orderBy('level', 'ASC')->get();
            foreach ($path as $row) {
                CategoryPath::create(['id' => $row->id, 'category_id' => $category_id, 'level' => (int)$level]);
                $level++;
            }
            CategoryPath::create(['id' => $category_id, 'category_id' => $category_id, 'level' => (int)$level]);
            return response()->json([
                'success' => true,
                "message" => "Add success",
            ]);
        }
    }

    public function changeStatus($request)
    {
        $updateBlogCate = Category::where('id', $request->id)->first();
        $updateBlogCate->status = $request->status;
        $updateBlogCate->save();
        return response()->json([
            'success' => true,
            'message' => "Change status success",
        ]);
    }
}
