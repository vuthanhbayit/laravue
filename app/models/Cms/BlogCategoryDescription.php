<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;

class BlogCategoryDescription extends Model
{
    protected $table = 'blog_category_description';

    protected $primaryKey = 'category_id';

    protected $fillable = [
        'category_id', 'language_id', 'title', 'description', 'meta_title', 'meta_description'
    ];
    protected $guarded = [
        'category_id', 'language_id', 'title', 'description', 'meta_title', 'meta_description'
    ];
}
