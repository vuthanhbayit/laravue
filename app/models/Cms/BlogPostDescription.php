<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BlogPostDescription extends Model
{
    protected $table = 'blog_post_description';

    protected $primaryKey = 'blog_post_id';

    protected $fillable = [
        'blog_post_id', 'language_id', 'title', 'description', 'content', 'meta_title', 'meta_description'
    ];
    protected $guarded = [
        'blog_post_id', 'language_id', 'title', 'description', 'content', 'meta_title', 'meta_description'
    ];
}
