<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = 'product_category';
    protected $primaryKey = 'product_id';
    protected $fillable = [
        'product_id', 'category_id'
    ];
    protected $guarded = [
        'product_id', 'category_id'
    ];
}
