<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'setting';
    protected $primaryKey = 'id';
    protected $fillable = [
        'code', 'key', 'value'
    ];
    protected $guarded = [
        'code', 'key', 'value'
    ];

    public function getSetting()
    {
        $data = array();
        $query = Setting::select()->get();
        foreach ($query as $result) {
            if (!$result['serialized']) {
                $data[$result['key']] = $result['value'];
            } else {
                $data[$result['key']] = unserialize($result['value']);
            }
        }
        return $data;
    }
}
