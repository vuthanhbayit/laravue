<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    protected $table = 'product_attribute';

    protected $primaryKey = 'product_id';

    protected $fillable = [
        'product_id', 'attribute_id', 'language_id', 'content'
    ];
    protected $guarded = [
        'product_id', 'attribute_id', 'language_id', 'content'
    ];
}
