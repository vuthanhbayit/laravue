<?php

namespace App\models\Cms;

use Illuminate\Support\Facades\File;
use Illuminate\Database\Eloquent\Model;

class BannerDescription extends Model
{
    protected $table = 'banner_description';

    protected $primaryKey = 'banner_id';

    protected $fillable = [
        'banner_id', 'language_id', 'title', 'content'
    ];
    protected $guarded = [
        'banner_id', 'language_id', 'title', 'content'
    ];
}
