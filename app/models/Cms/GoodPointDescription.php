<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GoodPointDescription extends Model
{
    protected $table = 'good_point_description';

    protected $primaryKey = 'good_point_id';

    protected $fillable = [
        'good_point_id', 'language_id', 'content'
    ];
    protected $guarded = [
        'good_point_id', 'language_id', 'content'
    ];
}
