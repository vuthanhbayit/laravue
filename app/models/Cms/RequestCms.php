<?php

namespace App\models\Cms;

use App\models\Cms\RequestDescription;
use Illuminate\Database\Eloquent\Model;

class RequestCms extends Model
{

    protected $table = 'request';

    protected $fillable = [
        'id', 'status'
    ];
    protected $guarded = [
        'id', 'status'
    ];

    public $timestamps = true;

    public function search($request)
    {
        $query = RequestCms::where(function ($query) use ($request) {
            if (isset($request->title) && $request->title != '') {
                $query->where('request_description.title', 'like', '%' . $request->title . '%');
            }
            if (isset($request->status) && $request->status != -1) {
                $query->where('status', '=', $request->status);
            }
            if (isset($request->languageCode) && $request->languageCode != '') {
                $query->where('request_description.language_id', '=', $request->languageCode);
            } else {
                $query->where('request_description.language_id', '=', getConfig('config_language_admin'));
            }
        })->join('request_description', 'request_description.request_id', '=', 'request.id')
            ->select('request.id AS id', 'request_description.title AS title', 'request.status AS status')
            ->orderBy('id', 'DESC');

        $totalRow = $query->get();

        if (isset($request->pageIndex) && $request->pageIndex > 0) {
            if ($request->pageIndex == 1) {
                $query->offset(0);
            } else {
                $query->offset(($request->pageIndex - 1) * $request->pageSize);
            }
        }
        if ($request->pageSize == null) {
            $data = $query->get();
        } else {
            $data = $query->limit($request->pageSize)->get();
        }

        return response()->json([
            'success' => true,
            'message' => "Search success",
            'data' => $data,
            'totalRow' => $totalRow->count(),
        ]);
    }

    public function detail($request)
    {
        $data = RequestCms::where('id', $request->id)->first();
        $data['titles'] = $this->getRequestDescriptions($request->id);
        return response()->json([
            'success' => true,
            'message' => "Get detail success",
            'data' => $data,
        ]);
    }

    public function getRequestDescriptions($requestId)
    {
        $description = array();
        $query = RequestDescription::where('request_id', $requestId)->get();
        foreach ($query as $i => $item) {
            $description[$i] = array(
                'languageCode' => $item['language_id'],
                'title' => $item['title'],
            );
        }
        return $description;
    }

    public function saveData($request)
    {
        if ($request->id > 0) {
            RequestCms::where('id', $request->id)->update([
                'status' => $request->status
            ]);
            RequestDescription::where('request_id', $request->id)->delete();
            foreach ($request->titles as $item) {
                if (trim($item['title']) != '') {
                    RequestDescription::create([
                        'request_id' => $request->id, 'language_id' => $item['languageCode'], 'title' => $item['title']
                    ]);
                }
            }
            return response()->json([
                'success' => true,
                'message' => "Update success",
            ]);
        } else {
            $data = RequestCms::create([
                'status' => $request->status
            ]);
            $requestId = $data->id;
            RequestDescription::where('request_id', $requestId)->delete();
            foreach ($request->titles as $item) {
                if (trim($item['title']) != '') {
                    RequestDescription::create([
                        'request_id' => $requestId, 'language_id' => $item['languageCode'], 'title' => $item['title']
                    ]);
                }
            }
            return response()->json([
                'success' => true,
                'message' => "Add success",
            ]);
        }
    }

    public function changeStatus($request)
    {
        $updateRequest = RequestCms::where('id', $request->id)->first();
        $updateRequest->status = $request->status;
        $updateRequest->save();
        return response()->json([
            'success' => true,
            'message' => "Change status success",
        ]);
    }

}
