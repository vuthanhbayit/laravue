<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'email', 'remember_token', 'avatar'
    ];
    protected $guarded = [
        'name', 'email', 'remember_token', 'avatar'
    ];
}
