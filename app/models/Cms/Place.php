<?php

namespace App\models\Cms;

use App\models\Cms\PlaceDescription;
use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    protected $table = 'place';

    protected $fillable = [
        'id', 'status'
    ];
    protected $guarded = [
        'id', 'status'
    ];

    public $timestamps = true;

    public function search($request)
    {
        $query = Place::where(function ($query) use ($request) {
            if (isset($request->title) && $request->title != '') {
                $query->where('place_description.title', 'like', '%' . $request->title . '%');
            }
            if (isset($request->status) && $request->status != -1) {
                $query->where('status', '=', $request->status);
            }
            if (isset($request->languageCode) && $request->languageCode != '') {
                $query->where('place_description.language_id', '=', $request->languageCode);
            } else {
                $query->where('place_description.language_id', '=', getConfig('config_language_admin'));
            }
        })->join('place_description', 'place_description.place_id', '=', 'place.id')
            ->select('place.id AS id', 'place_description.title AS title', 'place.status AS status')
            ->orderBy('id', 'DESC');

        $totalRow = $query->get();

        if (isset($request->pageIndex) && $request->pageIndex > 0) {
            if ($request->pageIndex == 1) {
                $query->offset(0);
            } else {
                $query->offset(($request->pageIndex - 1) * $request->pageSize);
            }
        }
        if ($request->pageSize == null) {
            $data = $query->get();
        } else {
            $data = $query->limit($request->pageSize)->get();
        }

        return response()->json([
            'success' => true,
            'message' => "Search success",
            'data' => $data,
            'totalRow' => $totalRow->count(),
        ]);
    }

    public function detail($request)
    {
        $data = Place::where('id', $request->id)->first();
        $data['titles'] = $this->getPlaceDescriptions($request->id);
        return response()->json([
            'success' => true,
            'message' => "Get detail success",
            'data' => $data,
        ]);
    }

    public function getPlaceDescriptions($placeId)
    {
        $description = array();
        $query = PlaceDescription::where('place_id', $placeId)->get();
        foreach ($query as $i => $item) {
            $description[$i] = array(
                'languageCode' => $item['language_id'],
                'title' => $item['title'],
            );
        }
        return $description;
    }

    public function saveData($request)
    {
        if ($request->id > 0) {
            Place::where('id', $request->id)->update([
                'status' => $request->status
            ]);
            PlaceDescription::where('place_id', $request->id)->delete();
            foreach ($request->titles as $item) {
                if (trim($item['title']) != '') {
                    PlaceDescription::create([
                        'place_id' => $request->id, 'language_id' => $item['languageCode'], 'title' => $item['title']
                    ]);
                }
            }
            return response()->json([
                'success' => true,
                'message' => "Update success",
            ]);
        } else {
            $data = Place::create([
                'status' => $request->status
            ]);
            $placeId = $data->id;
            PlaceDescription::where('place_id', $placeId)->delete();
            foreach ($request->titles as $item) {
                if (trim($item['title']) != '') {
                    PlaceDescription::create([
                        'place_id' => $placeId, 'language_id' => $item['languageCode'], 'title' => $item['title']
                    ]);
                }
            }
            return response()->json([
                'success' => true,
                'message' => "Add success",
            ]);
        }
    }

    public function changeStatus($request)
    {
        $updatePlace = Place::where('id', $request->id)->first();
        $updatePlace->status = $request->status;
        $updatePlace->save();
        return response()->json([
            'success' => true,
            'message' => "Change status success",
        ]);
    }

}
