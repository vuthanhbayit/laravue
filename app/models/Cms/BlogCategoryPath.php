<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;

class BlogCategoryPath extends Model
{
    protected $table = 'blog_category_path';

    protected $primaryKey = 'path_id';

    protected $fillable = [
        'category_id', 'path_id', 'level'
    ];
    protected $guarded = [
        'category_id', 'path_id', 'level'
    ];
}
