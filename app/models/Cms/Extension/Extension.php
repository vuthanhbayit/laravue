<?php

namespace App\models\Cms\Extension;

use Illuminate\Database\Eloquent\Model;

class Extension extends Model
{
    protected $table = 'extension';

    protected $primaryKey = 'id';

    protected $fillable = [
        'title', 'code', 'status'
    ];
    protected $guarded = [
        'title', 'code', 'status'
    ];

    public function searchExtension($request)
    {
        $data = array();
        $result = Extension::where(function ($query) use ($request) {
            if (isset($request->title) && $request->title != '') {
                $query->where('title', 'like', '%' . $request->title . '%');
            }
            if (isset($request->code) && $request->code != '') {
                $query->where('code', 'like', '%' . $request->code . '%');
            }
            if (isset($request->status) && $request->status != -1) {
                $query->where('status', '=', $request->status);
            }
        })
            ->select()
            ->get();
        foreach ($result as $i => $item) {
            $data[$i] = $item;
            $data[$i]['module'] = Module::where('code', $item['code'])->get();
        }
        return response()->json([
            'success' => true,
            'message' => "Search success",
            'data' => $data,
            'totalRow' => 0,
        ]);
    }

    public function changeStatus($request)
    {
        $update = Extension::where('id', $request->id)->first();
        $update->status = $request->status;
        $update->save();
        if ($request->status == 0) {
            Module::where('code', $request->code)->update([
                'status' => 0
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => "Change status success",
        ]);
    }
}
