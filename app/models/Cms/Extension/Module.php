<?php

namespace App\models\Cms\Extension;

use App\models\Cms\Product;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table = 'module';

    protected $primaryKey = 'id';

    protected $fillable = [
        'page', 'position', 'sort_order', 'name', 'code', 'setting', 'status'
    ];
    protected $guarded = [
        'page', 'position', 'sort_order', 'name', 'code', 'setting', 'status'
    ];

    public function detail($request)
    {
        $data = Module::where('id', $request->id)->first();
        $data['setting'] = json_decode($data['setting'], true);
        $data['descriptions'] = $this->getModuleDescription($request->id);
        return response()->json([
            'success' => true,
            'message' => "Get detail success",
            'data' => $data,
        ]);
    }

    public function getModuleDescription($moduleId)
    {
        $description = array();
        $query = ModuleDescription::where('module_id', $moduleId)->get();
        if (count($query) > 0) {
            foreach ($query as $i => $item) {
                $description[$i] = array(
                    'languageCode' => $item['language_id'],
                    'title' => $item['title'],
                    'description' => $item['description'],
                    'content' => $item['content'],
                );
            }
        }
        return $description;
    }

    public function saveData($request)
    {
        $moduleId = $request->id;
        if ($moduleId > 0) {
            Module::where('id', $moduleId)->update([
                'name' => $request->name, 'code' => $request->code, 'setting' => json_encode($request->setting), 'page' => $request->page, 'position' => $request->position,
                'sort_order' => $request->sort_order, 'status' => $request->status
            ]);
            ModuleDescription::where('module_id', $moduleId)->delete();
            foreach ($request->descriptions as $item) {
                ModuleDescription::create([
                    'module_id' => $moduleId, 'language_id' => $item['languageCode'], 'title' => $item['title'], 'description' => $item['description'],
                    'content' => $item['content'],

                ]);
            }
            return response()->json([
                'success' => true,
                'message' => "Update success",
            ]);
        } else {
            $data = Module::create([
                'name' => $request->name, 'code' => $request->code, 'setting' => json_encode($request->setting), 'page' => $request->page, 'position' => $request->position,
                'sort_order' => $request->sort_order, 'status' => $request->status
            ]);
            $moduleId = $data->id;
            foreach ($request->descriptions as $item) {
                ModuleDescription::create([
                    'module_id' => $moduleId, 'language_id' => $item['languageCode'], 'title' => $item['title'], 'description' => $item['description'],
                    'content' => $item['content'],
                ]);
            }
            return response()->json([
                'success' => true,
                'message' => "Add success",
            ]);
        }
    }

    public function changeStatus($request)
    {
        $updateArea = Module::where('id', $request->id)->first();
        $updateArea->status = $request->status;
        $updateArea->save();
        return response()->json([
            'success' => true,
            'message' => "Change status success",
        ]);
    }
}
