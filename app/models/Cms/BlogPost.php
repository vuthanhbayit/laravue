<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model
{
    protected $table = 'blog_post';

    protected $primaryKey = 'id';

    protected $fillable = [
        'category_id', 'image', 'viewed', 'status', 'featured'
    ];
    protected $guarded = [
        'category_id', 'image', 'viewed', 'status', 'featured'
    ];

    public $timestamps = true;

    public function search($request)
    {
        $query = BlogPost::where(function ($query) use ($request) {
            if (isset($request->title) && $request->title != '') {
                $query->where('title', 'like', '%' . $request->title . '%');
            }
            if (isset($request->status) && $request->status != -1) {
                $query->where('status', '=', $request->status);
            }
            if (isset($request->languageCode) && $request->languageCode != '') {
                $query->where('blog_post_description.language_id', '=', $request->languageCode);
            } else {
                $query->where('blog_post_description.language_id', '=', getConfig('config_language_admin'));
            }
        })
            ->join('blog_post_description', 'blog_post_description.blog_post_id', '=', 'blog_post.id')
            ->select('blog_post.id AS id', 'blog_post.image AS image',  'blog_post.viewed AS viewed', 'blog_post_description.title AS title', 'blog_post_description.description AS description', 'blog_post.status AS status')
            ->orderBy('id', 'DESC');

        $totalRow = $query->get();

        if (isset($request->pageIndex) && $request->pageIndex > 0) {
            if ($request->pageIndex == 1) {
                $query->offset(0);
            } else {
                $query->offset(($request->pageIndex - 1) * $request->pageSize);
            }
        }

        if ($request->pageSize == null) {
            $data = $query->get();
        } else {
            $data = $query->limit($request->pageSize)->get();
        }
        foreach ($data as $k => $v) {
            $filename = $v->image;
            if ($filename) {
                resizeImage($filename, 'cache/' . $filename);
                $v->image = 'cache/' . $filename;
            }
        }
        return response()->json([
            'success' => true,
            'message' => "Search success",
            'data' => $data,
            'totalRow' => $totalRow->count(),
        ]);
    }

    public function detail($request)
    {
        $data = BlogPost::where('id', $request->id)->first();
        $data['descriptions'] = $this->getBlogPostDescriptions($request->id);
        return response()->json([
            'success' => true,
            'message' => "Get success",
            'data' => $data,
        ]);
    }

    public function getBlogPostDescriptions($blogPostId)
    {
        $description = array();
        $query = BlogPostDescription::where('blog_post_id', $blogPostId)->get();
        foreach ($query as $i => $item) {
            $description[$i] = array(
                'languageCode' => $item['language_id'],
                'title' => $item['title'],
                'description' => $item['description'],
                'content' => $item['content'],
                'meta_title' => $item['meta_title'],
                'meta_description' => $item['meta_description'],
            );
        }
        return $description;
    }

    public function saveData($request)
    {
        $blogPostId = $request->id;
        if ($blogPostId > 0) {
            BlogPost::where('id', $blogPostId)->update(['category_id' => $request->category_id, 'image' => $request->image, 'viewed' => $request->viewed, 'featured' => $request->featured,
                'status' => $request->status]);
            BlogPostDescription::where('blog_post_id', $blogPostId)->delete();
            foreach ($request->descriptions as $item) {
                if (trim($item['title']) != '') {
                    BlogPostDescription::create([
                        'blog_post_id' => $blogPostId, 'language_id' => $item['languageCode'], 'title' => $item['title'], 'description' => $item['description'], 'content' => $item['content'],
                        'meta_title' => $item['meta_title'], 'meta_description' => $item['meta_description']
                    ]);
                }
            }
            return response()->json([
                'success' => true,
                'message' => "Update success",
            ]);
        } else {
            $data = BlogPost::create(['category_id' => $request->category_id, 'image' => $request->image, 'viewed' => $request->viewed, 'featured' => $request->featured,
                'status' => $request->status]);
            $blogPostId = $data->id;
            foreach ($request->descriptions as $item) {
                if (trim($item['title']) != '') {
                    BlogPostDescription::create([
                        'blog_post_id' => $blogPostId, 'language_id' => $item['languageCode'], 'title' => $item['title'], 'description' => $item['description'], 'content' => $item['content'],
                        'meta_title' => $item['meta_title'], 'meta_description' => $item['meta_description']
                    ]);
                }
            }
            return response()->json([
                'success' => true,
                'message' => "Add success",
            ]);
        }
    }

    public function changeStatus($request)
    {
        $updateBlogPost = BlogPost::where('id', $request->id)->first();
        $updateBlogPost->status = $request->status;
        $updateBlogPost->save();
        return response()->json([
            'success' => true,
            'message' => "Change status success",
        ]);
    }
}
