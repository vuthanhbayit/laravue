<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;

class RequestDescription extends Model
{
    protected $table = 'request_description';

    protected $primaryKey = 'request_id';
    protected $fillable = [
        'request_id', 'language_id', 'title'
    ];
    protected $guarded = [
        'request_id', 'language_id', 'title'
    ];
}
