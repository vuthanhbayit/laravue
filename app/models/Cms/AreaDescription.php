<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;

class AreaDescription extends Model
{
    protected $table = 'area_description';

    protected $primaryKey = 'area_id';
    protected $fillable = [
        'area_id', 'language_id', 'title'
    ];
    protected $guarded = [
        'area_id', 'language_id', 'title'
    ];
}
