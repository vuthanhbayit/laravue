<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;

class ProvinceDescription extends Model
{
    protected $table = 'province_description';

    protected $primaryKey = 'province_id';
    protected $fillable = [
        'province_id', 'language_id', 'title'
    ];
    protected $guarded = [
        'province_id', 'language_id', 'title'
    ];
}
