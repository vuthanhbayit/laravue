<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;

class CategoryPath extends Model
{
    protected $table = 'category_path';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id', 'category_id', 'level'
    ];
    protected $guarded = [
        'id', 'category_id', 'level'
    ];
}
