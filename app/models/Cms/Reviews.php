<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;

class Reviews extends Model
{
    protected $table = 'reviews';
    protected $primaryKey = 'id';
    protected $fillable = [
        'image', 'viewed', 'status', 'rating'
    ];
    protected $guarded = [
        'image', 'viewed', 'status', 'rating'
    ];

    public $timestamps = true;

    public function search($request)
    {
        $query = Reviews::where(function ($query) use ($request) {
            if (isset($request->title) && $request->title != '') {
                $query->where('title', 'like', '%' . $request->title . '%');
            }
            if (isset($request->status) && $request->status != -1) {
                $query->where('status', '=', $request->status);
            }
            if (isset($request->languageCode) && $request->languageCode != '') {
                $query->where('reviews_description.language_id', '=', $request->languageCode);
            } else {
                $query->where('reviews_description.language_id', '=', getConfig('config_language_admin'));
            }
        })->join('reviews_description', 'reviews_description.reviews_id', '=', 'reviews.id')
            ->select('reviews.id AS id', 'reviews.image AS image', 'reviews_description.title AS title', 'reviews_description.description AS description', 'reviews.viewed AS viewed', 'reviews.status AS status')
            ->orderBy('id', 'DESC');

        $totalRow = $query->get();

        if (isset($request->pageIndex) && $request->pageIndex > 0) {
            if ($request->pageIndex == 1) {
                $query->offset(0);
            } else {
                $query->offset(($request->pageIndex - 1) * $request->pageSize);
            }
        }

        if ($request->pageSize == null) {
            $data = $query->get();
        } else {
            $data = $query->limit($request->pageSize)->get();
        }
        foreach ($data as $k => $v) {
            $filename = $v->image;
            if ($filename) {
                resizeImage($filename, 'cache/' . $filename);
                $v->image = 'cache/' . $filename;
            }
        }
        return response()->json([
            'success' => true,
            'message' => "Search success",
            'data' => $data,
            'totalRow' => $totalRow->count(),
        ]);
    }

    public function detail($request)
    {
        $data = Reviews::where('id', $request->id)->first();
        $data['descriptions'] = $this->getReviewsDescriptions($request->id);
        return response()->json([
            'success' => true,
            'message' => "Get success",
            'data' => $data,
        ]);
    }

    public function getReviewsDescriptions($reviewsId)
    {
        $description = array();
        $query = ReviewsDescription::where('reviews_id', $reviewsId)->get();
        foreach ($query as $i => $item) {
            $description[$i] = array(
                'languageCode' => $item['language_id'],
                'title' => $item['title'],
                'description' => $item['description'],
                'content' => $item['content'],
                'meta_title' => $item['meta_title'],
                'meta_description' => $item['meta_description'],
            );
        }
        return $description;
    }

    public function saveData($request)
    {
        $reviewsId = $request->id;
        if ($reviewsId > 0) {
            Reviews::where('id', $reviewsId)->update(['image' => $request->image, 'viewed' => $request->viewed, 'status' => $request->status, 'rating' => $request->rating]);
            ReviewsDescription::where('reviews_id', $reviewsId)->delete();
            foreach ($request->descriptions as $item) {
                if (trim($item['title']) != '') {
                    ReviewsDescription::create([
                        'reviews_id' => $reviewsId, 'language_id' => $item['languageCode'], 'title' => $item['title'], 'description' => $item['description'], 'content' => $item['content'],
                        'meta_title' => $item['meta_title'], 'meta_description' => $item['meta_description']
                    ]);
                }
            }
            return response()->json([
                'success' => true,
                'message' => "Update success",
            ]);
        } else {
            $data = Reviews::create(['title' => $request->title, 'image' => $request->image, 'viewed' => $request->viewed, 'status' => $request->status, 'rating' => $request->rating]);
            $reviewsId = $data->id;
            foreach ($request->descriptions as $item) {
                if (trim($item['title']) != '') {
                    ReviewsDescription::create([
                        'reviews_id' => $reviewsId, 'language_id' => $item['languageCode'], 'title' => $item['title'], 'description' => $item['description'], 'content' => $item['content'],
                        'meta_title' => $item['meta_title'], 'meta_description' => $item['meta_description']
                    ]);
                }
            }
            return response()->json([
                'success' => true,
                'message' => "Add success",
            ]);
        }
    }

    public function changeStatus($request)
    {
        $updateReviews = Reviews::where('id', $request->id)->first();
        $updateReviews->status = $request->status;
        $updateReviews->save();
        return response()->json([
            'success' => true,
            'message' => "Change status success",
        ]);
    }
}
