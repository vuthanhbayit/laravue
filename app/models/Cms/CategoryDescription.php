<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;

class CategoryDescription extends Model
{
    protected $table = 'category_description';

    protected $primaryKey = 'category_id';

    protected $fillable = [
        'category_id', 'language_id', 'title', 'short_description', 'description', 'meta_title', 'meta_description'
    ];
    protected $guarded = [
        'category_id', 'language_id', 'title', 'short_description', 'description', 'meta_title', 'meta_description'
    ];

    public function getCategoryDescriptions($category_id)
    {
        $category_description_data = array();
        $query = CategoryDescription::where('category_id', $category_id)->get();
        if(count($query) > 0) {
            foreach ($query as $i => $item) {
                $category_description_data[$i] = array(
                    'languageCode' => $item['language_id'],
                    'title' => $item['title']
                );
            }
        }
        return $category_description_data;
    }
}
