<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;
use DB;

class MenuDescription extends Model
{
    protected $table = 'menu_description';

    protected $primaryKey = 'menu_id';

    protected $fillable = [
        'menu_id', 'language_id', 'title', 'link'
    ];
    protected $guarded = [
        'menu_id', 'language_id', 'title', 'link'
    ];
}
