<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class InformationDescription extends Model
{
    protected $table = 'information_description';

    protected $primaryKey = 'information_id';

    protected $fillable = [
        'information_id', 'language_id', 'title', 'description', 'content', 'meta_title', 'meta_description'
    ];
    protected $guarded = [
        'information_id', 'language_id', 'title', 'description', 'content', 'meta_title', 'meta_description'
    ];
}
