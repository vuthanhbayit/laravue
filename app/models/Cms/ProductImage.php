<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $table = 'product_image';
    protected $primaryKey = 'id';
    protected $fillable = [
        'product_id', 'image', 'position'
    ];
    protected $guarded = [
        'product_id', 'image', 'position'
    ];
}
