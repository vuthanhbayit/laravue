<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;

class Day extends Model
{
    protected $table = 'day';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id', 'position', 'icon', 'status'
    ];
    protected $guarded = [
        'id', 'position', 'icon', 'status'
    ];

    public function search($request)
    {
        $query = Day::where(function ($query) use ($request) {
            if (isset($request->title) && $request->title != '') {
                $query->where('day_description.title', 'like', '%' . $request->title . '%');
            }
            if (isset($request->status) && $request->status != -1) {
                $query->where('day.status', '=', $request->status);
            }
            if (isset($request->languageCode) && $request->languageCode != '') {
                $query->where('day_description.language_id', '=', $request->languageCode);
            } else {
                $query->where('day_description.language_id', '=', getConfig('config_language_admin'));
            }
        })->join('day_description', 'day_description.day_id', '=', 'day.id')
            ->select('day.id AS id', 'day_description.title AS title', 'day.status AS status')
            ->orderBy('id', 'DESC');

        $totalRow = $query->get();

        if (isset($request->pageIndex) && $request->pageIndex > 0) {
            if ($request->pageIndex == 1) {
                $query->offset(0);
            } else {
                $query->offset(($request->pageIndex - 1) * $request->pageSize);
            }
        }
        if ($request->pageSize == null) {
            $data = $query->get();
        } else {
            $data = $query->limit($request->pageSize)->get();
        }

        return response()->json([
            'success' => true,
            'message' => "Search success",
            'data' => $data,
            'totalRow' => $totalRow->count(),
        ]);
    }

    public function detail($request)
    {
        $data = Day::where('id', $request->id)->first();
        $data['titles'] = $this->getDayDescriptions($request->id);
        return response()->json([
            'success' => true,
            'message' => "Get detail success",
            'data' => $data,
        ]);
    }

    public function getDayDescriptions($dayId)
    {
        $description = array();
        $query = DayDescription::where('day_id', $dayId)->get();
        foreach ($query as $i => $item) {
            $description[$i] = array(
                'languageCode' => $item['language_id'],
                'title' => $item['title'],
            );
        }
        return $description;
    }

    public function saveDay($request)
    {
        if ($request->id > 0) {
            Day::where('id', $request->id)->update([
                'position' => $request->position, 'icon' => $request->icon, 'status' => $request->status
            ]);
            DayDescription::where('day_id', $request->id)->delete();
            foreach ($request->titles as $item) {
                if (trim($item['title']) != '') {
                    DayDescription::create([
                        'day_id' => $request->id, 'language_id' => $item['languageCode'], 'title' => $item['title']
                    ]);
                }
            }
            return response()->json([
                'success' => true,
                'message' => "Update success",
            ]);
        } else {
            $data = Day::create([
                'position' => $request->position, 'icon' => $request->icon, 'status' => $request->status
            ]);
            $dayId = $data->id;
            DayDescription::where('day_id', $dayId)->delete();
            foreach ($request->titles as $item) {
                if (trim($item['title']) != '') {
                    DayDescription::create([
                        'day_id' => $dayId, 'language_id' => $item['languageCode'], 'title' => $item['title']
                    ]);
                }
            }
            return response()->json([
                'success' => true,
                'message' => "Add success",
            ]);
        }
    }

    public function changeStatus($request)
    {
        Day::where('id', $request->id)->update(['status' => $request->status]);
        return response()->json([
            'success' => true,
            'message' => "Change status success",
        ]);
    }
}
