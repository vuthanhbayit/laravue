<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $table = 'attribute';

    protected $primaryKey = 'id';

    protected $fillable = [
        'position', 'icon', 'status'
    ];
    protected $guarded = [
        'position', 'icon', 'status'
    ];

    public function search($request)
    {
        $query = Attribute::where(function ($query) use ($request) {
            if (isset($request->title) && $request->title != '') {
                $query->where('attribute_description.title', 'like', '%' . $request->title . '%');
            }
            if (isset($request->status) && $request->status != -1) {
                $query->where('attribute.status', '=', $request->status);
            }
            if (isset($request->languageCode) && $request->languageCode != '') {
                $query->where('attribute_description.language_id', '=', $request->languageCode);
            } else {
                $query->where('attribute_description.language_id', '=', getConfig('config_language_admin'));
            }
        })->join('attribute_description', 'attribute_description.attribute_id', '=', 'attribute.id')
            ->select('attribute.id AS id', 'attribute_description.title AS title', 'attribute.status AS status')
            ->orderBy('id', 'DESC');

        $totalRow = $query->get();

        if (isset($request->pageIndex) && $request->pageIndex > 0) {
            if ($request->pageIndex == 1) {
                $query->offset(0);
            } else {
                $query->offset(($request->pageIndex - 1) * $request->pageSize);
            }
        }
        if ($request->pageSize == null) {
            $data = $query->get();
        } else {
            $data = $query->limit($request->pageSize)->get();
        }

        return response()->json([
            'success' => true,
            'message' => "Search success",
            'data' => $data,
            'totalRow' => $totalRow->count(),
        ]);
    }

    public function detail($request)
    {
        $data = Attribute::where('id', $request->id)->first();
        $data['titles'] = $this->getAttributeDescriptions($request->id);
        return response()->json([
            'success' => true,
            'message' => "Get detail success",
            'data' => $data,
        ]);
    }

    public function getAttributeDescriptions($attributeId)
    {
        $description = array();
        $query = AttributeDescription::where('attribute_id', $attributeId)->get();
        foreach ($query as $i => $item) {
            $description[$i] = array(
                'languageCode' => $item['language_id'],
                'title' => $item['title'],
            );
        }
        return $description;
    }

    public function saveAttribute($request)
    {
        if ($request->id > 0) {
            Attribute::where('id', $request->id)->update([
                'position' => $request->position, 'icon' => $request->icon, 'status' => $request->status
            ]);
            AttributeDescription::where('attribute_id', $request->id)->delete();
            foreach ($request->titles as $item) {
                if (trim($item['title']) != '') {
                    AttributeDescription::create([
                        'attribute_id' => $request->id, 'language_id' => $item['languageCode'], 'title' => $item['title']
                    ]);
                }
            }
            return response()->json([
                'success' => true,
                'message' => "Update success",
            ]);
        } else {
            $data = Attribute::create([
                'position' => $request->position, 'icon' => $request->icon, 'status' => $request->status
            ]);
            $attributeId = $data->id;
            AttributeDescription::where('attribute_id', $attributeId)->delete();
            foreach ($request->titles as $item) {
                if (trim($item['title']) != '') {
                    AttributeDescription::create([
                        'attribute_id' => $attributeId, 'language_id' => $item['languageCode'], 'title' => $item['title']
                    ]);
                }
            }
            return response()->json([
                'success' => true,
                'message' => "Add success",
            ]);
        }
    }

    public function changeStatus($request)
    {
        Attribute::where('id', $request->id)->update(['status' => $request->status]);
        return response()->json([
            'success' => true,
            'message' => "Change status success",
        ]);
    }
}
