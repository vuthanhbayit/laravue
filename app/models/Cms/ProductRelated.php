<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;

class ProductRelated extends Model
{
    protected $table = 'product_related';
    protected $primaryKey = 'related_id';
    protected $fillable = [
        'related_id', 'product_id'
    ];
    protected $guarded = [
        'related_id', 'product_id'
    ];
}
