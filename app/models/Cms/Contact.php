<?php

namespace App\models\Cms;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contact';

    protected $primaryKey = 'id';

    protected $fillable = [
        'fullName', 'email', 'phone', 'address', 'node', 'is_read'
    ];
    protected $guarded = [
        'fullName', 'email', 'phone', 'address', 'node', 'is_read'
    ];
    public function search($request)
    {
        $query = Contact::where(function ($query) use ($request) {
            if (isset($request->fullName) && $request->fullName != '') {
                $query->where('fullName', 'like', '%' . $request->fullName . '%');
            }
            if (isset($request->is_read) && $request->is_read != -1) {
                $query->where('is_read', '=', $request->is_read);
            }
        })
            ->orderBy('id', 'DESC');
        if (isset($request->pageIndex) && $request->pageIndex > 0) {
            if ($request->pageIndex == 1) {
                $query->offset(0);
            } else {
                $query->offset(($request->pageIndex - 1) * $request->pageSize);
            }
        }
        if ($request->pageSize == null) {
            $data = $query->get();
        } else {
            $data = $query->limit($request->pageSize)->get();
        }

        $totalRow = Contact::where(function ($query) use ($request) {
            if (isset($request->fullName) && $request->fullName != '') {
                $query->where('fullName', 'like', '%' . $request->fullName . '%');
            }
            if (isset($request->is_read) && $request->is_read != -1) {
                $query->where('is_read', '=', $request->is_read);
            }
        })
            ->orderBy('id', 'DESC')
            ->get();

        return response()->json([
            'success' => true,
            'message' => "Search success",
            'data' => $data,
            'totalRow' => $totalRow->count(),
        ]);
    }

    public function detail($request)
    {
        $data = Contact::where('id', $request->id)->first();
        return response()->json([
            'success' => true,
            'message' => "Get detail success",
            'data' => $data,
        ]);
    }

    public function readContact($request)
    {
        $updateContact = Contact::where('id', $request->id)->first();
        $updateContact->is_read = $request->is_read;
        $updateContact->save();
        return response()->json([
            'success' => true,
            'message' => "Change status success",
        ]);
    }
}
