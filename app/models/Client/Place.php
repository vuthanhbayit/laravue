<?php

namespace App\models\Client;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class Place extends Model
{
    protected $table = 'place';

    protected $fillable = [
        'id', 'status'
    ];
    protected $guarded = [
        'id', 'status'
    ];

    public $timestamps = true;

    public function getPlace()
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('place_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $data = Cache::get('place_' . app()->getLocale());
            return $data;
        } else {
            $data = DB::table('place')
                ->where([
                    ['place_description.language_id', '=', app()->getLocale()]
                ])
                ->join('place_description', 'place_description.place_id', '=', 'place.id')
                ->select('place.id AS id', 'place_description.title AS title', 'place.status AS status')
                ->orderBy('id', 'DESC')->get();
            if (getConfig('config_debug') == 0) {
                Cache::put('place_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }

}
