<?php

namespace App\models\Client;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Language extends Model
{
    protected $table = 'language';

    protected $fillable = [
        'code', 'name', 'priority', 'flagIcon', 'viName', 'status'
    ];
    protected $guarded = [
        'code', 'name', 'priority', 'flagIcon', 'viName', 'status'
    ];

    public function getLanguages()
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('languages') && getConfig('config_debug') == 0) {
            $value = Cache::get('languages');
            return $value;
        } else {
            $query = Language::where('status', 1)->get();
            $data = [];
            foreach ($query as $item) {
                $data[$item->code] = $item;
            }
            if (getConfig('config_debug') == 0) {
                Cache::put('languages', $data, $expiresAt);
            }
            return $data;
        }
    }
}
