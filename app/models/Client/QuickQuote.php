<?php

namespace App\models\Client;

use Illuminate\Database\Eloquent\Model;

class QuickQuote extends Model
{
    protected $table = 'quick_quotes';

    protected $fillable = [
        'id',
        'expected_departure_date',
        'expected_number_of_days',
        'departure_from_id',
        'number_of_adults',
        'number_of_children',
        'number_of_babies',
        'criterias',
        'provinces',
        'places',
        'requests',
        'contact_id',
        'special_requirements',
        'status',
    ];
    protected $guarded = [
        'id',
        'expected_departure_date',
        'expected_number_of_days',
        'departure_from_id',
        'number_of_adults',
        'number_of_children',
        'number_of_babies',
        'criterias',
        'provinces',
        'places',
        'requests',
        'contact_id',
        'special_requirements',
        'status',
    ];

    public $timestamps = true;
}
