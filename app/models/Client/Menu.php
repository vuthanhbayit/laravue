<?php

namespace App\models\Client;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Menu extends Model
{
    protected $table = 'menu';

    protected $primaryKey = 'menu_id';

    protected $fillable = [
        'parent_id', 'item_id', 'title', 'link', 'position', 'type', 'status', 'group_id'
    ];
    protected $guarded = [
        'parent_id', 'item_id', 'title', 'link', 'position', 'type', 'status', 'group_id'
    ];
    private $children;

    public function getChilds($id = null, $menu_group_id = 0)
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('childMenu_' . $menu_group_id . '_' . $id . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $value = Cache::get('childMenu_' . $menu_group_id . '_' . $id . '_' . app()->getLocale());
            return $value;
        } else {
            $data = Menu::where('status', 1)
                ->where(function ($query) use ($id) {
                    if ($id != null) {
                        return $query->where('parent_id', '=', (int)$id);
                    }
                })->where(function ($query) use ($menu_group_id) {
                    if ($menu_group_id != 0) {
                        return $query->where('group_id', '=', $menu_group_id);
                    }
                })
                ->where('menu_description.language_id', app()->getLocale())
                ->join('menu_description', 'menu_description.menu_id', '=', 'menu.menu_id')
                ->select('menu.*', 'menu_description.title', 'menu_description.link')
                ->orderBy('position', 'ASC')
                ->get();
            if (getConfig('config_debug') == 0) {
                Cache::put('childMenu_' . $menu_group_id . '_' . $id . '_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }

    public function hasChild($id, $menu_group_id = 0)
    {
        return isset($this->children[$id . '_' . $menu_group_id]);
    }

    public function getNodes($id, $menu_group_id = 0)
    {
        return $this->children[$id . '_' . $menu_group_id];
    }

    public function getTree($parent = 1, $menu_group_id = 0, $logo = '')
    {
        $childs = $this->getChilds(null, $menu_group_id);
        foreach ($childs as $child) {
            $this->children[$child['parent_id'] . '_' . $menu_group_id][] = $child;
        }
        if ($logo != '') {
            $htmlLogo = '<h1 class="logo"><a href="/">
                            <img src="/' . $logo . '" title="' . config('config_name') . '" alt="" class="img-responsive"/></a>
                         </h1>';
        } else {
            $htmlLogo = '<h1 class="logo"><a href="/">' . config('config_name') . '</a></h1>';
        }
        $output = $htmlLogo . '<nav class="big-menu-sakura">';
        if ($this->hasChild($parent, $menu_group_id)) {
            $data = $this->getNodes($parent, $menu_group_id);
            $output .= '<ul class="list-menu">';
            foreach ($data as $menu) {
                if ($this->hasChild($menu['menu_id'], $menu_group_id)) {
                    $output .= '<li class="item-menu dropdown"><div class="wrap-menu">';
                    $output .= '<a class="text heading-3" href="' . $this->getLink($menu) . '">';
                    $output .= '<span class="menu-title">' . $menu['title'] . "</span>";
                    $output .= '</a>';
                    $output .= '<i class="fas fa-angle-down"></i>';
                    if ($menu['menu_id'] > 1) {
                        $output .= $this->genTree($menu['menu_id'], 1, $menu, $menu_group_id);
                    }
                    $output .= '</div></li>';
                } else {
                    $output .= '<li class="item-menu">';
                    $output .= '<a class="text heading-3" href="' . $this->getLink($menu) . '">';
                    $output .= '<span class="menu-title">' . $menu['title'] . "</span>";
                    $output .= '</a></li>';
                }
            }
            $output .= '</ul>
                    </nav>
                    <div class="bar-toggle-menu-mobile d-lg-none"><i class="fas fa-bars"></i></div>';
        }
        return $output;
    }

    public function genTree($parentId, $level, $menu, $menu_group_id = 0)
    {
        if ($this->hasChild($parentId, $menu_group_id)) {
            $data = $this->getNodes($parentId, $menu_group_id);
            $output = '<div class="wp-list-menu-child-sakura level-' . $level . '"><div class="wp-menu-level-' . $level . '">';
            $row = '<ul class="list-menu-child-sakura level-' . $level . '">';
            foreach ($data as $item) {
                if ($level == 1) {
                }
                $row .= $this->renderMenuContent($item, $level + 1, $menu_group_id);
            }
            $row .= '</ul></div></div>';
            $output .= $row;
            return $output;
        }
        return;
    }

    public function renderMenuContent($menu, $level, $menu_group_id = 0)
    {
        $output = '';
        if ($this->hasChild($menu['menu_id'], $menu_group_id)) {
            $output .= '<li class="sub-menu level-' . $level . '">';
            $output .= '<a class="text heading-3" href="' . $this->getLink($menu) . '">';
            $output .= '<span class="menu-title">' . $menu['title'] . '</span>';
            $output .= '</a><i class="fa fa-angle-right float-right"></i>';
            if ($menu['menu_id'] > 1) {
                $output .= $this->genTree($menu['menu_id'], $level, $menu, $menu_group_id);
            }
            $output .= '</li>';
        } else {
            $output .= '<li class="">';
            $output .= '<a class="text heading-3" href="' . $this->getLink($menu) . '">';
            $output .= '<span class="menu-title">' . $menu['title'] . "</span>";
            $output .= '</a>';
            $output .= '</li>';
        }
        return $output;
    }

    public function getLink($menu)
    {
        $id = (int)$menu['item_id'];
        switch ($menu['type']) {
            case 'category':
                return '/c' . $id;
            case 'product':
                return '/p' . $id;
            case 'information':
                return '/i' . $id;
            case 'blogCategory':
                return '/bc' . $id;
            default:
                return $menu['link'];
        }
    }

}
