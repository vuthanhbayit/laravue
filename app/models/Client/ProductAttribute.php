<?php

namespace App\models\Client;

use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    protected $table = 'product_attribute';

    protected $primaryKey = 'product_id';

    protected $fillable = [
        'attribute_id', 'language_id', 'content'
    ];
    protected $guarded = [
        'attribute_id', 'language_id', 'content'
    ];
}
