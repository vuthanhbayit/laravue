<?php

namespace App\models\Client;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class RequestClient extends Model
{
    protected $table = 'request';

    protected $fillable = [
        'id', 'status'
    ];
    protected $guarded = [
        'id', 'status'
    ];

    public $timestamps = true;

    public function getRequest()
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('request_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $data = Cache::get('request_' . app()->getLocale());
            return $data;
        } else {
            $data = DB::table('request')
                ->where([
                    ['request_description.language_id', '=', app()->getLocale()]
                ])
                ->join('request_description', 'request_description.request_id', '=', 'request.id')
                ->select('request.id AS id', 'request_description.title AS title', 'request.status AS status')
                ->orderBy('id', 'DESC')
                ->get();
            if (getConfig('config_debug') == 0) {
                Cache::put('request_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }

}
