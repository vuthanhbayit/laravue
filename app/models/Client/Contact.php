<?php

namespace App\models\Client;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contact';

    protected $primaryKey = 'id';

    protected $fillable = [
        'fullName', 'email', 'phone', 'address', 'node', 'is_read'
    ];
    protected $guarded = [
        'fullName', 'email', 'phone', 'address', 'node', 'is_read'
    ];

}
