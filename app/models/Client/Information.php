<?php

namespace App\models\Client;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class Information extends Model
{
    protected $table = 'information';

    protected $primaryKey = 'id';

    protected $fillable = [
        'banner_group_id', 'status'
    ];
    protected $guarded = [
        'banner_group_id', 'status'
    ];

    public function getInformation($informationId = 0)
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('information_' . $informationId . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $data = Cache::get('information_' . $informationId . '_' . app()->getLocale());
            return $data;
        } else {
            $data = array();
            $result = Information::where([
                ['information.id', '=', $informationId],
                ['information.status', '=', 1],
                ['information_description.language_id', '=', app()->getLocale()]
            ])
                ->join('information_description', 'information_description.information_id', '=', 'information.id')
                ->first();
            $banner_group_id = $result['banner_group_id'];
            $banner = new BannerGroup();
            $data['information'] = $result;
            $data['banner'] = $banner->getBanner('information', $banner_group_id);
            if (getConfig('config_debug') == 0) {
                Cache::put('information_' . $informationId . '_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }
}
