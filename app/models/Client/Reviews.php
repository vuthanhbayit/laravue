<?php

namespace App\models\Client;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Reviews extends Model
{
    protected $table = 'reviews';

    protected $primaryKey = 'id';

    protected $fillable = [
        'image', 'title', 'description', 'content', 'viewed', 'status', 'rating'
    ];
    protected $guarded = [
        'image', 'title', 'description', 'content', 'viewed', 'status', 'rating'
    ];

    public function getReviews($limit = 10)
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('reviews_' . $limit . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $data = Cache::get('reviews_' . $limit . '_' . app()->getLocale());
            return $data;
        } else {
            $data = Reviews::where([
                ['reviews.status', '=', 1],
                ['reviews_description.language_id', '=', app()->getLocale()]
            ])->join('reviews_description', 'reviews_description.reviews_id', '=', 'reviews.id')
                ->select('reviews.*', 'reviews_description.title AS title', 'reviews_description.description AS description', 'reviews_description.content AS content',
                    'reviews_description.meta_title AS meta_title', 'reviews_description.meta_description AS meta_description')
                ->orderBy('id', 'DESC')
                ->paginate($limit);
            if (getConfig('config_debug') == 0) {
                Cache::put('reviews_' . $limit . '_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }

    public function getDetailReviews($reviewsId = 0)
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('detailReviews_' . $reviewsId . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $data = Cache::get('detailReviews_' . $reviewsId . '_' . app()->getLocale());
            return $data;
        } else {
            $data = Reviews::where([
                ['reviews.id', '=', $reviewsId],
                ['reviews.status', '=', 1],
                ['reviews_description.language_id', '=', app()->getLocale()]
            ])
                ->join('reviews_description', 'reviews_description.reviews_id', '=', 'reviews.id')
                ->select('reviews.*', 'reviews_description.title AS title', 'reviews_description.description AS description', 'reviews_description.content AS content',
                    'reviews_description.meta_title AS meta_title', 'reviews_description.meta_description AS meta_description')
                ->first();
            if (getConfig('config_debug') == 0) {
                Cache::put('detailReviews_' . $reviewsId . '_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }
}
