<?php

namespace App\models\Client;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class LanguageStaticByLang extends Model
{
    protected $table = 'language_static_by_lang';
    protected $fillable = [
        'languageStaticKey', 'languageCode', 'languageValue'
    ];
    protected $guarded = [
        'languageStaticKey', 'languageCode', 'languageValue'
    ];

    public function getLanguageValue($key, $code)
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('languageValue_' . $key . '_' . $code) && getConfig('config_debug') == 0) {
            $value = Cache::get('languageValue_' . $key . '_' . $code);
            return $value;
        } else {
            $result = LanguageStaticByLang::where([
                ['languageStaticKey', '=', $key],
                ['languageCode', '=', $code]
            ])->first();
            if ($result) {
                $value = $result['languageValue'];
            } else {
                $value = $key;
            }
            if (getConfig('config_debug') == 0) {
                Cache::put('languageValue_' . $key . '_' . $code, $value, $expiresAt);
            }
            return $value;
        }
    }
}
