<?php

namespace App\models\Client;

use Illuminate\Database\Eloquent\Model;

class ProductDay extends Model
{
    protected $table = 'product_day';
    protected $primaryKey = 'day_id';
    protected $fillable = [
        'product_id', 'language_id', 'content'
    ];
    protected $guarded = [
        'product_id', 'language_id', 'content'
    ];
}
