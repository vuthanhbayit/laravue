<?php

namespace App\models\Client;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = 'banner';

    protected $primaryKey = 'id';

    protected $fillable = [
        'banner_group_id', 'link', 'image', 'status'
    ];
    protected $guarded = [
        'banner_group_id', 'link', 'image', 'status'
    ];

}
