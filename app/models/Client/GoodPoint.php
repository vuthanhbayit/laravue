<?php

namespace App\models\Client;

use Illuminate\Database\Eloquent\Model;

class GoodPoint extends Model
{
    protected $table = 'good_point';

    protected $primaryKey = 'id';

    protected $fillable = [
        'category_id', 'icon', 'position', 'status'
    ];
    protected $guarded = [
        'category_id', 'icon', 'position', 'status'
    ];

}
