<?php

namespace App\models\Client;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class BlogCategory extends Model
{
    protected $table = 'blog_category';

    protected $primaryKey = 'id';

    protected $fillable = [
        'parent_id', 'image', 'status'
    ];
    protected $guarded = [
        'parent_id', 'image', 'status'
    ];

    public function getCategoryToParent($parent_id)
    {
        $data = array();
        $result = BlogCategory::where([
            ['parent_id', '=', $parent_id],
            ['status', '=', 1]
        ])->select('id')
            ->get();
        foreach ($result as $item) {
            $data[] = $item['id'];
        }
        $data[] = (int)$parent_id;
        return $data;
    }

    public function getBlogCategoryPost($categoryId = 0, $limit = 10)
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('blogCategoryPost_' . $categoryId . '_' . $limit . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $value = Cache::get('blogCategoryPost_' . $categoryId . '_' . $limit . '_' . app()->getLocale());
            return $value;
        } else {
            $arrParentId = $this->getCategoryToParent($categoryId);
            $data = BlogPost::where([
                ['blog_post.status', '=', 1],
                ['blog_post_description.language_id', '=', app()->getLocale()]
            ])
                ->whereIn('blog_post.category_id', $arrParentId)
                ->join('blog_post_description', 'blog_post_description.blog_post_id', '=', 'blog_post.id')
                ->select('blog_post.id AS id', 'blog_post_description.title AS title', 'blog_post.image AS image', 'blog_post_description.description AS description', 'blog_post_description.content AS content',
                    'blog_post_description.meta_title AS meta_description', 'blog_post_description.created_at AS created_at')
                ->orderBy('blog_post.id', 'DESC')
                ->paginate($limit);
            if (getConfig('config_debug') == 0) {
                Cache::put('blogCategoryPost_' . $categoryId . '_' . $limit . '_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }

    public function getBlogCategory($blogCategoryId = 0)
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('blogCategory_' . $blogCategoryId . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $value = Cache::get('blogCategory_' . $blogCategoryId . '_' . app()->getLocale());
            return $value;
        } else {
            $data = BlogCategory::where([
                ['blog_category.id', '=', $blogCategoryId],
                ['blog_category.status', '=', 1],
                ['blog_category_description.language_id', '=', app()->getLocale()]
            ])->join('blog_category_description', 'blog_category_description.category_id', '=', 'blog_category.id')
                ->select('blog_category.id AS id', 'blog_category.banner_group_id AS banner_group_id', 'blog_category.image AS image', 'blog_category.icon AS icon',
                    'blog_category_description.title AS title', 'blog_category_description.description AS description', 'blog_category_description.meta_title AS meta_title',
                    'blog_category_description.meta_description AS meta_description')
                ->first();
            if (getConfig('config_debug') == 0) {
                Cache::put('blogCategory_' . $blogCategoryId . '_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }

    public function getBlogSubCategory($blogCategoryId = 0)
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('blogSubCategory_' . $blogCategoryId . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $value = Cache::get('blogSubCategory_' . $blogCategoryId . '_' . app()->getLocale());
            return $value;
        } else {
            $data = BlogCategory::where([
                ['blog_category.parent_id', '=', $blogCategoryId],
                ['blog_category.status', '=', 1],
                ['blog_category_description.language_id', '=', app()->getLocale()]
            ])->join('blog_category_description', 'blog_category_description.category_id', '=', 'blog_category.id')
                ->select('blog_category.id AS id', 'blog_category.banner_group_id AS banner_group_id', 'blog_category.image AS image', 'blog_category.icon AS icon', 'blog_category_description.title AS title',
                    'blog_category_description.description AS description')
                ->get();
            if (getConfig('config_debug') == 0) {
                Cache::put('blogSubCategory_' . $blogCategoryId . '_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }
}
