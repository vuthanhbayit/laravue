<?php

namespace App\models\Client;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = 'area';
    protected $fillable = [
        'id', 'status'
    ];
    protected $guarded = [
        'id', 'status'
    ];
}