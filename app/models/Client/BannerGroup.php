<?php

namespace App\models\Client;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class BannerGroup extends Model
{
    protected $table = 'banner_group';
    protected $minutes = 60;

    public function getBanner($page, $bannerId = 0)
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('banner_' . $page . '_' . $bannerId . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $value = Cache::get('banner_' . $page . '_' . $bannerId . '_' . app()->getLocale());
            return $value;
        } else {
            if ($page == 'home' || $page == 'reviews' || $page == 'pageStatic') {
                $data = BannerGroup::where([
                    ['page', 'like', '%' . $page . '%'],
                    ['banner_group.status', '=', 1],
                    ['banner.status', '=', 1],
                    ['banner_description.language_id', '=', app()->getLocale()]
                ])
                    ->join('banner', 'banner.banner_group_id', '=', 'banner_group.id')
                    ->join('banner_description', 'banner_description.banner_id', '=', 'banner.id')
                    ->select('banner.id AS id', 'banner_description.title AS title', 'banner.link AS link', 'banner.image AS image', 'banner_description.content AS content', 'banner_group.type AS type')
                    ->orderBy('banner.id', 'ASC')
                    ->get();
            } else {
                $data = BannerGroup::where([
                    ['page', 'like', '%' . $page . '%'],
                    ['banner_group.status', '=', 1],
                    ['banner.status', '=', 1],
                    ['banner_group.id', '=', $bannerId],
                    ['banner_description.language_id', '=', app()->getLocale()]
                ])
                    ->join('banner', 'banner.banner_group_id', '=', 'banner_group.id')
                    ->join('banner_description', 'banner_description.banner_id', '=', 'banner.id')
                    ->select('banner.id AS id', 'banner_description.title AS title', 'banner.link AS link', 'banner.image AS image', 'banner_description.content AS content', 'banner_group.type AS type')
                    ->orderBy('banner.id', 'ASC')
                    ->get();
            }
            if (getConfig('config_debug') == 0) {
                Cache::put('banner_' . $page . '_' . $bannerId . '_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }
}
