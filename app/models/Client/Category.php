<?php

namespace App\models\Client;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class Category extends Model
{
    protected $table = 'category';

    protected $primaryKey = 'id';

    protected $fillable = [
        'parent_id', 'banner_group_id', 'position', 'status', 'image', 'icon'
    ];
    protected $guarded = [
        'parent_id', 'banner_group_id', 'position', 'status', 'image', 'icon'
    ];

    public function getCategories($limit = 0, $parentId = 0)
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('categories_' . $limit . '_' . $parentId . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $data = Cache::get('categories_' . $limit . '_' . $parentId . '_' . app()->getLocale());
            return $data;
        } else {
            $query = Category::select('category.id AS id', 'category.icon AS icon', 'category.image AS image', 'category_description.title AS title',
                'category_description.description AS description', 'category_description.short_description AS short_description')
                ->join('category_description', 'category.id', '=', 'category_description.category_id')
                ->where([
                    ['category.parent_id', '=', $parentId],
                    ['category.status', '=', 1],
                    ['category_description.language_id', '=', app()->getLocale()],
                ]);
            if ($limit > 0) {
                $query->limit($limit);
            }
            $data = $query->orderBy('position', 'DESC')->get();
            if (getConfig('config_debug') == 0) {
                Cache::put('categories_' . $limit . '_' . $parentId . '_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }

    public function getCategoryProduct($categoryId = 0, $limit = 10)
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('categoryProduct_' . $categoryId . '_' . $limit . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $data = Cache::get('categoryProduct_' . $categoryId . '_' . $limit . '_' . app()->getLocale());
            return $data;
        } else {
            $data = Product::where([
                ['product_category.category_id', '=', $categoryId],
                ['product.status', '=', 1],
                ['product_description.language_id', '=', app()->getLocale()]
            ])
                ->join('product_category', 'product.id', '=', 'product_category.product_id')
                ->join('product_description', 'product_description.product_id', '=', 'product.id')
                ->select('product.id AS id', 'product_description.title AS title', 'product.code AS code', 'product_description.dayNumber AS dayNumber', 'product_description.departAddress AS departAddress',
                    'product.seatsExist AS seatsExist', 'product.image AS image', 'product.price AS price', 'product_description.description AS description', 'product_description.content AS content')
                ->orderBy('product.id', 'DESC')
                ->paginate($limit);
            if (getConfig('config_debug') == 0) {
                Cache::put('categoryProduct_' . $categoryId . '_' . $limit . '_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }

    public function getCategory($categoryId)
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('detailCategory_' . $categoryId . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $data = Cache::get('detailCategory_' . $categoryId . '_' . app()->getLocale());
            return $data;
        } else {
            $data = Category::where([
                ['category.id', '=', $categoryId],
                ['category.status', '=', 1],
                ['category_description.language_id', '=', app()->getLocale()]
            ])->join('category_description', 'category_description.category_id', '=', 'category.id')
                ->select('category.id AS id', 'category.banner_group_id AS banner_group_id', 'category.image AS image', 'category.icon AS icon', 'category_description.title AS title',
                    'category_description.description AS description', 'category_description.short_description AS short_description', 'category_description.meta_title AS meta_title',
                    'category_description.meta_description AS meta_description')
                ->first();
            if (getConfig('config_debug') == 0) {
                Cache::put('detailCategory_' . $categoryId . '_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }

    public function getSubCategory($categoryId = 0)
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('subCategory_' . $categoryId . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $data = Cache::get('subCategory_' . $categoryId . '_' . app()->getLocale());
            return $data;
        } else {
            $data = [];
            if ($categoryId > 0) {
                $data = Category::where([
                    ['parent_id', '=', $categoryId],
                    ['status', '=', 1],
                    ['category_description.language_id', '=', app()->getLocale()]
                ])->join('category_description', 'category_description.category_id', '=', 'category.id')
                    ->select('category.id AS id', 'category.banner_group_id AS banner_group_id', 'category.image AS image', 'category.icon AS icon', 'category_description.title AS title',
                        'category_description.description AS description', 'category_description.short_description AS short_description')
                    ->get();
            }
            if (getConfig('config_debug') == 0) {
                Cache::put('subCategory_' . $categoryId . '_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }

    public function getGoodPoint($categoryId = 0)
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('goodPoint_' . $categoryId . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $data = Cache::get('goodPoint_' . $categoryId . '_' . app()->getLocale());
            return $data;
        } else {
            $data = [];
            if ($categoryId > 0) {
                $data = GoodPoint::where([
                    ['good_point.category_id', '=', $categoryId],
                    ['good_point.status', '=', 1],
                    ['good_point_description.language_id', '=', app()->getLocale()]
                ])->join('good_point_description', 'good_point_description.good_point_id', '=', 'good_point.id')
                    ->select('good_point.id AS id', 'good_point.icon AS icon', 'good_point_description.content AS content')
                    ->orderBy('position', 'ASC')
                    ->get();
            }
            if (getConfig('config_debug') == 0) {
                Cache::put('goodPoint_' . $categoryId . '_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }
}
