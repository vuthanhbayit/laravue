<?php

namespace App\models\Client;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class MenuGroup extends Model
{
    protected $table = 'menu_group';

    protected $primaryKey = 'id';

    protected $fillable = [
        'title', 'position', 'status'
    ];
    protected $guarded = [
        'title', 'position', 'status'
    ];

    public function getMenuPosition($position = 'top')
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('menuPosition_' . $position . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $data = Cache::get('menuPosition_' . $position . '_' . app()->getLocale());
            return $data;
        } else {
            $data = MenuGroup::where([
                ['position', '=', $position],
                ['status', '=', 1],
            ])->select('id')
                ->get();
            if (getConfig('config_debug') == 0) {
                Cache::put('menuPosition_' . $position . '_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }
}
