<?php

namespace App\models\Client;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class BlogPost extends Model
{
    protected $table = 'blog_post';

    protected $primaryKey = 'id';

    protected $fillable = [
        'category_id', 'image', 'viewed', 'status', 'featured'
    ];
    protected $guarded = [
        'category_id', 'image', 'viewed', 'status', 'featured'
    ];

    public function getBlogPost($blogPostId = 0)
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('blogPost_' . $blogPostId . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $data = Cache::get('blogPost_' . $blogPostId . '_' . app()->getLocale());
            return $data;
        } else {
            $data = BlogPost::where([
                ['blog_post.id', '=', $blogPostId],
                ['blog_post.status', '=', 1],
                ['blog_post_description.language_id', '=', app()->getLocale()]
            ])
                ->join('blog_post_description', 'blog_post_description.blog_post_id', '=', 'blog_post.id')
                ->first();
            if (getConfig('config_debug') == 0) {
                Cache::put('blogPost_' . $blogPostId . '_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }

    public function getBlogPosts($limit = 0, $categoryId = 0)
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('blogPosts_' . $limit . '_' . $categoryId . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $data = Cache::get('blogPosts_' . $limit . '_' . $categoryId . '_' . app()->getLocale());
            return $data;
        } else {
            $query = BlogPost::select('blog_post.id AS id', 'blog_post.image AS image', 'blog_post_description.title AS title',
                'blog_post_description.description AS description', 'blog_post_description.content AS content',
                'blog_post_description.meta_title AS meta_title', 'blog_post_description.meta_description AS meta_description')
                ->join('blog_post_description', 'blog_post_description.blog_post_id', '=', 'blog_post.id')
                ->where([
                    ['blog_post.category_id', '=', $categoryId],
                    ['blog_post.status', '=', 1],
                    ['blog_post_description.language_id', '=', app()->getLocale()],
                ]);
            if ($limit > 0) {
                $query->limit($limit);
            }
            $data = $query->orderBy('id', 'DESC')->get();

            if (getConfig('config_debug') == 0) {
                Cache::put('blogPosts_' . $limit . '_' . $categoryId . '_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }
}
