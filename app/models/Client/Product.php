<?php

namespace App\models\Client;

use App\models\Cms\ProductImage;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Product extends Model
{
    protected $table = 'product';
    protected $primaryKey = 'id';
    protected $fillable = [
        'code', 'dayNumber', 'departAddress', 'departsTime', 'seatsExist', 'price', 'image', 'title', 'description', 'content', 'viewed', 'status'
    ];
    protected $guarded = [
        'code', 'dayNumber', 'departAddress', 'departsTime', 'seatsExist', 'price', 'image', 'title', 'description', 'content', 'viewed', 'status'
    ];

    public function getProduct($productId = 0)
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('product_' . $productId . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $data = Cache::get('product_' . $productId . '_' . app()->getLocale());
            return $data;
        } else {
            $data = Product::where([
                ['product.status', '=', 1],
                ['product.id', '=', $productId],
                ['product_description.language_id', '=', app()->getLocale()]
            ])->join('product_description', 'product_description.product_id', '=', 'product.id')
                ->first();
            if (getConfig('config_debug') == 0) {
                Cache::put('product_' . $productId . '_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }

    public function getProducts($limit = 0)
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('products_' . $limit . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $data = Cache::get('products_' . $limit . '_' . app()->getLocale());
            return $data;
        } else {
            $data = Product::where([
                ['product.status', '=', 1],
                ['product_description.language_id', '=', app()->getLocale()]
            ])->join('product_description', 'product_description.product_id', '=', 'product.id')
                ->orderBy('id', 'DESC')
                ->limit($limit)
                ->get();
            if (getConfig('config_debug') == 0) {
                Cache::put('products_' . $limit . '_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }

    public function getProductAttribute($product_id = 0)
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('productAttribute_' . $product_id . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $data = Cache::get('productAttribute_' . $product_id . '_' . app()->getLocale());
            return $data;
        } else {
            $data = ProductAttribute::select('product_attribute.product_id AS product_id', 'attribute_description.title AS title', 'product_attribute.attribute_id AS attribute_id', 'attribute.icon AS icon',
                'product_attribute.content AS content', 'attribute.position AS position', 'attribute.status AS status')
                ->join('attribute', 'attribute.id', '=', 'product_attribute.attribute_id')
                ->join('attribute_description', 'attribute_description.attribute_id', '=', 'attribute.id')
                ->where([
                    ['product_attribute.product_id', '=', $product_id],
                    ['attribute.status', '=', 1],
                    ['attribute_description.language_id', '=', app()->getLocale()],
                    ['product_attribute.language_id', '=', app()->getLocale()]
                ])
                ->get();
            if (getConfig('config_debug') == 0) {
                Cache::put('productAttribute_' . $product_id . '_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }

    public function getProductImage($product_id)
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('productImage_' . $product_id) && getConfig('config_debug') == 0) {
            $data = Cache::get('productImage_' . $product_id);
            return $data;
        } else {
            $data = ProductImage::select('product_image.id as id', 'product_image.image as image', 'product_image.position as position')
                ->join('product', 'product.id', '=', 'product_image.product_id')
                ->where([
                    ['product_id', '=', $product_id],
                    ['product.status', '=', 1]
                ])
                ->orderBy('position', 'ASC')
                ->get();
            if (getConfig('config_debug') == 0) {
                Cache::put('productImage_' . $product_id, $data, $expiresAt);
            }
            return $data;
        }
    }

    public function getProductDay($product_id = 0)
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('productDay_' . $product_id . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $data = Cache::get('productDay_' . $product_id . '_' . app()->getLocale());
            return $data;
        } else {
            $data = ProductDay::select('product_day.day_id as day_id', 'day_description.title as title', 'product_day.content as content')
                ->join('day', 'day.id', '=', 'product_day.day_id')
                ->join('day_description', 'day_description.day_id', '=', 'day.id')
                ->where([
                    ['product_day.product_id', '=', $product_id],
                    ['day.status', '=', 1],
                    ['day_description.language_id', '=', app()->getLocale()],
                    ['product_day.language_id', '=', app()->getLocale()]
                ])
                ->orderBy('day.position', 'ASC')
                ->get();
            if (getConfig('config_debug') == 0) {
                Cache::put('productDay_' . $product_id . '_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }

    public function getProductRelated($product_id = 0)
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('productRelated_' . $product_id . '_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $data = Cache::get('productRelated_' . $product_id . '_' . app()->getLocale());
            return $data;
        } else {
            $data = Product::select('product_related.related_id AS related_id', 'product_related.product_id AS product_id', 'product_description.title AS title', 'product.image AS image',
                'product_description.dayNumber AS dayNumber', 'product_description.departAddress AS departAddress', 'product.departTime AS departTime', 'product.price AS price')
                ->join('product_related', 'product_related.related_id', '=', 'product.id')
                ->join('product_description', 'product_description.product_id', '=', 'product.id')
                ->where([
                    ['product_related.product_id', '=', $product_id],
                    ['product.status', '=', 1],
                    ['product_description.language_id', '=', app()->getLocale()]
                ])
                ->get();
            if (getConfig('config_debug') == 0) {
                Cache::put('productRelated_' . $product_id . '_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }
}
