<?php

namespace App\models\Client\Extension;

use Illuminate\Database\Eloquent\Model;

class ModuleDescription extends Model
{
    protected $table = 'module_description';

    protected $primaryKey = 'module_id';

    protected $fillable = [
        'module_id', 'language_id', 'title', 'description', 'content'
    ];
    protected $guarded = [
        'module_id', 'language_id', 'title', 'description', 'content'
    ];
}
