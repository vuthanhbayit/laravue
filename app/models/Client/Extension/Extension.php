<?php

namespace App\models\Client\Extension;

use Illuminate\Database\Eloquent\Model;

class Extension extends Model
{
    protected $table = 'extension';

    protected $primaryKey = 'id';

    protected $fillable = [
        'title', 'code', 'status'
    ];
    protected $guarded = [
        'title', 'code', 'status'
    ];
}
