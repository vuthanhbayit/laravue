<?php

namespace App\models\Client\Extension;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table = 'module';

    protected $primaryKey = 'id';

    protected $fillable = [
        'page', 'position', 'sort_order', 'name', 'code', 'setting', 'status'
    ];
    protected $guarded = [
        'page', 'position', 'sort_order', 'name', 'code', 'setting', 'status'
    ];
}
