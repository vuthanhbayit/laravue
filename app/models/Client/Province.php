<?php

namespace App\models\Client;

use App\models\Client;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Province extends Model
{

    protected $table = 'province';

    protected $fillable = [
        'id', 'area_id', 'status'
    ];
    protected $guarded = [
        'id', 'area_id', 'status'
    ];

    public $timestamps = true;

    public function getProvince()
    {
        $expiresAt = Carbon::now()->addHours(24);
        if (Cache::has('province_' . app()->getLocale()) && getConfig('config_debug') == 0) {
            $data = Cache::get('province_' . app()->getLocale());
            return $data;
        } else {
            $data = [];
            $area = Area::where([
                ['area.status', '=', 1],
                ['area_description.language_id', '=', app()->getLocale()]
            ])
                ->join('area_description', 'area_description.area_id', '=', 'area.id')
                ->orderBy('area.id', 'DESC')
                ->select('area.id AS area_id', 'area_description.title AS title')
                ->get();
            foreach ($area as $i => $item) {
                $data[$i]['area'] = $item;
                $data[$i]['province'] = Province::where([
                    ['province.status', '=', 1],
                    ['province.area_id', '=', $item['area_id']],
                    ['province_description.language_id', '=', app()->getLocale()]
                ])
                    ->join('province_description', 'province_description.province_id', '=', 'province.id')
                    ->select('province.id AS province_id', 'province_description.title AS title')
                    ->orderBy('id', 'DESC')
                    ->get();
            }
            if (getConfig('config_debug') == 0) {
                Cache::put('province_' . app()->getLocale(), $data, $expiresAt);
            }
            return $data;
        }
    }

}
