<?php

namespace App\models\Client;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Setting extends Model
{
    protected $table = 'setting';
    protected $primaryKey = 'id';
    protected $fillable = [
        'code', 'key', 'value'
    ];
    protected $guarded = [
        'code', 'key', 'value'
    ];
    public function getValue($key)
    {
        if ($key == 'config_debug' || $key == 'config_maintenance') {
            $result = Setting::where('key', $key)
                ->select('value')
                ->first();
            return $result['value'];
        } else {
            $expiresAt = Carbon::now()->addDays(30);
            if (Cache::has('settingValue_' . $key)) {
                $data = Cache::get('settingValue_' . $key);
                return $data;
            } else {
                $result = Setting::where('key', $key)
                    ->select('value')
                    ->first();
                Cache::put('settingValue_' . $key, $result['value'], $expiresAt);
                return $result['value'];
            }
        }
    }
}
